var form = document.querySelector('form[name="accessUser"]');

function addErrorMessage(el, message) {
    if (el.nextElementSibling) {
        return;
    }

    var error = document.createElement('div');
    error.innerText = message;
    el.parentElement.appendChild(error);
}
// (el.value.includes('@  .com') || (el.value.includes('u')))
//if ((el.value.includes('@')) && (el.value.includes('.' + '' + '' + '' + '')))
function validateEmail(el) {
    var p1 = el.value.indexOf('@'); //Questo ti va a cercare almeno una @
    var p2 = el.value.indexOf('.', p1); //Questo ti va a cercare almeno un punto dopo la @
    var domainName = el.value.substr(p1, p2 - p1); //Questo prende la sottostringa che va dalla @ al punto
    var domain = el.value.substr(p2); //Questo ti prende la sottostringa che parte dal punto

    /** 
     - "p1 > -1" dice che ci dev'essere almeno un carattere dove cerchiamo la @
     - "p2 > -1" dice che ci dev'essere almeno un carattere dove cerchiamo il punto
     - "(ext.length > 2)" dice che a partire da ext (cioè la posizione del punto)
        ci vogliono almeno due caratteri
    */
    if (el.value.length > 5 && p1 > -1 && domainName.length > 2 && p2 > -1 && domain.length > 2) {

        var error = el.nextElementSibling;
        if (error) {
            error.remove();
        }

        return true;
    }

    addErrorMessage(el, 'La mail deve contenere almeno una chiocciola e due caratteri dopo il punto');
    
    return false;
}


function validatePassword(e, min) {
    if (e.value.length >= min) {

        var error = e.nextElementSibling;        
        if (error) {
            error.remove();
        }

        return true;
    }

    addErrorMessage(e, 'La password deve contenere almeno 8 caratteri');

    return true;
}

form.addEventListener('submit', function(e) {
    e.preventDefault();

    var email = document.querySelector('input[name="inputEmail"]');
    var password = document.querySelector('input[name="inputPassword"]');

    validateEmail(email);
    // var isValid = validateEmail(email, 3);

    validatePassword(password, 8);

    // if (!isValid) {
    //     return;
    // }
});



