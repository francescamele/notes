const xhr = XMLHttpRequest();
xhr.open('GET', 'https://');

//Funzione thr which we can gestire la ricezione dei dati:
//Essendo tutte le funz in js delle closure, possiamo
//accedere direttamente a xhr, ricordandoci che troviamo qui
//il JSON.
//? ...

//Anche questa roba dev'essere soggetta a transpiling:
//se l'utente usa un browser molto vecchio, c'è reference
//error, fetch non funziona
// xhr.addEventListener('loadend', ...
// ...

fetch('linkDaInterrogare').then(
    res => res.json(), //JSON.parse(res) - ma questa non è la
    //forma corretta

    //Il risultato non sarà un file: sarà una trasformaz del
    //json in oggetti istanza

    //Qui, nel caso di successo, ci cadrai anche con codici
    //di errore: perché la seconda callback gestisce solo
    //situazioni di TIMEOUT della richiesta: tutto ciò che
    //non ha permesso al backend di gestire la richiesta, qst
    //non ha potuto rispondere.
    err => console.error(err)
).then(
    data => console.debug(data)
);


// const p = new Promise((resolve, reject) => {
//     return resolve(100);
// });
// Se vogliamo gestire eventuali casi di errore, lavoriamo
// con due funzioni: eg, mostrare messaggio di errore:
// p.then(
//     res => console.log(res),
//     err => console.error(err)
// )
//Quando vogliamo ascoltare il risultato di qst chiamata, al 
//metodo then forniamo una funz che gestirà la struttura
//della nostra risposta

//27' promise


//Riscrittura con la fetch dello slider:
export function getMovies(movie, cb) {
    fetch(`link${movie}`)
        .then((res => res.json())
        .then((data) => {
            // Situazione analoga ad "addEventListener / loadend"
            const movies = data.Search.map(
                m => new Movie(m.imdbID, m.Title, m.Poster)
            );

            cb(movies);
        }));
}

/**
 * NON HA BISOGNO DEL 'SEND': il fetch fa partire la richiesta
 * alla stessa riga dove c'è il fetch
 */