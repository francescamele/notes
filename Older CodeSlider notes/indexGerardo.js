// entry point
import "bootstrap/dist/js/bootstrap";
import { getMovies, sliceMovies } from "./functions/slider";
import { getUsers } from "./functions/users";

var tbody = document.getElementById("tbody");

getUsers(function (users) {
  users.forEach(function (user) {
    tbody.innerHTML =
      "<tr><td>" +
      user.id +
      "</td>" +
      user.email +
      "</td><td>" +
      user.username +
      "</td></tr>";
  });
});

var slider = document.getElementById("slider");
var from = 0;
var to = 3;

var movies = [];

getMovies(function (apiMovies) {
  movies = apiMovies;
  sliceMovies(movies, from, to).forEach(function (movie) {
    slider.innerHTML +=
      '<div class="col-lg-3"><img class="poster" src="' +
      movie.image +
      '"></div>';
  });
});

function handleSliderChange(isNext, isPrev) {
  //Se sto andando avanti:
  if (isNext) {
    from = from === movies.length - 1 ? 0 : ++from;
    to = to === movies.length - 1 ? 0 : ++to;
  } else if (isPrev) {
    from = from === 0 ? movies.length - 1 : --from;
    to = to === 0 ? movies.length - 1 : --to;
  }
/**
 * fai una select in cui puoi scegliere batman o Star Wars
 * in base alla selezione, rifai la chiamata: ricarichi tutti i film dello slider
 * se selezioni batman, devono comparire le locandine di batman
 * DEVONO RIMANERE I PULSANTI BACK/FWD
 * 
 * Devi cercare di capire qual è l'evento da ascoltare sulla select
 * 
 */
  slider.innerHTML = "";
  sliceMovies(movies, from, to).forEach(function (movie) {
    slider.innerHTML +=
      '<div class="col-lg-3"><img class="poster" src="' +
      movie.image +
      '"></div>';
  });
}

document.querySelectorAll("button.slider-action").forEach(function (b) {
  b.addEventListener("click", function (e) {
    handleSliderChange(e.target.id === "slider-next", e.target.id === "slider-prev");

    //^Quest'operazione di confronto mi dice: se l'id è slider-next (se stai
    //premendo su Next), fammi quello che c'è nell'if.
    //Altrimenti, fammi quello che c'è nell'else.
  });
});

document.addEventListener("keydown", function (e) {
  e.key === "ArrowRight";
  e.key === "ArrowLeft"
  }
);

//Se mi interessa filtrare due bottoni e non altri, guarda cosa succede se metti
//true quando premi un bottone e false quando premi l'altro

//unexpected token in console: si usa quando il parse non funziona perché
//cerca una stringa e non ne trova una


//FIRST TRY
var menu = document.getElementById("slideDownMenu");
menu.addEventListener("click", function (b) {
  if (b.target.id === "batmanMenu") {
    console.log("bella, Batman");
  } else if (b.target.id === "swMenu") {
    console.log("bella, Jedi");
  }
});

//SECOND TRY
//var menu = document.getElementById("slideDownMenu");
function bellaFra(bm, sw) {
    if (bm) {
      console.log("bella, Batman");
    } else if (sw) {
      console.log("bella, Jedi");
    }
  }
  document.querySelectorAll("slideDownMenu").forEach(function (a) {
    a.addEventListener("click", function (b) {
      bellaFra(b.target.id === "bmMenu", b.target.id === "swMenu");
    });
  });