// entry point
import "bootstrap/dist/js/bootstrap";
import { getMovies, sliceMovies } from "./functions/slider";
import { getUsers } from "./functions/users";

var tbody = document.getElementById("tbody");

getUsers(function (users) {
  users.forEach(function (user) {
    tbody.innerHTML =
      "<tr><td>" +
      user.id +
      "</td>" +
      user.email +
      "</td><td>" +
      user.username +
      "</td></tr>";
  });
});

var slider = document.getElementById("slider");
var from = 0;
var to = 3;

var movies = [];

/**
 * 29.11 Aggiunta select
 * Due alternative: o la prima chiamata la facciamo impostando Batman
 * noi a mano, o appena arriviamo ci leggiamo il primo valore della
 * select e lasciamo batman o sw.
 *
 * Prima di tutto, dobbiamo individuare/chiamare la select, perché sulla 
 * select dobbiamo andare ad ascoltare l'evento 'change'.
 * Usi il querySelector, che è un metodo (a cui dobbiamo passare delle info,
 * e che vuole le parentesi tonde per invocarlo).
 * Gli passi una stringa, con prima di tutto il nome del tag:
 * 'select[name="nomeSelect"]'.
 * 
 * La rendi inoltre una variabile.
 */
 var select = document.querySelector("select[name='slideDownMenu']");

/*
 * Subito dopo, visto che abbiamo trovato l'elemento, aggiungiamo un
 * ascoltatore dell'evento.
 * Che evento dobbiamo ascoltare? Il 'change', che è il primo elemento. 
 * Poi, dopo la stringa, gli dobbiamo passare la funzione da eseguire 
 * ogni volta che scateniamo l'evento.
 * Ogni volta che la select cambierà, potremo leggerne il valore.
 * Sugli eventi, questo payload ha una serie di proprietà, tra cui l'elemento
 * che ha subito l'evento: 'target' (l'elem. che subisce o scatena
 * l'evento; in qst caso se l'evento 'change' si verifica sulla select, è la 
 * select che subisce l'evento).
 * e.target in console ci dà la select. La select ha l'attributo value 
 * al suo interno, quindi e.target.value = quello che l'utente ha selezionato.
 */
select.addEventListener("change", function (e) {
  //e.target.value;
  //^Potrei anche scrivere select.value
  slider.innerHTML = "";

  getMovies(e.target.value, function (apiMovies) {
    movies = apiMovies;
    sliceMovies(movies, from, to).forEach(function (movie) {
      slider.innerHTML +=
        '<div class="col-lg-3"><img class="poster" src="' +
        movie.image +
        '"></div>';
    });
  });
});
/**
 * ragionare su due fronti: da una parte devo inizializzare lo slider 
 * con qualcosa che essere i film di batman.
 * di base ho la funzione getMovies:
 * su slider, metto getMovies come primo parametro, non secondo
 */

getMovies("batman", function (apiMovies) {
  movies = apiMovies;
  sliceMovies(movies, from, to).forEach(function (movie) {
    slider.innerHTML +=
      '<div class="col-lg-3"><img class="poster" src="' +
      movie.image +
      '"></div>';
  });
});



document
  .querySelector("select[name='slideDownMenu']")
  .addEventListener("change", function (e) {
    var film = e.target.value;
    //Qui vai a capo perché stacchi la parte dichiarativa da 
    //quella funzionale.
    slider.innerHTML = "";
    //^Così facendo, gli dici che ogni volta che richiami la funzione 
    //la pagina dev'essere vuota, che è quello che NON fa rimanere le 
    //immagini della prima selezione quando selezioni la seconda.
    //Cioè gli dici che, all'evento change, prima di riempire l'array 
    //con le immagini parti con un array vuoto.
    //Questo perché con sliceMovies, col +=, CONCATENI elementi: 
    //non svuoti lo slider.

    //Quando selezioni una voce di una select, il browser mette un 
    //attributo selected sulla option.
    //Basta mettere 'selected' sulla voce vuota: quando ricarichi la 
    //pagina, l'elemento selezionato è quello vuoto (cioè quello dove
    //hai messo 'selected').

    //Qui vai a mettere tutto quello che hai scritto su sliceMovies, inclusi 
    //gli indici: una volta che prendiamo SW, vogliamo che ripartano da 0.
    //Stesso vale per l'array, che facciamo ripartire da vuoto.
    from = 0;
    to = 3;
    
    //movies = [];
    //^Questa NON deve stare dentro l'evento 'change': la dichiariamo
    //fuori, e ogni volta che richiamieremo getMovies ripartirà da qui.

    /**
     * Quando invochiamo questa funzione:
     * - le passiamo la funzione che dice cosa farà quando le passiamo
     *   i film;
     * - come seconda informazione, diamo il parametro 'film' che abbiamo 
     *   dato su slider.js
     *   (parametro al singolare, perché le variabili vanno al plurale 
     *   quando si tratta di un array)
     */
    getMovies(
      function (apiMovies) {
        movies = apiMovies;
        sliceMovies(movies, from, to).forEach(function (movie) {
          slider.innerHTML +=
            '<div class="col-lg-3"><img class="poster" src="' +
            movie.image +
            '"></div>';
        });
      },
      film === "0" ? "batman" : film
    );
    //film || 'batman'
    //^Questo 'film' è la voce selezionata nel menù a tendina: se dal
    //menù a tendina non abbiamo selezionato nulla, possiamo dire: 
    //passami i film, oppure batman.
    
    //Metti solo 'film', non fa vedere nulla se non hai selezionato nulla.
    //Se non metto l'attributo 'value' nell'html, lui in automatico prende il 
    //contenuto della option e lo mette dentro il value. Quindi se l'attributo
    //value è una stringa vuota, mi ci mette dentro 'Seleziona un film'. Perché
    //se il value è vuoto, mette il contenuto della option allo stesso modo.

    //Quindi metti 0 nella value? In teoria, lo 0, come carattere stringa, 
    //dovrebbe valutarlo come true.
    //Potrebbe aiutarci, però non funziona perché dà comunque ???
    //Prova con un ternario: 
    //se film è 0, passagli batman, altrimenti passagli film.
    // film === "0" ? batman : film
    //Questo non funziona perché in fase iniziale l'evento change non si 
    //scatena.
    //In fase iniziale dobbiamo dire: guarda il contenuto della select, 
    //prendi il valore iniziale, e fai selectMovies.
    //Il pezzo di codice che devi richiamare più volte lo metti dentro una 
    //funzione, così richiami quella.
  });

function handleSliderChange(isNext) {
  //Se sto andando avanti:
  if (isNext) {
    from = from === movies.length - 1 ? 0 : ++from;
    to = to === movies.length - 1 ? 0 : ++to;
  } else {
    from = from === 0 ? movies.length - 1 : --from;
    to = to === 0 ? movies.length - 1 : --to;
  }

  slider.innerHTML = "";
  sliceMovies(movies, from, to).forEach(function (movie) {
    slider.innerHTML +=
      '<div class="col-lg-3"><img class="poster" src="' +
      movie.image +
      '"></div>';
  });
}

document.querySelectorAll("button.slider-action").forEach(function (b) {
  b.addEventListener("click", function (e) {
    handleSliderChange(e.target.id === "slider-next");
    //^Quest'operazione di confronto mi dice: se l'id è slider-next (se stai
    //premendo su Next), fammi quello che c'è nell'if.
    //Altrimenti, fammi quello che c'è nell'else.
  });
});
document.addEventListener("keydown", function (e) {
  if (e.key === "ArrowRight") {
    handleSliderChange(true);
  } else if (e.key === "ArrowLeft") {
    handleSliderChange(false);
  }
});

document.addEventListener("select.menu", function (e) {
  console.log("bella");
});

// NON scrivere questo:
// handleSliderChange(e.key === "true");
// handleSliderChange(e.key === "false");

//Se mi interessa filtrare due bottoni e non altri, guarda cosa succede se metti
//true quando premi un bottone e false quando premi l'altro

//unexpected token in console: si usa quando il parse non funziona perché
//cerca una stringa e non ne trova una
