// import { Movie } from "../models/movie";

// export function getMovies(cb) {
//   var xhr = new XMLHttpRequest();
//   xhr.open("GET", "http://fake-movie-database-api.herokuapp.com/api?s=batman");

//   xhr.addEventListener("loadend", function () {
//     var data = JSON.parse(xhr.response);

//     var movies = data.Search.map(function (m) {
//       return new Movie(m.imdbID, m.Title, m.Poster);
//     });

//     cb(movies);
//   });

//   xhr.send();
// }

// export function sliceMoviesPrev(movies, from, to) {
//   var idx = from;
//   var goOn = true;
//   var ret = [];

//   do {
//   // Questo funziona: filtra in modo tale che idx non può mai superare la lunghezza dell'arr (non
//   // può mai valere 9), e quindi non può darti 9, 10, 11, che sono undefined e risultano come
//   // bianchi nel sito.

//     if (idx < movies.length) {
//       ret.push(movies[idx]);
//     }
//     ret.push(movies[idx]);
//     if (idx == to) {
//       goOn = false;
//     } else if (idx === movies.length) {
//       idx = 0;
//     } else {
//       idx++;
//     }
//     // if (idx < to) {
//     //   goOn = false;
//     // } else {
//     //   idx++;
//     // }
//   } while (goOn);

//   do {
//     ret.push(movies[idx]);

//     if (idx === to) {
//       goOn = false;
//     } else if ((idx < movies.length - 1 && idx > to) || idx < to) {
//       idx++;
//     } else {
//       idx = 0;
//     }
//   } while (goOn);

//   return ret;
// }

// export function sliceMoviesPrev(movies, from, to) {
//   var idx = from;
//   var goOn = true;
//   var ret = [];

//   // do {
//   //   ret.push(movies[idx]);

//   //   //Prima controllo condizione di uscita:
//   //   //è quella più semplice:
//   //   if (idx === to) {
//   //     goOn = false;
//   //   } else if (
//   //     (idx <= movies.length - 1 && idx > to) ||
//   //     (idx < to && idx >= movies[0])
//   //   ) {
//   //     idx--;
//   //   } else idx = movies.length - 1;
//   // } while (goOn);

//   do {
//     ret.push(movies[idx]);

//     //Prima controllo condizione di uscita:
//     //è quella più semplice:
//     if (idx === to) {
//       goOn = false;
//     } else if (idx < to || (idx > to && idx < movies.length - 1)) {
//       idx--;
//     } else idx = movies.length - 1;
//   } while (goOn);
//   return ret;
// }
