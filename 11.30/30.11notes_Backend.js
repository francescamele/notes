//1a REGISTRAZIONE
//28'
//Così, chiediamo tutti i post dell'utente con id 75:
// /users/75/posts 
//Così, chiediamo il suo post con id 980:
// /users/75/posts/980
//Si può scrivere anche così (?):
// posts/980
//perché sarà il backend a ripescarsi a quale utente è associato
//il post con quell'id.


/**
 * tabella del post:
 * ID TITOLO CONTENUTO
 * Di solito sono tabelle, le banche dati, che hanno un nome e tante righe.
 * ID: CHIAVE PRIMARIA, perché è l'elemento con cui identifichiamo in modo
 * univoco un record dentro la TABELLA DEI POST. 
 * Questa tabella determina una STRUTTURA all'interno della quale mettiamo
 * le info.
 * Questi sono dbs RELAZIONALI: struttura ben definita.
 * Linguaggio SQL: Structured Query Language. Attraverso di esso, possiamo
 * interagire coi dati tramite interrogazioni chiamate query.
 *  
 * Altri dbs sono NON RELAZIONALI: NoSQL
 * 
 * Chiave primaria: non necessariamente un id auto-incrementale: è uno o più
 * campi che mi consentono di identificare in modo univoco una di queste 
 * schede che ho nel cassetto.
 * 
 * Di solito però, per convenzione, si utilizza un id autoincrementale.
 * Il database si genera da solo l'id, assegnandolo. Di solito è 
 * autoincrementale perché se sono arrivata a 979 post, il prossimo sarà 980.
 * Non sono soltanto numerici, alcuni alfanumerici: combinazioni maggiori 
 * per poter avere più elementi all'interno dello stesso db.
 * Possono essere interi a una determinata lunghezza di bit, eg. a 16bit, o
 * 32bit (> è il numero di bit per definire un numero, > può essere il numero).
 * O possiamo avere sequenze alfanumeriche, che sono più lente perché sono
 * stringhe.
 * 
 * Foreign key:
 * Ci serve a dire che questo elemento appartiene a qualcuno. Per convenzione 
 * (quando si rispetta), avrò una tabella Users con, al solito: ID, email, ecc.
 * Devo associare un post a un utente? So che a 'user.id' troverò l'utente 
 * che ha generato il post.
 * 
 * Una delle regole delle interfacce REST (REpresentational State Transfer) 
 * è che le RISPOSTE devono essere OMOGENEE: per elenchi di post, singoli 
 * post, ecc, ci devono sempre mandare la STESSA STRUTTURA, perché sennò il 
 * frontend esce matto.
 * Puoi fare tu la richiesta direttamente al backend di mettere in ordine 
 * i dati in backend, ma dev'essere predistposto perché sia possibile.
 * 
 * SET DI CODICI DEL PROTOCOLLO HTTP PER COMUNICARE VARIE COSE:
 * 401 non autorizzato: stai cercando di accedere a una risorsa senza 
 *     il token.
 * 403 accesso negato: stai cercando di accere a una risorsa non tua,
 *     eg. cerchi di modificare il post di un altro utente.
 * 404 elemento non trovato
 * 200 richiesta andata a buon fine
 * 201 hai creato una risorsa
 * (56' ma non mi sembra molto importante)
 * 
 * Metodologia AGILE: sistema di lavoro che divide le fasi di lavoro in 
 * SPRINT.
 * A differenza di quello che dicono molti, NON significa lavorare con 
 * il backend 'in corsa', senza pianificazione. La pianificazione la fai, 
 * per le azioni da fare entro intervalli di tot tempo. Ma parti comunque 
 * dall'ANALISI.
 * 
 */