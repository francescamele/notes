//REC 1, 1h09'
//JAVASCRIPT ES6

/**
 * ES6 (7, 8, 9...) ha cercato di rinfrescare JS, che è nato come
 * linguaggio sfigato per far giocare i ragazzini. Col tempo si è
 * evoluto fino a permetterci di creare applicazioni per il web
 * davvero reattive e asincrone, più orientate all'utente che alla
 * semplice animazione della pagina.
 * Eg: puoi fare chiamate a servizi esterni, con applicazioni che 
 * basano TUTTO su chiamate a interfacce REST, a volte addirittura 
 * con l'utente che non deve ricaricare la pagina perché sei sistemi
 * ricaricano pezzettini di pag, come fa Angular (single-page app).
 * 
 * ES6 & following releases: dal 2015 in poi
 * ES5: prima del 2015
 * 
 * DIFFERENZA SOSTANZIALE: introduzione dello "use strict"
 * 'use strict': si aggiunge questa stringa all'inizio dei file js
 * per correggere alcuni bug del JavaScript Engine. Non si potevano
 * correggere tutti i bug di botto perché avrebbe spaccato l'internet.
 * Corregge i bug noti fino a quel momento.
 * 
 * ES6 nasce per modernizzare JS, per avvicinarlo ai linguaggi di 
 * programmmazione moderni dal punto di vista concettuale E pratico.
 * Per esempio, implementa il concetto di CLASSE: il costruttore è 
 * molto più semplice. 
 * Vengono introdotte le costanti.
 * Alle variabili si può limitare lo scope, non come avviene con var,  
 * visibili dappertutto.
 * ES6 ci darà una marea d'errori in qualche contesto perché si 
 * comporta come un linguaggio più normodotato.
 */
//Per esempio:
// console.log(x);
// var x = 10;
//^questo in ES5 funziona.

//Ma se:
// console.log(x);
// let x = 10;
//^Questo non funziona, perché let non è soggetta a hoisting. È una 
//variabile che non ha global scope (a meno che non sia sopra a tutto) 
//e ha visibilità solo dopo che è stata dichiarata.

//Posso ridichiarare let più tardi, riassegnarle un valore:
// let x = 14;

//MA:
// const k = 3.14;
// const k = 10;
//^Questo non si può fare, perché const è costante.

//Né const né let esistono in ES5, solo var.
//Tutte le versioni di JS sono RETROCOMPATIBILI: ES6 può leggerti ES5.
/**
 * È più facile iterare con gli array, in ES6, non ci sono for e indici.
 * Quando lavoriamo per il web, abbiamo lo scoglio del BROWSER, superabile
 * solo attraverso un TRANSPILER: non sappiamo quale browser userà l'utente,
 * né tantomeno la sua versione.
 * Quindi posso scrivere nel JS nuovo, MA devo TRASFORMARLO nel JS vecchio.
 * Allo stesso tempo, ci sono cose troppo nuove che però non possono essere
 * trasformate in cose vecchie.
 * 
 * Per fare questa trasformazione, si usano i TRANSPILER:
 * Applicazione che prende del codice sorgente in una certa forma e lo 
 * trasforma in una forma diversa ma perfettamente equivalente.
 * Il quando usarli, dipende da come vuoi usarli: da soli, o dentro 
 * webpack, in modo che venga un processo automatizzato.
 * 
 * Da soli, i transpiler non sanno fare niente: hanno bisogno delle POLYFILL.
 * Queste sono ISTRUZIONI che dicono al transpiler: quando trovi un pezzo 
 * di codice di questo tipo, trasformalo in quest'altro.
 * EG: Una polyfill di babel diraà: quando trovi let/const, mettici var, 
 * + serie di controlli tali per evitare che quel pezzo di codice cambi. 
 * Ci sono dei controlli nei transpiler che non permettono di fare cose 
 * che ti rompono il codice, quindi non ti permetterebbero di riassegnare
 * la variabile.
 * 
 * BABEL è il più utilizzato, ma fanno tutti la stessa cosa.
 * Volendo, Babel gira da solo: puoi usarlo da riga di comando, dicendogli 
 * 'prendi e transpila', 'parti, controlla i file, e ogni volta transpila';
 * puoi dire a webpack: 'prima esegui prettier, sopra mettici babel'.
 * 
 * (Riprende a 1h39')
 * Esempio funzionamento Babel:
 * https://babeljs.io/docs/en/babel-plugin-transform-arrow-functions
 * 
 * (Riprende 1h45')
 * https://babeljs.io/setup
 * Operazione In the browser: 
 * Mettiamo Babel dentro il browser con uno script: il browser lo analizza 
 * e transpila a runtime ogni volta che lo apri. Bordello, non usarlo.
 * Operazione CLI (clicca qui, sul sito): 
 * Common Line Interface: fra il transpiling PRIMA di farlo girare nel 
 * browser.
 * Operazione BUILD SYSTEM: 
 * anche webpack ne fa parte.
 * 
 * Babel, al di là di webpack, fa un lavoro che deve avvenire in fase di 
 * sviluppo, PRIMA della messa in produzione: non dev'essere messo 
 * nell'ambiente di produzione ma nelle dev dependencies.
 * Assicuriamo quindi di trovarci nella stessa cartella dove c'è package.json.
 * 
 * (Riprende a 1:54:25)
 * https://babeljs.io/setup
 * Se guardi sul sito (https://babeljs.io/setup), al paragrafo 3 Usage trovi
 * quello che dovresti aggiungere al package.json:
 * aggiungi uno script:
 * 
 *  *   {
    "name": "my-project",
    "version": "1.0.0",
+   "scripts": {
+     "build": "babel src -d lib"
+   },
    "devDependencies": {
      "@babel/cli": "^7.0.0"
    }
  }

 * "babel": "babel src -d lib"
 * ^Qui Andrea mette "babel" a sinistra perché così sarà quello il comando 
 * per babel.
 * "src" è il nome della cartella source per convenzione: ritroviamo qui lo 
 * stesso nome.
 * "-d" : destinazione: flag che precede la destinazione dei file
 * "lib" : qui verranno spostati tutti i file transpilati: è la cartella di 
 * destinazione.
 * In webpack invece, la cartella di destinazione non è 'lib' ma 'dist':
 * webpack esegue le sue operazioni e sposta tutti i file nella cartella
 * di distribuzione 'dist'. Noi poi ci leggiamo i sorgenti e facciamo altro.
 * Babel invece fa altro, con un'altra cartella: la cartella lib.
 * Quindi da una parte avremo i file scritti in un modo (ES5), dall'altra 
 * ho una cartella con altri file scritti in un altro modo (ES6), una per
 * webpack e una per Babel, che lavorano ognuno per conto loro.
, 
 * CLI
 * --save-dev : Babel è una dipendenza di sviluppo. 
  
  Quando lo aggiungi, Andrea ha chiamato il comando 'babel', non 'build',
  e ha lasciato lib.

  Nella root del progetto (la cartella dove c'è index.html, webpack.config.js,
  sove ci sono i file json), crea file:
    babel.config.json
  e al suo interno scrivi:
      {
      "presets": ["@babel/preset-env"]
    }
  che trovi sul sito di babel, alla fine della pagina (ci sono le istruzioni).

  npm install @babel/preset-env --save-dev
  Installare questo pacchetto di polyfill serve, certo, ma non basta:


  From index.js in lib:
  "use strict";

 * 2° REGISTRAZIONE
  Troverai questo al posto degli import, nell'index.js in lib:

    require("bootstrap/dist/js/bootstrap");
  
  Questo perché require è ciò che si utilizzava prima: gli import sono 
  stati aggiunti dopo. Gli import sono asincroni, i require sono asincroni.
  L'import però di fatto è asincrono perché lavora con le promise: queste
  sono state introdotte dopo, con ES6, e quindi Babel ci mette il require.
  (I require li usa prevalentemente il NodeJS, non lo sviluppo frontend
  per il web.) 
  
  Esempio di cambiamento:
  - Quando trasforma gli import in require, Babel dice: ciò che vado a 
  importare me lo carico dentro una variabile che è _slider. Quindi 
  getMovies me lo vado a prendere da _slider.
  - Var al posto di const
  
  Ora abbiamo una cartella lib con la roba e una dist con altra roba.
  Noi dovremmo usare quella di distribuzione, dist: quella che ha un 
  unico file. Però qui i let sono rimasti let, gli import sono rimasti 
  import: c'è del codice in ES6, qui non c'è stato il transpiling.
  Dobbiamo quindi dire a wb: dopo aver indentato il codice con prettier, 
  chiama Babel e digli di fare il transpile del codice, così il codice
  transpilato che ci dà babel lo passiamo nel babel.js
  Ecco il comando:
  
  npm install -D babel-loader @babel/core @babel/preset-env webpack


   
  Quindi webpack deve fare una cosa in più.
  Webpack funziona con elementi esterni, quali prettier e Babel, 
  sfruttando dei LOADER. Quindi, se per Prettier abbiamo usato 
  prettier-loader, adesso useremo babel-loader, 
  che prendiamo dalla documentazione:
  https://www.npmjs.com/package/babel-loader
  oppure:
  https://webpack.js.org/loaders/babel-loader/
  Nella documentazione, nella libreria, controlla sezione Usage: 
  troverai questo:

    module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [
              ['@babel/preset-env', { targets: "defaults" }]
            ]
          }
        }
      }
    ]
  }

  Quindi apriamo webpack.config.js: nelle regole che abbiamo messo, c'è
  use, e lì dobbiamo aggiungere loader: 'babel-loader'.  
  IMPORTANTE: WEBPACK LEGGE IL CODICE DAL BASSO VERSO L'ALTO: prima metti 
  babel-loader, poi metti prettier-loader:
  Prima ti indenta il sorgente, poi viene fatto il transpiling, perché non
  ci frega niente di quanto sia bello/brutto quello che scrive il browser,
  l'importante è che il sorgente sia indentato.

  A questo punto, fai ripartire webpack: perché webpack legge la 
  configurazione SOLO quando lo avviamo.
  Fatto quello, controlli che prettier funzioni (non si sa mai).
  Poi, se guardi il bundle.js dopo aver aggiunto babel e controllato 
  prettier, vedi un casino: tra le altre cose, ci sono tutti gli indici.
  Inoltre, se cerchi 'let' e/o 'let from' (CTRL + F per cercare) non trovi 
  nulla, mentre di 'var from' ne trovi una. Né troverai let in index.js ??
  Cerchi let sel: non la trovi, perché adesso è var sel
  ('sel' è il nome della variabile di Andrea, dipende da come l'hai chiamata)

  A questo punto, la cartella lib è verde: VSCode avvisa che non è stata 
  aggiunta al repository. MA NON DEVI, come node_modules: la roba generata
  da sistemi esterni NON VA MESSA NEL REPOSITORY.
  Quindi la cancelli PRIMA di fare git add/commit/push (NON la metti su 
  gitignore).

  Avendo eliminato lib, il browser si prende il bundle: babel ha lavorato
  il codice per webpack, poi lui se l'è buttato dove voleva, cioè nel 
  bundle.



 * Quando faremo la VERIFICA:
 * Come usare npm: inizializzare un progetto.
 * Come preparare webpack (potrai usare la documentazione).
 * Non prettier (vuole vedere come scriviamo il codice), ma Babel.
 * devtool: 'eval-source-map' lo dobbiamo mettere perché è quello che 
 * permette di fare debug del codice.
 * 
 * Promise
 * require
 * asincronia
 * callback
  */