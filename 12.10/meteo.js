import { Weather } from "./classes/weather";

const city = document.getElementById("city");
const forecast = document.getElementById("forecast");
const icon = document.getElementById("icon");
const temp = document.getElementById("temp");
// const weather = new Weather('Rome', 'IT-62', 'IT');

//...

const load = async () => {
    const weather = new Weather('Rome', 'IT-62', 'IT');
    const data = await weather.update();

    // Fai prima questo per verificare, nella console, POI 
    //aggiungi in modo visibile
    // console.log(data);

    city.innerText = `${data.city.name} - ${data.description}`;

    //Usa innerHTML e non Text perché così non prende gli invio:
    forecast.innerHTML = `
    ${data.temperature.current}°C (${data.temperature.feelsLike} percepiti)
    Min: ${data.temperature.min} - Max: ${data.temperature.max}
    `;

    //Utilizzanod il getter di una classe, accedo al valore come se 
    //leggessi il contenuto di una proprietà:
    // forecast.iconUrl

    //Questa è l'invocazione del getter:
    icon.src = data.iconUrl;

    // Invocando un metodo, uso invece le parentesi tonde:
    // i.e. forecast.getIconURL()
};

load();