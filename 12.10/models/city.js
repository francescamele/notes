export class City {
    /**
     * 
     * @param {string} name City name
     * @param {string} country Country code (i.e. IT)
     * @param {number} tz Timezone: minutes from the GMT+0
     * @param {numebr} lat latitudine
     * @param {number} long
     * @param {number} sunrise Sunrise timestamp
     * @param {number} sunset Sunset timestamp
     */
    constructor(name, country, tz, lat, long, sunrise, sunset) {
        this.name = name;
        this.country = country;
        this.lat = lat;
        this.long = long;
        this.sunrise = sunrise;
        this.sunset = sunset;
        this.tz = tz;
    }
}