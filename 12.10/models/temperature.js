export class Temperature {
    constructor(current, min, max, feelslike) {
        this.current = current;
        this.feelslike = feelslike;
        this.max = max;
        this.min = min;
    }
}