export class Wind {
    constructor(speed, deg, gust) {
        this.deg = deg;
        this.gust = gust;
        this.speed = speed;
    }

}