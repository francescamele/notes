export class Forecast {
    /**
     * 
     * @param {City} city 
     * @param {string} description 
     * @param {string} icon
     * @param {Temperature} temperature 
     * @param {Wind} wind 
     * @param {number} pressure 
     * @param {number} humidity 
     */
    constructor(city, description, temperature, wind, pressure, humidity) {
        this.city = city;
        this.description = description;
        this.humidity = humidity;
        this.icon = icon;
        this.pressure = pressure;
        this.temperature = temperature;
        this.wind = wind;
    }
    //Voglio sapere che proprietà metterci dentro: 54'
    //Per me deve avere: città, temperatura, tempo che fa...
    //Oppure posso dire: fammi vedere cos'ho nella risposta 
    //del mio provider, magari trovo info interessanti e 
    //voglio mapparmele:
    //nuovo file, in linguaggio json, poi copio-incollo lì 
    //le info che l'API mi ha dato: seleziono tutto, poi 
    //CTRL + K e poi F
    //Così me lo indenta, e posso vedere tutti i dati che 
    //l'API ha: posso vedermi quello che magari mi interessa 
    //e potrei voler aggiungere 

    //Dichiaro un metodo che a sx ha la keyword get:
    //serve a definire dei getter: un metodo che deve restituire 
    //qualcosa
    //Un getter (un metodo che usa come keyword get) crea una 
    //propr fittizia all'interno dell'istanza, il cui valore è 
    //ciò che viene restituito dal metodo
    //i.e. this.iconUrl <- NO parentesi tonde (si comporta come 
    //                     una proprietà)
    get iconUrl() {
        return `http://openweathermap.org/img/wn/${this.icon}@2x.png`;
    }

    //La differenza tra il getter e il metodo è come accedo 
    //al valore restituito

    //Qui invece è tutto attaccato, è il nome del metodo
    //Questo è un metodo qualsiasi, per leggere il valore 
    //restituito devo invocarlo
    //i.e. this.getIconUrl() <- SI' parentesi tonde (è un metodo, 
    //                          deve essere invocato)
    getIconURL() {
        return `http://openweathermap.org/img/wn/${this.icon}@2x.png`;
    }
    //Di là però di devi richiamare questo metodo


}
//In qst caso, c'è un sacco di roba, quindi potrei farmi 
    //anche più model, e ognuno strutturarmelo con i dati che 
    //ho disponibili: vedi file wind.js
