//1h18'

function User(id, email) {
    this.id = id;
    //Usando "this", ogni volta che invochiamo 'new' noi 
    //puntiamo alla nuova zona di memoria creata nello heap.
    //21'
    this.email = email;
}

const u = new User();

//Possiamo aggiungere metodi: ... servono a risolvere problemi 
//...
//Come si aggiungono i metodi all'interno di tutti gli ogg 
//User? Agendo sul prototype:

User.prototype.sayHi = () => {

};
//E poi chiamandolo:
u.sayHi();

//Abbiamo esteso classi: 22'
//Vedi appunti cartacei. 
//In Es5 si faceva in maniera funzionale, cioè thr functions 
//Poi si è passati a una sintassi più standard, che .. a 
//partire da CLASSI, non .-.

//class è la chiave per definire una classe. Serve a 
//sostituire la definiz dei costruttori ES5 (orientati alle
//funz) a favore di una sintassi universalmente riconosciuta 
//dai linguaggi orientati alla Programmaz ad Oggetti (OOP)
class User {
    //Metodo che verrà invocato facendo "new User" (equivalente 
    //al costruttore ES5)
    constructor(id, email) {
        this.email = email;
        this.id = id;
    }

    //La differenza è che posso, rispetto al metodo precedente, 
    //definire i metodi direttamente all'interno della classe:
    //non serve più usare prototype
    sayHi() {

    }
}
//Per estendere una superlasse, eg User, non agisco più sul 
//ptototype ma uso la chiave extends:
//Admin (la sottoclasse) prenderà tutte le proprietà ed i 
//metodi della superclasse User
class Admin extends User {
    constructor(id, email) {
        // con super invoco il constructor di User
        super(id, email);
    }

    //Saluta p un metodo inutile, anche Admin ha il metodo
    //sayHi. Inserito solo per farvi vedere come accedere 
    //ai metodi della superclasse
    saluta() {
        //se devo richiamare dei metodi all'interno della 
        //superclasse:
        super.sayHi();
    }
}

new Admin(1, 'email1@dominio.com');

//1h36 ?