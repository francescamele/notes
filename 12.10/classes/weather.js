import { config } from "../../environments/develop";
import { Wind } from "../models/wind";
import { City } from "../models/city";
import { Forecast } from "../models/forecast";
import { Temperature } from "../models/temperature";

export class Weather {
    constructor(city, state, country) {
        //Si prende le info per fare la chiamata: il widget 
        //del meteo a che città punta? Che STato? Che Paese?
        this.city = city;
        this.country = country;
        this.state = state;
    }

    //Questo metodo, thr una chiamata, aggiornerà le info del
    //meteo:
    // update() {
    //     fetch(`http://api.openweathermap.org/data/2.5/weather?q=${this.city},${this.state},${this.country}&appid=${config.weather.apiKey}`)
    //     //`http://api.openweathermap.org/data/2.5/weather?q=${this.city},${this.state},${this.country}&appid=`
    //     //alla fine, nell'appid, mettiamo la funzione che abbiamo 
    //     //importato, config
    //     //Adesso dobbiamo prenderci la risposta:
    //     .then(response => response.json())
    //     .then(data => {

    //     });
    // }

    //Essendo un metodo asincrono, posso scrivere:
    /**
     * Fetch the weather data from the OpenweatherMap API
     * 
     * @param {string} lang Language of the response text
     * @returns {Promise}
     */
    async update(lang = 'it') {
        const response = await fetch(`http://api.openweathermap.org/data/2.5/weather?q=${this.city},${this.state},${this.country}&appid=${config.weather.apiKey}&units=metric&${lang}`);
        const data = await response.json();

        //Trasformo i dati in un Model "Forecast"
        // return 'ciaoneeeeeee';
        //^Ma quindi restituisco la stringa? O è wrappata da 
        //qualcos'altro?
        //WRAPPED IN A PROMISE!, as always quando c'è un async 

        return new Forecast(
            new City(data.name, data.sys.country, data.coord.lat, data.coord.lon, data.timezone, data.sys.sunrise, data.sys.sunset),
            data.weather[0].description,
            data.weather[0].icon,
            new Temperature(data.main.temp, data.main.temp_min, data.main.temp_max),
            new Wind(data.wind.speed, data.wind.deg, data.wind.gust),
            data.main.pressure,
            data.main.humidity
        );
        //^Adesso l'oggetto ce l'abbiamo e lo restituiamo

        //Se faccio:
        // const forecast = new Forecast(
        //     ...
        // );
        //Allora poi faccio:
        //return forecast;
    }
}
//Di default, gli metto lang = 'it'
//Così, se dovessi fare: (?)
const w = new Weather('rome', 'IT-62', 'IT');
w.update();


/**
 * Passaggi per mostrare info:
 * 
 * 1. Carica info da openweathermap
 * 2. Elaborale (trasformale in un model) 37'
 * 3. Mostrale
 * 
 * 1. Thr metodo update()
 * 2. Trasformerò quei dati in una classe che chiamerò 
 *    Forecast: saranno le mie previsioni
 * 3. L'ogg Weather si occuperà di fare queste cose
 */

//In una cartella 'environments', metto il file develop.js
//In una cartella 'models', metto il file forecast.js