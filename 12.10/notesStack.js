/**
 * in questo stack, nel weboack.config hai più entry:
 * main: ./src/app.js
 * meteo: ./src/meteo.js
 * users: ./src/users.js
 * 
 * Quindi, il file meteo.html vorrà come script:
 * dist/meteo.bundle.js
 * 
 * Dentro app.js ci potrebbe mettere cose che sono condivise
 * in tutte le esercitazioni
 * 
 * 
 * Vanno installati questi due pacchetti:
 * npm install --save core-js
 * npm install --save regenerator-runtime  
 * Poi importi: 
 * import "core-js/stable";
 * import "regenerator-runtime/runtime";
 * Servono ad aggiungere delle polyfill, così babel puà 
 * funzionare: lui ti trasforma async in regenerator-runtime, 
 * e con quelle installazioni tu ti scarichi la libreria che 
 * permette al browser di leggere quella trasformazione.
 * 
 * 
 * Una volta installati, 29' usiamo app.js, il file principale, 
 * per importarci questa roba, 
 * O MEGLIO, nel nostro caso, lo metti nell'index, che va 
 * nel bundle.js (vedi quaderno)
 * 
 */