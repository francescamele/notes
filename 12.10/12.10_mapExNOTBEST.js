class myMap {
    #_keys = new Set();
    #_values = [];

    constructor() {
        //Nothing to do here
    }

    set(key, value) {
        //aggiungiamo la chiave 6' solo se non ce l'ha
        //Forse l'implementaz migliore è quella di Ari, 
        //non qst: i controlli li devo fare comunque
        if (this.#_keys.has(key)) {
            return;
        }

        this.#_keys.add(key);
        this.#_values.push(value);
    }
}