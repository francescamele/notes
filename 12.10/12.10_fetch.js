//Questo metodo ci restituisce una promise
//PROMISE CHAIN: il json ci restituisce una Promise, 
//ma anche il secondo then lo fa.
fetch()
    .then(response => response.json())
    .then(val => console.log(val))

//^Questo si può scrivere in un altro modo, ma è la 
//STESSA COSA, con le 

//FUNZIONI ASINCRONE
//una funzione asincrona inizia la sua firma con la 
//chiave async
//All'interno delle funz asincrone posso usare la keyword 
//"await": serve ad attendere il risultato di un'espressione.
//Lo usiamo dovunque devo aspettare il risultato di una promise, 
//dovunque io debba lavorare con una Promise.
async function myAsyncFunc() {
    //'await' serve a mettersi in ascolto di una Pr senza dover 
    //scrivere il then.
    //Vantaggio: dal pov di sintassi è molto più simile a una 
    //semplice assegnaz che non alla gestione di una Promise 
    //con il then.
    //Dal punto di vista pratico, il processo NON DIVENTA SINCRONO: 
    //è come se await aggiungesse per me il then, senza farmelo
    //vedere. Ma rimane asincrono
    //NON blocca lo script a questa riga: lo fa mettere nella 
    //task queue e lo fa aspettare.
    //Await semplicemente avvolge la Promise in un costrutto 
    //più semplice
    
    //Con questo, posso prendermi il response:
    const response = await fetch();
    //Per ascoltare il secondo then:
    const val = await response.json();

    //Altro esempio di quello che fa l'await
    const p = new Promise((resolve, reject) => {
        return resolve('info');
    });

    const successo = await p;
}

//È identico a:
new Promise((resolve, reject) => {
    fetch()
        .then(response => response.json())
        .then(val => resolve(val));
})

//Il corpo della Pr (nelle {}) equivale al corpo della 
//funz myAsyncFunc

//Le funz asincrone sono trattate come Pr, tanto che posso 
//aggangiarmici con "then". La chiave "async" è quindi un 
//wrapper di funzioni all'interno di Promise
myAsyncFunc().then()

/**
 * Per le chiamate HTTP, il JS ha un thread dedicato, separato 
 * dal suo single thread, in cui fa partire SUBITO la 
 * chiamata: la parte più lunga se la gestisce da solo subito,
 * la 
 */