/**
 * Le promise servono a gestire processi asincroni.
 * 
 * JS è un linguaggio asincrono e single thread: 
 * NON si può parallelizzare il lavoro, cioè eseguire due
 * funzioni allo stesso momento.
 * È asincrono perché può INTERROMPERE L'ESECUZIONE del 
 * codice a un certo momento, per poi riprenderla in seguito
 * per gestire per esempio degli eventi, come un click.
 * 
 * Il JS gestisce questa asincronia lavorando su una pila che si 
 * chiama stack, ma NON QUELLO DELLA RAM: una colonna in cui si 
 * impilano una serie di strati di eventi (lo strato più basso è 
 * il nostro script, il nostro bundle.js per es.), e poi quando 
 * questi strati si risolvono c'è un elemento, l'Event Loop,
 * che vede che si è svuotato lo stack e va a vedere se ci
 * sono operazioni in programma nella Task Queue: se sì, in base
 * all'ordine stabilito dalla coda prende quello che va e
 * eseguito e lo esegue, uno alla volta.
 * Quando parliamo di processi nella Queue, queste operazioni 
 * sono SCHEDULATE, eg con:
 */
setTimeout()
//^Esegui questa funzione dopo un intervallo di tempo espresso in 
//millisecondi
setInterval(() => {
    
}, interval);
//^Stessa cosa ma con cadenza regolare, non una tantum

//Anche gli addEventListener schedulano dei processi: la funzione 
//verrà schedulata e, ogni volta che facciamo il click, eseguita 
//con massima priorità
document.getElementById('').addEventListener('click', e => {})

//Poi ci sono le Promise: ci permettono di eseguire una funzione, 
//che definiamo con le {} (il corpo), posticipandone l'esecuzione 
//nella task queue
const p = new Promise(() => {

});
//Questo magari perché questa funz è complessa, potrebbe 
//rallentare la funzionalità dell'applicativo rispetto 
//all'utente, o magari vogliamo solo dare priorità ad 
//altro, e quindi possiamo dire: questa roba falla più
//tardi

//La Pr ammette 3 stati, ma nel corso della sua esistenza 
//ne vivrà solo 2, di cui 1 sarà SEMPRE :
//Pending: quando è stata dichiarata e inizia la sua 
//esecuzione.
//Poi può RISOLVERE o TERMINARE CON UN ERRORE:
//Resolve: restituisce un risultato
//Reject: termina con errore

const p = new Promise(() => {
    return resolve('valore da restituire');
});


const p = new Promise(() => {
    return reject('valore da restituire');
});

//Per ascoltare il risultato della Promise utilizzo il
//then: 'poi', quando la Pr avrà terminato la sua 
//esecuzione. 
//Ammette due funz: 
//1. Caso di successo: il suo parametro formale conterrà
//   il valore restituito da resolve
//2. Caso di errore: caso in cui la Pr faccia return 
//   reject: conterrà il valore da restituire in caso 
//   di errore
p.then(
    successo => console.log(successo),
    errore => console.error(errore)
)

//Per le fetch, a volte anche in resolved c'è un errore: 
//fetch, nel caso di successo, agglomera qualsiasi 
//risposta data dal server
fetch().then(
    //Il caso di successo di una fetch rappresenta sia quando 
    //il backend dà una risposta, un 200, sia quando dà un 
    //500, un internal server error.
    response => {},
    error => console.error('timeout')
)
//Quindi per il fetch il fallimento è altro, eg. il timeout.