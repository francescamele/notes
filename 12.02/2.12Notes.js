// Con ES6, possiamo usare for in (for of per gli array, for in
//     per gli oggetti)
    
//     a destra abbiamo l'oggetto, a sx avremo la CHIAVE che di volta in 
//     volta vorremo riutilizzare, la singola chiave su cui stai iterando
//     (in questo caso, key: che la prima volta conterrà k1, la seconda volta
//     conterrà k2, la terza chiappa2).

const o = {
    k1: 'valore1',
    k2: 10,
    chiappa2: {
        culetto1: 'valore1'
    }
};

for (const key in o) {
    //Se scrivessi o.key direi ad o di accedere alla chiave 'key': accederei alla proprietà
    //'key' nell'oggetto 'o'
    //NON è corretta perché la costante key non viene interpellata: non c'è

    //Accedo alla chiave presente in 'key' a ogni iterazione: la prima volta conterrà k1, la
    //seconda volta k2, la terza volta chiappa2..., all'interno dell'oggetto
    //"o"
    console.log(o[key]);
    //Così ti stampa il valore delle chiavi che key vede ogni volta.
    console.log(key, o[key]);
    //La seconda volta ti stampa prima la chiave e poi il valore.
}

/**
 * il for in si prende tutte le chiavi
 * (riascolta)
 * A ogni iterazione viene ridichiarata una costante k con le chiavi
 * 
 * Se facessi o.k NON andrei a leggere, nell'oggetto o i valori a, b, c, ecc.:
 * andrei a leggere la chiave che si chiama k
 **/

//58'
/**
 * 
 */
const key = Object.keys(o);

Object.keys(o).forEach(function (key) {
    console.log(key, o[key]);
})

//IN OPERATOR: in ci permette di capire se un oggetto ha al suo 
//interno una chiave
const o2 = {
    k1: undefined,
    alessandroBorghese: 10
};

/**
 * Con questa sintassi non controllo se esiste la chiave bens' il valore ad essa 
 * associato:
 */
//Metti che voglio controllare se dentro a o2 esiste la chiave k1
//e qla chiave k2:
if (o2.k1) {
    console.log('k1 esiste');
}
//Questo non funziona perché k1 è undefined, e undefined è false in
//booleano: ma se ti dà false, non ti fa entrare nell'if


if (o2.alessandroBorghese) {
    console.log("alessandroBorghese esiste");
}

//Anche una chiave cge non esiste in un oggetto restituisce "undefined",
//come spesso accade in JS quando si tenta di accedere a degli elementi
//inesistenti
// if (o2.chiaveCheNonEsiste) {

// }



const o2 = {
    k1: undefined,
    alessandroBorghese: 10
};
//"In" mi permette di verificare se la chiave presente in "o2" esiste,
//indipendentemente dal valore associato ad essa
if ('k1' in o2) {
    console.log('k1 esiste')
}
if ('alessandroBorghese' in o2) {
    console.log('alessandroBorghese esiste')
}

//2H
//CLONAZIONE OGGETTI:
const altraTipa = {
    name: 'Bora',
    tette: 40
}
Object.assign({}, altraTipa, {tette: 100})
//restituisce: {name: 'Bora', tette: 100}