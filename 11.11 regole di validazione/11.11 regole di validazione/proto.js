function Vehicle() {
    this.vProp = 'Prop del veicolo';
}

function Car() {
    Vehicle.call(this);
    this.cProp = 'Prop della macchina';
}

// Car eredita proprietà e metodi di Vehicle
// Nel farlo, perde però la definizione del suo costruttore
Car.prototype = Object.create(Vehicle.prototype);

// Per sopperire alla perdita di informazione sul costruttore
// di Car, riassocio il costruttore corretto alla classe
Car.prototype.constructor = Car;

var c = new Car();
console.debug(c);
