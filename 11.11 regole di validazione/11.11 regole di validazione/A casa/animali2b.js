
var form = document.querySelector('form[name="create-animal"]');/**
 * 
 * @param {*} el Variabile che individua il value dell'input
 * @param {*} min Valore minimo del value dell'input
 */
function validateInput(min, el) {
    if (el.value.length >= min) {
        var error = el.nextElementSibling;
        if (error) {
            error.remove();
        }
        return true;
    }

    if (el.nextElementSibling) {
        return;
    }

    var errore = document.createElement('div');
    errore.innerText = 'Scrivi almeno tre caratteri!'
    var posizioneErrore = document.body.children.item(0).children.item(1);
    posizioneErrore.appendChild(errore);
}


form.addEventListener('submit', function(el) {
    el.preventDefault();

    var isValid = validateInput(3, nameInputValue);
    if (!isValid){
        return;
    }

    var nameInputValue = document.querySelector('input[name="inputName"]');
    var animal = document.querySelector('select[name="selectName"]');

    console.log(nameInputValue, animal);
    validateInput(3, nameInputValue);
    
});