var form = document.querySelector('form[name="create-animal"]');

/**
 * Validate the "name" field
 * @param {HTMLElement} e Name Input field
 */
 function validateName(e) {
    if (e.value.length > 3) {
        var error = e.nextElementSibling;

        if (error) {
            error.remove();
        }
        
        return true;
    }

    if (e.nextElementSibling) {
        return false;
    }

    var error = document.createElement('div');
    error.className = 'error';
    error.innerText = 'Nome non valido';

    e.parentElement.appendChild(error);

    return false;
}

form.addEventListener('submit', function(e) {
    e.preventDefault();

    var animal = document.querySelector('select[name="animal"]');
    var name = document.querySelector('input[name="name"]');

    var isValid = validateName(name);

    if (!isValid) {
        return;
    }
});


