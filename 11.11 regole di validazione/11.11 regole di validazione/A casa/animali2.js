function Animals(riproduzione, dormire) {
    this.animalsProp = 'cervello, arti, sensi,';
    this.respirare = function() {

    };
    this.dormire = dormire;
    this.riproduzione = riproduzione || 'ovipara o vivipara.';
    this.sonno = function() {
        var sonnoAnimali = document.createElement('p');
        sonnoAnimali.innerHTML = 'Sonno del' +
        this.name + ': ' + this.dormire + '.';

        //PERCHè ME LO METTE DENTRO IL DIV DEL BUTTON????
        var posizioneSonno = document.body.children.item(0).children.item(2);
        posizioneSonno.append(sonnoAnimali);
    }
}

function Dog(name, riproduzione, dormire) {
    Animals.call(this, riproduzione, dormire);
    this.cacciare = function(){

    };
    this.dogProp = 'denti, coda, quattro zampe';
    this.name = name;
}

function Dolphin(name, riproduzione, dormire) {
    Animals.call(this, riproduzione, dormire);
    this.comunicare = function() {

    };
    this.dolphinProp = 'sonar, pinne, sfiatatoio';
    this.name = name;
}

function Duck(name, riproduzione, dormire) {
    Animals.call(this, riproduzione, dormire);
    this.duckProp = 'becco, ali, piume';
    this.name = name;
    this.volare = function(){

    };
}

Dog.prototype = Object.create(Animals.prototype);
Dog.prototype.constructor = Dog;

Dolphin.prototype = Object.create(Animals.prototype);
Dolphin.prototype.constructor = Dolphin;

Duck.prototype = Object.create(Animals.prototype);
Duck.prototype.constructor = Duck;

var dog = new Dog(' cane', 'vivipara', 'molte ore durante la giornata');
var dolphin = new Dolphin(' delfino', 'vivipara', 'sonno uniemisferico');
var duck = new Duck('l\'anatra', 'ovipara', 'brevi sonnellini durante la giornata');
var animals = new Animals();

dog.sonno();
dolphin.sonno();
duck.sonno();

console.log(animals, dog, dolphin, duck);