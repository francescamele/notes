// NON SERVE:
var a = document.body.children.item(0).children.item(1);


function Animals(riproduzione, dormire) {
    this.aProp = 'cervello, sensi, arti';
    this.dormire = dormire;
    this.respirare = function() {

    };

    //Qui metto "riproduzione || 'vivipara o ovipara'" perché così se
    //NON do il valore (come nel caso di Animals, ma anche di qualsiasi
    //sottoclasse in cui non lo specifico) la funzione darà false a
    //"riproduzione" e quindi mostrerà l'alternativa (in questo caso
    //la stringa 'vivipara o ovipara').
    //Se scrivessi solo "riproduzione", darebbe undefined.
    this.riproduzione = riproduzione || 'vivipara o ovipara';
    
    this.sonno = function() {
        var sonnoAnimali = document.createElement('p');
        sonnoAnimali.innerHTML = 'Sonno del' + this.name + ": " + this.dormire + '.';
        var b = document.body.children.item(0).children.item(2);
        b.appendChild(sonnoAnimali);
    };
    //^^non devi scrivere sonno = function... perché sennò crei una 
    //VARIABILE, non un metodo!!!
}

function Dog(name, riproduzione, dormire) {
    this.name = name;
    Animals.call(this, riproduzione, dormire);
    this.dogProp = '4 arti, pelo, denti';
    this.cacciare = function() {

    };
}

function Duck(name, riproduzione, dormire) {
    this.name = name;
    Animals.call(this, riproduzione, dormire);
    this.duckProp = 'ali, becco, piume';
    this.volare = function() {

    };
}

function Dolphin(name, riproduzione, dormire) {
    this.name = name;
    Animals.call(this, riproduzione, dormire);
    this.dolphinProp = 'sonar, pinne, sfiatatoio';
    this.comunicare = function() {
       
    };
}

// var d = new Dog('viviparo');
// d.reproductionType;
// console.log(Dog());

Dog.prototype = Object.create(Animals.prototype);
Dog.prototype.constructor = Dog;

Duck.prototype = Object.create(Animals.prototype);
Duck.prototype.constructor = Duck;

Dolphin.prototype = Object.create(Animals.prototype);
Dolphin.prototype.constructor = Dolphin;

var dog = new Dog(' cane', 'vivipara', 'tante ore durante la giornata');
dog.sonno();
var duck = new Duck('l\'anatra', 'ovipara', 'brevi pisolini');
duck.sonno();
var dolphin = new Dolphin(' delfino', 'viviparo', 'sonno uniemisferico');
dolphin.sonno();
var anim = new Animals();

console.log(anim, dog, duck, dolphin);
