/**
 * Funzione come quella di protChainAnimale2: nell'input
 * ci dev'essere un 
 * @param {*} min valore minimo della stringa
 * @param {*} el elemento in input, l'input feedback
 */
function minLength(min, el) {
    if (el.value.length >= min) {
        return true;
    }
    
    return false;
}
