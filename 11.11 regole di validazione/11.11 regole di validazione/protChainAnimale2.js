//VEDI P. 83

var form = document.querySelector('form[name="create-animal"]');

/**
 * 
 * @param {*} e Variabile che individua l'elemento HTML input.
 * @returns     Messaggio di errore per 
 */
function validateInput(e) {
    /**
     * Questa funzione determina cosa succede SE il campo INPUT 
     * è VALIDO:
     * Prima scrivo la condizione: controllo se la stringa digitata
     * dall'utente nel campo sia valida (più lunga di 3 caratteri).
     *  - In caso di successo, la funzione restituirà true, ovvero il 
     * valore che indicherà all'esterno che il campo risulta essere 
     * valido;
     *  - In caso di fallimento, la funzione restituirà false.
     **/
    
    if (e.value.length > 3) {
        //Questa prima parte, prima di dire 'return true' (="la regola
        //di validazione è stata rispettata"), dice:
        //SE c'è un messaggio di errore, cioè SE c'è un elemento 
        //sibling sotto il campo input, ELIMINALO. L'elemento sarebbe:
        //  <input name="name">
        //  <div>Scrivi almeno tre caratteri!</div>
        //Questo serve per quando adesso l'utente sta rispettando
        //i caratteri necessari, però prima si era sbagliato: se
        //il messaggio di errore c'è per via dell'errore precedente,
        //eliminalo.
        var error = e.nextElementSibling;

        // nextElementSibling può restituire due valori:
        //   - un elemento HTML qualora esista il div dopo l'elemento
        //     input
        //   - null qualora il messaggio di errore non ci sia
        // 
        // RICORDA: null, convertito a boolean restituisce FALSE
        //          HTMLElement convertito a boolean (come qualsiasi
        //          oggetto istanza) restituisce TRUE
        if (error) {
            error.remove();
        }

        // Restituisco true ed interrompo qui l'esecuzione della funzione
        return true;
    } 
    
    //Questo if serve per evitare che il messaggio di errore si
    //ripeta ogni volta che si clicca il bottone: 
    //se c'è un elemento dopo il campo input, interrompo l'esecuzione
    //della funzione qui con il return, e non passo proprio oltre,
    //quindi non vado a ricreare il messaggio.
    //Se è la prima volta che l'utente mette un valore sbagliato 
    //nell'input (< 3 caratteri), il nextElementSibling NON C'È,
    //quindi non entro neanche nell'if: vado oltre e leggo la var 
    //errore, e l'append dell'errore
    if (e.nextElementSibling) {
        //Se esiste, esco dalla funzione: non serve aggiungerne uno,
        //essendo questo già presente
        //funziona sia con return che return false, perché return
        //restituisce undefined, che in booleano ti dà false
        return;
    }

    //Non esiste un messaggio di errore ed il campo non è valido:
    // - creo un elemento div che conterrà il messaggio di errore
    var errore = document.createElement('div');
    // - aggiungo all'elemento appena creato una classe "error" che,
    //   in alcuni contesti, rappresenterà una discriminnte con 
    //   altri elementi
    errore.className = "error";
    errore.innerText = "Scrivi almeno tre caratteri!";
    a.append(errore);

    return false;
}

/**
 * Ogni volta che facciamo il submit del form, vogliamo che venga 
 * mandato il contenuto dell'input, cioè dello specchietto accanto 
 * a 'Nome': dobbiamo mandare quello che l'utente ha scritto.
 **/

form.addEventListener('submit', function(e) {
    e.preventDefault();
    //Questo e.preventDefault serve perché il default è che la pagina 
    //si ricarica, perché prima il form funzionava così: quando cliccavi 
    //submit, tutto il form si svuotava, e il backend ti restituiva 
    //qualcosa di diverso, inclusi html e CSS, con i dati opportunamente
    //utilizzati. Ma adesso non funziona più così, quindi si usa il 
    //prevent.Default, altrimenti automaticamente riavvia la pagina.

    //Blocca il comportamento di default del browser per l'evento che si è
    //scatenato, ovvero, nel caso del submit di un form, l'invio delle informazioni
    //alla action sfruttando il verbo HTTP declinato nell'attributo method.
    //(???)

    var nameInputValue = document.querySelector('input[name="name"]');
    var animal = document.querySelector('select[name="animal"]')

    //Valido il campo nameInputValue e prelevo il risultato (true: c'è un 
    //valore / false: non c'è un valore) che mi permetterà di capire se 
    //quanto introdotto dall'utente è valido oppure no)
    var isValid = validateInput(nameInputValue);

    //Se non è valido (validate ha restituito false), blocco
    //l'esecuzione del submit del form
    if (!isValid) {
        return;
    }

    //QUI metti tutte le azioni che dovresti fare se il form
    //fosse valido.

    /**
     * Vedi es. tabella_createElement.js.
     * Mettiamo name.value perché i campi in input mantengono quello che 
     * l'utente digita nel campo 'value'.
     * Questo attributo è sottinteso, puoi anche non specificarlo nell'html:
     * se lo specifichi, scrivi:
     * value=""
     * senza scrivere nulla nelle virgolette, perché se scrivi qualcosa 
     * rimarrà lì da modificare quando clicchi nel campo input, non si 
     * cancellerà automaticamente una volta che clicchi (vedi nota in html)
     */

    console.log(nameInputValue.value, animal);
    validateInput(nameInputValue);
});
/**
 * Se quando fai un querySelector ti restituisce NULL, allora probabilmente 
 * hai sbagliato selettore, oppure non c'è l'elemento nell'html.
 */