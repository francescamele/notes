var form = document.querySelector('form[name="create-animal"]');

/**
 * Validate the "name" field
 * @param {HTMLElement} e Name Input field
 */
 function validateName(e) {
    // Controllo se la stringa digitata dall'utente nel campo
    // è valida (più lunga di 3 caratteri)
    // Qualora sia più lunga di 3 caratteri, restituisco true
    // ovvero il valore che indicherà all'esterno che il campo
    // risulta essere valido
    if (e.value.length > 3) {
        // Prima di restituire true, verifico se in precedenza
        // questa regola di validazione ha inserito un messaggio
        // di errore dopo il campo in input:
        // <input name="name">
        // <div>Nome non valido</div>
        var error = e.nextElementSibling;

        // nextElementSibling può restituire due valori:
        //   - un elemento HTML qualora esista il div dopo l'elemento
        //     input
        //   - null qualora il messaggio di errore non ci sia
        // 
        //   Ricorda: null, convertito a boolean restituisce false
        //            HTMLElement convertito a boolean (come qualsiasi
        //            oggetto istanza) restituisce true
        if (error) {
            // È stato individuato un messaggio di errore introdotto
            // in precedenza: essendo un HTMLElement, richiamo
            // il metodo remove() per eliminarlo
            error.remove();
        }
        
        // Restituisco true ed interrompo qui l'esecuzione della funzione
        return true;
    }

    // Se sono arrivato qui, significa che non sono entrato nell'if
    // precedente: la stringa digitata dall'utente
    // non è più lunga di 3 caratteri e quindi non è valida
    //
    // Prima di aggiungere il messaggio di errore, verifico
    // se in precedenza ne è stato già inserito uno: per farlo,
    // controllo se il campo in input ha, a seguire, l'elemento div
    if (e.nextElementSibling) {
        // Se esiste, esco dalla funzione: non serve aggiungerne
        // uno, essendo questo già presente
        return false;
    }

    // Non esiste un messaggio di errore ed il campo non è valido:
    //   - creo un elemento div che conterrà il messaggio di errore
    var error = document.createElement('div');
    //   - aggiungo all'elemento appena creato una classe "error"
    //     che, in alcuni contesti, rappresenterà una discriminante
    //     con altri elementi
    error.className = 'error';
    //   - Definisco il contenuto del div
    error.innerText = 'Nome non valido';

    // Aggiungo il messaggio di errore:
    // A partire dal campo in input, accedo al contenitore (parentElement)
    // ed appendo a questo un figlio (il messaggio di errore)
    e.parentElement.appendChild(error);

    // Restituisco false poichè la regola di validazione non è
    // stata soddisfatta
    return false;
}

form.addEventListener('submit', function(e) {
    // Blocca il comportamento di default del browser
    // per l'evento che si è scatenato (ovvero, nel caso
    // del submit di un form, l'invio delle informazioni
    // alla action sfruttando il verbo HTTP declinato
    // nell'attributo method
    e.preventDefault();

    var animal = document.querySelector('select[name="animal"]');
    var name = document.querySelector('input[name="name"]');

    // Valido il campo name e prelevo il risultato (true / false)
    // che mi permetterà di capire se quanto introdotto dall'utente
    // è valido oppure no
    var isValid = validateName(name);

    // Se non è valido (validateName ha restituito false)
    // blocco l'esecuzione del submit del form
    if (!isValid) {
        return;
    }

    // Cose che dovrei fare se il form fosse valido
});


