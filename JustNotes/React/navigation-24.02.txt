24.02 rec I (phone)

Simile ad ang. 
In r. e r.native NON abbiamo già disponibili dei moduli di navigazione, a 
differenza di ang che è un framework che ci dà tutto disponibile da subito. 

    expo install react-navigation react-navigation-stack react-native-gesture-handler

^v. slide 54
Navigator: tipo routing module di ang: ogg che farà ? tra percorsi e componenti. 
Appl sarà costruita con createAppContainer: quando costruiamo ? dobbiamo avvolgerli 
con i comp di navigazione. 5'50
Con rn, ma anche altri framework per app ibride, abbiamo diversi concetti di 
navigazione, non solo col click. 
TRE TIPI DI NAVIGATOR, a seconda del MODO di navigazione: 
- Stack 
- Tab 
- Drawer

STACK 
Click su link navigazione -> se da App clicco su comp1 e poi da lì clicco su comp2, 
sia App che comp1 continuano a vivere: App perché vive sempre, comp1 perché li 
metti uno sopra l'altro. Però vedi solo quello on top. 
PROS: rapidità di load, non devi ricreare cose; NON PERDI lo STATO: in comp1 
lo stato rimane quello che hai lasciato lì quando sei passata a comp2.
CONS: hai poca ram, sul cell. 

TAB 
Di solito la trovi in app dove hai una specie di navbar. Le tab dove navigo sono 
attive, le altre no, e quelle non attive vengono distrutte. 

DRAWER 
Di solito si attiva quando scorro il pollice da un lato e a scomparsa ho il menu, 
tipo discord o gmail. 

p. 57: direttamente dall'App si crea il navigator e quindi non mostri più comp 
in App ma in navigator. 

Creo nuovo comp per la Home: 
    export default class Home extends React.Component {
        constructor(props) {
            super(props);
        }

        render() {
            return (
                <View>
                    <Text>Benvenuto!</Text>
                </View>
            );
        }
    }
Poi, in App.js: 
    import { createStackNavigator } from 'react-navigation-stack';

    const navigator = createStackNavigator({
        Home: { screen: HomeComponent },
        Location: { screen: LocationComponent }
    }, {
        initialRouteName: 'Home'
    });

^Si creano associazioni tra nome e componente: non sono percorsi web ma nomi 
che voglio dare a quel comp. 21'10
Qui si vede perché è meglio chiamare i comp con nomi "nameComponent", così è 
più chiara la differenza tra il nome del comp e il nome del percorso a esso. 

Quando usi lo stack navigator? 
Eg con i prodotti: schermo con elenco di prodotti -> clicco un dettaglio -> 
tornando indietro, non devo far ripartire la chiamata: lo stato già ce l'ho. 
I tre tipi di navigazione in realtà spesso sono obbligati, a seconda dell'app. 
Ovvio che col tab navigator passo da un comp di ricerca all'elenco eg delle 
playlist in spotify; ma nelmomento in cui sto nell'elenco e clicco un el, 
passo allo stack: se premo indietro non ho lo spinner che mi ricarica la roba. 

Import per default: 28'40

Se scrivessi:
  initialRouteName: Home
gli passerei il valore contenuto in una variabile/costante: gli voglio passare 
il 32'50

1h01 ??

1h17 ca ricomincia
https://reactnavigation.org/docs/hello-react-navigation

    function App() {
    return (
        <NavigationContainer>
        <Stack.Navigator>
            <Stack.Screen name="Home" component={HomeScreen} />
        </Stack.Navigator>
        </NavigationContainer>
    );
    }
^Cambia la forma, ma il contenuto no. 
Nella doc separa l'export default dalla dichiarazione, ma sono entrambi 
approcci corretti. 


1h40 ricomincia
Dalla docs c'è comp funzione: 
    function HomeScreen({ navigation }) {
        return (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Text>Home Screen</Text>
            <Button
                title="Go to Details"
                onPress={() => navigation.navigate('Details')}
            />
            </View>
        );
    }
^Qui abbiamo navigation passato come param alla fn, perché le props sono 
lì: function HomeScreen(props) {...}. Se scrivessi props, dovrei poi 
fare props.navigation.navigator, ma scrivendo direttamente { navigation } 
posso saltare un passaggio. 

In Home.js: 
        return (
            <View>
                <Text>Benvenuto! bella zi'</Text>
                <Pressable onPress={() => this.props.navigation.navigate('Products')}>
                    <Text>Prodotti</Text>
                </Pressable>
            </View>
        );
Se invece voglio passare dei param da un comp a un altro: 
    <Button
        title="Go to Details"
        onPress={() => {
          /* 1. Navigate to the Details route with params */
          navigation.navigate('Details', {
            itemId: 86,
            otherParam: 'anything you want here',
          });
        }}
    />
Dev'essere un ogg passato dopo il nome del comp dove stai andando. 
Eg: passando un determinato id, ti prendi solo la scheda di quel 
prodotto. 

I comp alle quali accedo con il soute, non possono lavorare con le props: 
queste posso metterle solo sui selettori, ma i selettori non li usi più. 
NON mandare info complesse: basta per es. l'id. 

In Products.js, updato il renderItem: da così
    #_renderItem({item}) {
        // console.log(item);
        return (
                <View style={style.row}>
                    <View style={style.oneThird}>
                        <Image source={ {uri: item.image} } style={style.image} />
                    </View>
                    <View style={style.twoThird}>
                        <Pressable onPress={() => this.props.navigation.navigate('Product')}>
                            <Text>{item.title}</Text>
                        </Pressable>
                    </View>
                </View>
        );
    }
a così
    #_renderItem = ({item}) => {
        // console.log(item);
        return (
                <View style={style.row}>
                    <View style={style.oneThird}>
                        <Image source={ {uri: item.image} } style={style.image} />
                    </View>
                    <View style={style.twoThird}>
                        <Pressable onPress={() => this.props.navigation.navigate('Product')}>
                            <Text>{item.title}</Text>
                        </Pressable>
                    </View>
                </View>
        );
    }
perché è un problema di scope: 2h03

In ProductComponent:
    constructor(props) {
        super(props);

        this.state = {
            product: {}
        }
    }
Perché così inizialmente c'è un ogg vuoto che verrà sostituito dagli ogg che 
richiamerò. 
E poi: 
    async componentDidMount() {
        // Prende una promise: mi ci aggancio con async
        this.setState({
            product: await getProduct()
        });
    }
