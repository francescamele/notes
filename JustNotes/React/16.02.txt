16.02 REC I (17)

LIST VIEW 

Nel config.babel abbiamo due arr, uno è plugins:
1. nome del plugin
2. oggetto di configurazione
Vedi babel.config.js 


REC II (18)
renderItem è un metodo: "#_renderItem({item}) " è una firma. 
{item} è un param formale: non è "item", ...

    #_renderItem({item}) {
        console.log(item);
        return (
            ...
        );
    }

...

const {k1, k3} = {
    k1: '',
    k2: '',
    k3: ''
}
^Con la destrutturazione creo due costanti, k1 e k3: k1 prende 
k3 prende k3. Non si lavora per posizione ma per chiavi: se 
scrivo
const {k3, k1} = {
    k1: '',
    k2: '',
    k3: ''
} 
è lo stesso. 

Quindi a me interessa SOLO "item" dell'oggetto che gli passo: questo 
avrà una serie di proprietà, all'interno avrà item, cioè il singolo 
el della lista, la singola occorrenza dove sto iterando l'arr. 

L'ogg viene generato da R. a partire dall'iterazione che fa Flatlist 
per visualizzare le info. 
Per ogni el contenuto dentro 'data', lui itererà, cioè invocherà il 
metdo renderItem. Di volta in volta, dentro item troverà l'el su 
cui itero: in questo caso, il prodotto. 
L'ogg non lo vediamo, si chiama ListRenderItem (o è di tipo 
ListRenderItem). 


FLATLIST / LISTVIEW
- Flatlist: istanzia solo gli ogg mostrati a schermo. Se ho 100 ogg 
in lista a, ma il cell mostra 4 ogg, io ho solo 4 ogg istanziati. 
L'arr dei prodotti ce l'ho pieno, devo averlo, in ram, ma NON ho 
tutti gli ogg istanziati. 

- Listview: stica. Ripassati flatlist, hai comunque keyEstractor, 
renderItem e data (probably).


REC III 
    <Image source={{item.image}} style={style.image} />
url -> protocollo + uri: https://fakestoreapi.com/img/61pHAEJ4NML._AC_UX679_.jpg
uri -> fakestoreapi.com/img/61pHAEJ4NML._AC_UX679_.jpg
il tag uri può prendersi tutto lo url, il tag url NON può prendersi 
solo lo uri 


branch searchbar