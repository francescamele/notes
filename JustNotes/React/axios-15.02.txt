15.02 RECI I (15)
AXIOS & GESTIONE ELEMENTI 

Sorta di comp per visualizzare prodotti, usando comp che ci 
dà React native per un'interfaccia carina: vedremo proprietà 
style di ciascun comp e lo sistemiamo come vogliamo.  

https://axios-http.com/docs/intro

API:
https://fakestoreapi.com/

OBIETTIVO: mostrare prodotto, magari anche mostrare info. 
In ang, se devo gestire un carrello avrò un servizio per 
mostrare ... 08

REACT: concetto distante da quello di a, perché di fatto per 
quello che riguarda gestione di DI e di percorsi fa cagare. 
In ang abbiamo guard, root, provider di DI: tutto ben 
separato. In r. si mette tutto nel codice jsx: abbiamo i 
selettori degli el da mostrare, e poi dobbiamo dirgli lì 
tutti le cose che lo riguardano (e solo UN provider). 
A. è uno standard per i progetti che devono essere ben 
strutturati. R. aiuta per un progetto molto piccolo: non ha 
tanti el scomposti, non devo scrivere troppo codice. A. 
conviene già se il progetto è medio-piccolo. 

Privalia: campagna, prodotti, carrello, login, logout, ordine. 
R. va bene. 
Amazon: a. è necessario. Con r. lo sviluppi pure, ma 
manutenere il codice è terribile. 



CREAZIONE COMP: lista di prodotti. 
Cartella products: Products.js 
    export default class ProductsComponent extends React.Component {
        constructor(props) {
            super(props);
        }
    }

Se il costruttore contiene soltanto l'invocazione a super, 
non c'è bisogno di scriverlo: perché estendendo React.Comp 
si prende già le props di React.Comp 

componentDidMount: comp è stato montato. 
Aggiungi il metodo: 
    componentDidMount() {
        
    }
Nel comp facciamo partire le chiamate: ^questo è come 
ngOnInit. 
Le props sono come le proprietà in binding ? 17'
Il costruttore è sincrono, ma tutti gli altri el sono 
asincroni, tutti schedulati. 
La chiamata la metti in componentDidMount, mentre lo stato 
lo gestisci nel costruttore. 

Comp che deve mostrare dei prodotti: in a. faremmo public 
prop con Prodotto[]

    constructor(props) {
        super(props);

        this.state = {
            products: []
        }
    }

Dentro componentDidMount potrei usare fetch per chiamata, 
però proviamo a usare axios. 
INSTALLAZIONE: 
- npm e yarn sono equivalenti, perché yarn 
  lavora sui repo di npm js. 
  npm nasce per gestire dip backend, per 21'40
- bower: non più gestito, quindi sul sito ti dicono di usare 
  npm.
  Nasce per gestire dip frontend: a volte c'era roba qui e 
  non in npm. 

IMPORTAZIONE DIPS: 
- import: asincrono. UMD (check acronym)
- require: sincrono 

Il comp deve gestire l'interazione con l'utente: visto che 
non so fino a dove arrivo con lo sviluppo, fai una cartella 
functions allo stesso livello di components 
functions -> products.js 
^Qui mettiamo tutte le funzioni legate ai prodotti.

    import axios from "axios";

    export const getProducts = () => {
        axios.
    }

Dopo il . trovo get, post, request: simile all'http di a. 
Firma del get() uguale all'http di a. 

Su axios.com: https://axios-http.com/docs/example
^vedi che lavora paro paro a fetch, con le Promise: famo 
tutto asincrono. 

    export const getProducts = async () => {
        const resp = await axios.get();
    }

Dentro get metti indirizzo di fakestoreapi, poi aggiungi 
console.log per vedere cosa ti stai prendendo 

    export const getProducts = async () => {
        const resp = await axios.get('https://fakestoreapi.com/products');
        console.log(resp);
    }

in Products.js: 
    componentDidMount() {
        getProducts();
    }

e poi richiami Products in view nell'App.js

53' ricomincia
Con async e await però NON possiamo usare la seconda fn 
per gestire un eventuale errore: si gestisce con try and 
catch: 
    export const getProducts = async () => {
        try {
            const resp = await axios.get('https://fakestoreapi.com/products');
            // .then(res => res.json())
            // .then(json => console.log(json));
            console.log(resp);
        } catch(e) {
            // 
        }
    }

Adesso dobbiamo crearci il model: prototipizzare l'ogg per 
il singolo product. 
cartella: models -> product.js 
    export class Product {
    
    }


...
1h09 ricomincia
    async componentDidMount() {
        const products = await getProducts();
    }

Quindi ora ho nello stato un array vuoto. Non posso 
pusharci dentro: lo stato è immutabile -> setState

    async componentDidMount() {
        const products = await getProducts();
        
        this.setState({
            products
        });
    }

o senza istanziare la costante, più elegante:
    