// Ci sarà un solo reducer per file, e un file per reducer: potresti
// anche importare il file per default se la fn fosse arrow fn

import { ADD_PRODUCTS, REMOVE_PRODUCT } from "../actions/products";

// I reducer prendono in input due info:
// - state: stato che arriva dallo store. Conterrà la porzione di
//   state relativa ai prodotti. Dentro a state declino i prodotti.
//   All'inizio, quando allo store passo i reducer, lo store non sa come 
//   sono strutturati i prodotti, qual è il valore iniziale da dare a products. 
//   Quando invoco createStore e gli passo i reducer, redux li legge e li
//   esegue tutti: perché io sul reducer gli do il valore iniziale di
//   quella porzione. 
//   Reducer: fn con switch and case. 
// - action: quella che abbiamo creato noi dandola al dispatch. 
export default function productsReducer(state = [], action) {
  // "state = []" come param -> quello è il suo valore di default. La prima volta 
  // che redux invocherà questa fn action è undefined e state è un arr vuoto. Poi
  // quando scatenerò una action lo stato esisterà e redux gli passerà lo stato 
  // e la action che ho erogato io con dispatch.

  // Sto controllando action.type perché voglio sapere che azione sto svolgendo
  // in questo reducer:
  switch (action.type) {
    case ADD_PRODUCTS:
      /**
       * Devi restituire l'arr che avevi prima concatenato con quello attuale. 
       * Ma se usi il push alteri l'originale, quindi devi restituirne uno nuovo. 
       * Intanto mett l'arr literal, già di suo un nuovo arr, una nuova istanza. 
       * Poi metti state.products: ho accesso allo stato attuale e leggo l'arr 
       * che è già in products: se è vuoto creo i primi el, se pieno li aggiungo.
       * Poi metti state, NON state.products: visto che a questo reducer corrisponde 
       * la chiave products, dentro state troviamo direttamente l'arr dei prodotti. 
       * Il reducer, una volta inizializzato, isola quella chiave con quel valore 
       * che gli diamo nel valore di default. Lo state di productsReducer contiene 
       * non tutto lo stato ma solo la porzione su cui opera. 
       * 
       * Poi devo aggiungere i nuovi prodotti agli el attuali: spread operator 
       * perché è un arr; action; riguardo la struttura: ho una chiave products, 
       * e quindi qua metto products.
       */
      return [
        ...state,
        ...action.products,
      ];
    case REMOVE_PRODUCT:
      return [...state].filter(p => p.id != action.productId);
    default:
      return state;
    // ^Se mi dovessi sbagliare a scrivere le action, mi darà lo stato 
    // attuale. Se un'azione non gli viene passata perché Redux sta inizializzando 
    // lo stato e sta leggendo i recuer, dirà: state di default è [], l'azione non 
    // esiste -> cado nel default, restituisco arr in valore iniziale, cioè 
    // inizializzo quella porzione di stato
  }
}
