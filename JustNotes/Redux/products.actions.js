// Le azioni sono ogg strutturati così, dove type è OBBLIGATORIO:
// {
//     users: [];
//     type: 'ADD_USERS';
// }

// Tutto maiuscolo perché è un el statico, NON calcolato a runtime, e
// le diamo un valore letterale:
export const ADD_PRODUCTS = "ADD_PRODUCTS";
export const REMOVE_PRODUCT = "REMOVE_PRODUCT";

// La action vera e propria è l'ogg, la fn ci serve per non fare cazzate.
export const addProducts = products => {
  // L'azione deve ricevere gli utenti e restituire l'oggettino???
  return {
    products,
    type: ADD_PRODUCTS,
  };
};

// Visto che è qui che ho il productId, sarà nel dispatch dove invocherò
// removeProduct che potrò prendermi l'id e quindi fare lo splice
export const removeProduct = productId => {
  return {
    productId,
    type: REMOVE_PRODUCT,
  };
};
