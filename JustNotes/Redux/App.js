// Questa è la versione che ci ha fatto fare Andrea inizialmente:
// Navigator
// import { createStackNavigator } from 'react-navigation-stack';
// import { createAppContainer } from 'react-navigation';

// const navigator = createStackNavigator({
//   Home: { screen: HomeComponent },
//   Location: { screen: LocationComponent }
// }, {
//   initialRouteName: 'Home'
// });

// export default createAppContainer(navigator);

import Ionicons from '@expo/vector-icons/Ionicons';

// Components
import HomeComponent from './components/home/Home';
import SearchComponent from './components/Search/Search';

// Navigation:
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import MyLibraryComponent from './components/my-library/My-Library';

import { Provider } from 'react-redux';
import { store } from './store';
import Products from './components/products/Products';
import LocationComponent from './components/location/Location';

const Tab = createBottomTabNavigator();
// Posso anche fare così, se voglio prendermeli separatamene:
// const { Navigator, Tab } = createBottomTabNavigator();

export default function App() {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Tab.Navigator
          screenOptions={({ route }) => ({
            tabBarActiveTintColor: 'green',
            tabBarIcon: ({ color, focused, size }) => {
              let iconName;

              switch(route.name) {
                case 'home': 
                  iconName = focused ? 'ios-home' : 'ios-home-outline';
                  break;
                case 'search':
                  iconName = focused ? 'ios-search' : 'ios-search-outline';
                  break;
                case 'my-library':
                  iconName = focused ? 'ios-albums' : 'ios-albums-outline';
                  break;
              }

              return <Ionicons name={iconName} size={size} color={color} />
            },
            tabBarInactiveTintColor: 'blue',
          })}
        >
          <Tab.Screen name='home' component={HomeComponent} options={{ title: 'Home' }} />
          <Tab.Screen name='search' component={SearchComponent} options={{ title: 'Search' }} />
          <Tab.Screen name='my-library' component={MyLibraryComponent} options={{ title: 'Library' }} />
          <Tab.Screen name='products' component={Products} options={{ title: 'Products' }} />
          <Tab.Screen name='map' component={LocationComponent} options={{ title: 'Location' }} />
        </Tab.Navigator>
      </NavigationContainer>
    </Provider>
  );
}

// export default function App() {
//   return (
//     <View style={styles.container}>
//       {/* <Button title={"+"} color={"green"}></Button>
//       <Button onPress={(isSum) => {
//         // if (isSum) {

//         // }
//       }} title={"-"} width={250} color={"red"}></Button>
//       <StatusBar style="auto" />
//       <Pressable>
//         <Text>Bella</Text>
//       </Pressable> */}
//       <StatusBar style="auto" />
//       {/* <ProductsComponent></ProductsComponent> */}
//       <LocationComponent />
//     </View>
//   );
// }

// const styles = StyleSheet.create({
//   btn: {
//     height: 150,
//     width: 200,
//   },
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
// });
