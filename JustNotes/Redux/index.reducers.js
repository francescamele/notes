// Si chiama index perché è il file iniziale, quello principale.
import { combineReducers } from "redux";
import products from "./products";

export default combineReducers({
  // Chiave: il nome che voglio dare alla porzione dello stato,
  // valore: il reducer
  // Dici a redux: per questa parte di stato, usa productsReducer
  products,
});
/**
 * ^Visto che productsReducer lo importo per default, posso chiamarlo
 * come voglio: lo chiamo products, così non devo scrivere chiave/valore.
 * export default perché così esporto il risultato di questa fn: serve
 * allo store per crearsi la combinazione dei reducer. Questa fn,
 * combineReducers, crea un unico reducer grande. La fn createStore
 * infatti vuole un unico reducer combinato.
 */
