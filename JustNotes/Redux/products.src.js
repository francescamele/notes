import "./app";

// Questo è uno degli entry point di webpack: vai in webpack-config
// e lo mappi
import { createStore } from "redux";
import { addProducts, removeProduct } from "./actions/products";
import { getProducts } from "./functions/products";
// Importo i reducer combinati: se hai chiamato il file "index", non
// c'è bisogno di darge il percorso preciso a index, perché
// automaticamente si prenderà quello.
import reducers from "./reducers";

// Prima cosa da importare: questa fn ci permette di creare lo store,
// cioè l'ogg istanza con cui interagisco per scatenare azioni e
// leggere lo stato.
// createStore vuole tutti i reducers combinati tra loro.
// 1h20'50 è quando aggiungo reducers
const store = createStore(reducers);
/**
 * Noi non partiremo dai reducer: partiremo dall'el più semplice, cioè 
 * l'azione.
 * Quando fai dispatch, passi al dispatch un ogg che rappresenta la tua
 * azione. Per convenienza, si definisce ciascuna azione in una fn: 44 se
 * al dispatch ogni volta che devo aggiungere prodotti passo un type e un 
 * arr, potrei sbagliare il nome delle chiavi. Se invece creo
 * una fn che dice "aggiungi prodotti", le passo un arr, e mi genera un 
 * ogg, e poi la invoco, sono sicura che non si sbaglierà.
 *
 * Per far avvenire un'azione:
 *  store.dispatch( addProducts([]) );
 * ma non vogliamo che avvenga in questo momento.
 * 
 * Dentro dispatch devi mettere un ogg, con type e i prodotti. Prende un 
 * ogg e lo passa allo store. 
 * store.dispatch({
    product,
    type: //costante 
   })
 * Per evitare di riscrivere ogni volta l'ogg, mi faccio una funzione che 
 * restituisce quest'ogg, come deleteProduct.
 * Perché passargli l'ogg product quando non so se è disponibile? Meglio 
 * productId. 
 */
// Posso accedere allo stato attuale:
console.log(store.getState());
// ^Lo vedo facendo un html con script per quel file in webpack.config,
// e in console vedo un ogg con un array vuoto come valore di products
// (sto a 1h22 ca)

getProducts().then(products => {
  store.dispatch(addProducts(products));
  console.log(store.getState());
});

// (33' rec II)
const table = document.getElementById("tbody");

// Quando ci arrivano i prodotti? Quand'è che l'ogg. state viene
// aggiornato?
// Ci sono momenti in cui lo store emette segnali, come gli obs. Per
// ascoltare usi subscribe (anche se non lavora con rxjs).
// Qui gli passi, come gli obs, la callback da eseguire quando
// si scatena quell'evento, cioè quando lo store emette un segnale.
// 39' Dentro questa fn posso andare a modificare prodotti nello stato?
store.subscribe(() => {
  // Per svuotare la tabella: magari non è vuota quando faccio la
  // chiamata, magari è la seconda volta
  table.innerHTML = "";

  // Modifico la table con i products nello state: se la fn viene eseguita
  // a ogni modifica dello state, essendo una closure, scrivendo questo
  // mi prendo tutti i prodotti all'ultimo passaggio dei reducer allo stato,
  // cioè l'arr di prodotti più recente possibile.
  store.getState().products.forEach(p => {
    table.innerHTML += `
         <tr>
            <td>${p.id}</td>
            <td>${p.name}</td>
            <td><img src="${p.image}"></td>
            <td><button data-product-id="${p.id}">Eliminate</button></td>
         </tr>
      `;

    const button = document.querySelectorAll("button").forEach(b =>
      b.addEventListener("click", e => {
        store.dispatch(removeProduct(e.target.dataset.productId));
        console.log(store.getState());
      })
    );
  });
});
