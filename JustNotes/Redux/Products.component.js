import { createNativeStackNavigator } from "@react-navigation/native-stack";
import React from "react";

// redux
import { connect } from "react-redux";

// react native elements
import { FlatList, Image, Pressable, StyleSheet, Text, View } from "react-native";

// functions
import { getProducts } from "../../functions/products";

// actions
import { addProducts } from "../../actions/products";

const Stack = createNativeStackNavigator();

const style = StyleSheet.create({
    image: {
        height: 100,
        width: 100
    },
    oneThird: {
        flex: 0.33
    },
    twoThird: {
        flex: 0.66
    },
    row: {
        flex: 1,
        // Di default potrebbero essere tutte flexbox:
        // display: 'flex',
        flexDirection: 'row'
    }
});

// export default class ProductsComponent extends React.Component {
// ^levo l'export: nessuno si deve prendere questo comp, ma solo lo stato
class ProductsComponent extends React.Component {
    #_keyExtractor(product) {
        return product.id.toString();
    }

    // renderItem è un metodo: "#_renderItem({item}) " è una firma. 
    // {item} è un param formale: non è "item", 
    #_renderItem = ({item}) => {
        return (
            <View>
            <View style={style.row}>
                <View style={style.oneThird}>
                    <Image source={ {uri: item.image} } style={style.image} />
                </View>
                <View style={style.twoThird}>
                    <Pressable onPress={() => this.props.navigation.navigate('Product', {id: item.id})}>
                        <Text>{item.title}</Text>
                    </Pressable>
                </View>
                {/* <View style={style.oneThird}>
                    <Image source={ {uri: item.image} } style={style.image} />
                </View> */}
            </View>
            </View>
        );
    }

    // Con redux si può togliere this.state, e a quel punto tutto il constructor pure
    // constructor(props) {
    //     super(props);

    //     this.state = {
    //         products: []
    //     }
    // }

    async componentDidMount() {
        this.props.addProducts(await getProducts());

        // this.setState({
        //     products: await getProducts()
        // });

        // console.log('boh', await getProducts());
    }

    render() {
        return (
            <View>
                {/* <Stack.Navigator>
                    <Stack.Screen name='Product' component={ProductsComponent} />
                </Stack.Navigator> */}
                <FlatList data={this.props.products} keyExtractor={this.#_keyExtractor} renderItem={this.#_renderItem}></FlatList>
            </View>
        );
    }
}

// Il nome dell'azione corrisponde alla funzione dell'azione. 
// const mapDispatchToProps = { addProducts: addProducts };
const mapDispatchToProps = { addProducts };
// Questa fn viene invocata da redux e accede allo state: declino 
// nell'ogg gli el dello stato che voglio che siano inseriti 
// all'interno delle props del comp. "state." perché il valore lo 
// leggo dallo stato. 
// ogg = {nomePropDelComp: valoreDatoNelloState}
const mapStateToProps = state => ({
    products: state.products
});

// connect() si prende queste due const che ho creato e mi restituisce una 
// funzione: per questo ci sono due (), perché le seconde () sono della funzione 
// che mi restituisce: sto invocando la fn che mi restituisce connect().
// Questa funzione la invoco, le passo il comp che ho creato, e mi restituisce un 
// altro comp, il nostro ProductsComponent con delle cose in più: quelle che 
// ho chiesto di aggiungere thr connect.
// Aggiunge cose a ProductsComponent -> Mi restituisce una fn decoratrice: 
// avvolgendo il comp a un livello più esterno.
export default connect(mapStateToProps, mapDispatchToProps)(ProductsComponent);
// ^Non esporterò più il mio ProductsComponent, che è la base: esporterò il 
// comp avvolto dalla funzione che mi dà connect.

/**
 * Ogni volta che products cambia, magari con un nuovo prodotto o uno in 
 * meno, react riesegue il metodo render: questo perché redux crea una 
 * nuova istanza di ogg, e quindi lo state è stato copiato e ricreato. 
 * 
 * 1h10
 * I dati li leggiamo dalle props. Le azioni pure le abbiamo messe nelle props. 
 * Visto che non usiamo più lo state di react, per fare un dispatch 
 * dell'azione non devo fare il dispatch dello store: mi basta chiamare 
 * un'azione passandole i prodotti. Vedi componentDidMount
 */