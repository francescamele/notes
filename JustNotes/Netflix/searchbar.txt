16.02 REC I (17)
Chiamata tra due comp molto distanti tra loro 

...

Chiamata barre nella barra di ricerca, ma il comp che ascolta i 
film rimane fisso ad ascoltare il canale (behavior subject): quando 
passeranno dei dati, tutti gli ascoltatori le riceveranno. 


SERVIZIO

SUBJECT: 
- Observable: può emettere dei dati. 
- Può anche RICEVERE info facendo subscribe ad altri canali. 
- MULTICAST.

BEHAVIOR SUBJECT
Prende, quando lo costruisci, un valore iniziale. 
Subj comportamentale: quando qualcuno ascolta il suo stream, 
emette l'ultima info che ho emesso in precedenza: così anche chi 
ascolta ha uno stato iniziale. 
Ergo, nel costruirlo devi dargli uno stato iniziale. 

    private _movies: SearchMovie[] = [];
    private _movies$ = new BehaviorSubject<SearchMovie[]>(this.movie);
    public ...

Il BehaviorSubject è privato: ...5'40
$ -> è uno stream 
La prop PUBBLICA è quella a cui potranno farsi i subscribe: si 
possono leggere i dati che transitano ma NON si potrà propagare. 
Quello si fa con next() sulla prop privata. 

AGGIUNTA:
Aggiungi film a quelli mostrati, eg quando 
arrivi al fondo della pagina e carichi altri film. 

addMovie(): propago un solo film. 
.push: aggiungo un film all'array 
.next: propaga una info in più: questo array con il film in più 

addMovies(): prendi tanti altri film e li metti in coda all'arr 
di film.
.next(): propago l'arr con i film. 


SETTING THE MOVIES 

setMovies(): l'arr di film viene sostituito da quello che gli 
passo io, e poi lo propago col next() 




COMPONENTI 
I due comp NON dialogano tra loro: sono separati a livello logico 
e concettuali. Potresti anche averne un altro che mostra il num di 
film che sto cercando, anche completamente staccato. 

... fino a 19'

/***
FORM MULTISTEP ? ...

COMPONENT FACTORY: istanziare comp thr navigazione url o bottoni? 
I form multistep però non si gestiscono così...
Nel comp gestisci l'istanza di un comp e lo inietti ???
Creazione comp a runtime. 
***/


ELENCO DEI FILM 
ResultsMoviesComponent 

public movies$: observable di un arr di film. Dico che è = allo 
stream pubblico del servizio. 
Questo perché il servizio è privato: non posso leggerlo dal 
template. Quindi devo metterlo in una prop pubblica. 


SOTTOSCRIZIONE AL RISULTATO
in ngOnInit non c'è un subscribe: mi iscrivo NON lì ma NEL TEMPLATE. 
ngFor: per ogni film, ...31'
movies$ | pipe async -> prendo gli el emessi da un subscribe, che 
sono arr quindi posso usarli nell'ngFor, e mi prendo i dati. Inoltre, 
la pipe async mi fa anche l'unsubscribe quando chiudi il comp. ?

Se metto |async anche in ? le chiamate non finiscono più. Inoltre, 
l'observable risulta una nuova istanza in continuazione, quindi ...
Qui invece faccio la chiamata, e poi quei dati che ricevo li passo 
al BehaviorSubject: mi aggancio a un altro stream. Ce ne sono due: 
uno che emette dati dalla chiamata e uno dal BehaviorSubject.

Puoi tecnicamente usare |async sulle chiamate, ma devi aggiungere 
altri operatori e sarebbe troppo hard mode. 
Andrea preferisce farlo perché in automatico gestisce subscribe e 
unsubscribe: non devo farlo nel comp, e visto che il comp dovrebbe 
essere più dry possibile, questo è meglio. Ma molti non sanno usare 
rxjs anyway. 


??perché solo prop private nel costruttore? Non funziona altrimenti?
Uno dei paradigmi della programmaz a ogg è l'INCAPSULAMENTO: incapsuli 
una prop all'interno di un'altra per proteggerla dall'esterno. 
È meglio evitare di rendere alterabile l'ogg: funzionerebbe metterlo 
in public, ma sarebbe pericoloso.
Andre ci tiene al fatto che gli el abbiano scope solo dove servono, 
altrimenti è come JS, tutto pubblico. 

Separazione netta tra CHI FA e CHI MOSTRA le info, se ho un problema 
a livello di algoritmo vado direttamente nel comp, non perdo tempo 
col template. 
