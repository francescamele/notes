import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { IListTitle } from '../interfaces/list-title.interface';
import { ListTitle } from '../models/list-title';

@Injectable({
  providedIn: 'root'
})
export class MovieListService {

  // private _objToModel(obj: IListTitle): ListTitle {
  //   return new ListTitle(obj.image, obj.id);
  // }

  constructor(private _http: HttpClient) { }

  // public getList(): Observable<ListTitle[]> {
  //   return this._http.get<IListTitle[]>(`http://localhost:3000/top-movies`).pipe(
  //     map(list => list.map(el => this._objToModel(el)))
  //   );
  // }
}
