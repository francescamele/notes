14.02 REC II (14)

Dentro il servizio ti fai questo:

il behaviourSubject è un el che, come il subscribe, può 
? i flussi, ma può anche ...

Particolarità: ha uno stato iniziale: arr vuoto, perché 
noi lavoreremo su un arr di film; e nel momento in cui fa 
un ? a uno stream, automaticamente riceve l'ultimo el 
emesso dallo stream.

_movies$ è privata: ...
Ma parallelamente ho anche una prop pubblica:
movies$ = this._movies$.asObservable([])
Così la parte pubblica sarà SOLO quella che ...
Così, i comp che si iniettano questo servizio non possono 
emettere dati: è privato. Poossono solo ascoltare ciò che è 
emesso: la parte pubblica.
...
Quando hai il comp della ricerca fai partire la chiamata della 
ricerca - ricevi i dati - ti crei un metodo tipo addMovies(par) 
dove par è il param formale che sono i film che devi passare:
    addMovier(par) {
        this._movies$.next(movies)
    }
Dove movies sono l'arr che devi ...

All'inizio non mostra niente: behaviourSubject ha un arr vuoto. 
Ma in ngOnInit puoi far partire una chiamata coi film da 
mostrare inizialmente e ...
Quei film che ricevi li propaghi in this._movies$.next(movies) 
...
