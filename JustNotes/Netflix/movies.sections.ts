import { Component, OnDestroy, OnInit } from '@angular/core';
import { map, Subscription, switchMap } from 'rxjs';
// import { MovieListService } from 'src/app/services/movie-list.service';
// import { MovieService } from 'src/app/services/movie.service';

@Component({
  selector: 'app-movies-section',
  templateUrl: './movies-section.component.html',
  styleUrls: ['./movies-section.component.scss']
})
export class MoviesSectionComponent implements OnDestroy, OnInit {

  private _list$!: Subscription;

  public idsList!: string[];

  // constructor(private _movieList: MovieListService, private _movieService: MovieService) { }
  constructor() { }

  ngOnDestroy(): void {
      this._list$.unsubscribe();
  }

  ngOnInit(): void {
    // this._list$ = this._movieList.getList().pipe(
    //   map(listTitlesArray => {
    //     for (let i = 0; i < listTitlesArray.length; i++) {
    //       this.idsList.push(listTitlesArray[i].id);
    //     }

    //     return this.idsList;
    //   }),
    //   switchMap(idsArray => this._movieService.getKeywordTitle(idsArray))
    // )

    // this._list$ = this._route.paramMap.pipe(
    // map(() => this._movieList.getList().forEach(titles => {
    //   // for (let i = 0; i < titles.length; i++) {
    //   //   this.titleId = titles[i].id;
    //   //   this.titleImg = titles[i].image;
    //   // }
    //   // return this.titleId as string;
    //   this.titles = titles.id;
    // })
    // ),
    // switchMap(id => this._movieService.getKeywordTitle(id as string))
    // ).subscribe(title => {this.titles = title})
  }

}
