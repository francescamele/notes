import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, map, Observable } from 'rxjs';
import { IResult } from '../interfaces/search-result.interface';
import { ISearchTitle } from '../interfaces/search-title.interface';
import { SearchTitle } from '../models/search-title';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  // private _movies: SearchTitle[] = [];
  // private _movies$ = new BehaviorSubject<SearchTitle[]>(this._movies);
  // public movies$ = this._movies$.asObservable();

  // constructor(private _http: HttpClient) { 
  //   console.log("Search Component has been loaded")
  // }

  // private _objSearchTitle (obj: ISearchTitle): SearchTitle {
  //   return new SearchTitle(
  //     obj.id,
  //     obj.image,
  //     obj.title
  //   )
  // }

  //  public addTitle(m : SearchTitle): void {
  //  this._movies.push(m)
  //  this._movies$.next(this._movies);
  // }

  // public addTitles(movies: SearchTitle[]): void {
  //   this._movies.push(...movies);
  //   this._movies$.next(this._movies)
  // }

  // public setTitles(movies: SearchTitle[]): void {
  //   this._movies = movies;
  //   this._movies$.next(this._movies);
  // }

  // public searchTitle(value:string): Observable<SearchTitle[]> {
  //   return this._http.get<IResult>(`${environment.api.url}/Search/${environment.key}/${value}`).pipe(
  //     map(result => result.results.map(title => this._objSearchTitle(title)))
  //   )
  // }  
}
