// url-sanitizer.pipe.spec.ts
import { UrlSanitizerPipe } from './url-sanitizer.pipe';

describe('UrlSanitizerPipe', () => {
  it('create an instance', () => {
    const pipe = new UrlSanitizerPipe();
    expect(pipe).toBeTruthy();
  });
});

// url-sanitizer.pipe.ts
import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({
  name: 'urlSanitizer'
})
export class UrlSanitizerPipe implements PipeTransform {

  constructor(private _domSanitizer: DomSanitizer) {}

  transform(url: string): unknown {
    return this._domSanitizer.bypassSecurityTrustResourceUrl(url);
  }

}
