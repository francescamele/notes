import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

// environment
import { environment } from 'src/environments/environment';

// interfaces
import { IKeyword } from '../interfaces/keyword.interface';
import { IListTitle } from '../interfaces/list-title.interface';

// models
import { ListTitle } from '../models/list-title';
import { IResult } from '../interfaces/search-result.interface';
import { Title } from '../models/title';

// rxjs
import { map, Observable } from 'rxjs';
import { ITitle } from '../interfaces/title.interface';

@Injectable({
  providedIn: 'root'
})
export class MovieService {
  constructor(private _http: HttpClient) { 
  }

  // private _objToKeywordModel(obj: IListTitle): ListTitle {
  //   return new Title(obj.image, obj.id, obj.title, obj.favorite);
  // }


  // public getKeywordTitle(genre: string): Observable<ListTitle[]> {
  //   return this._http.get<IKeyword>(`${environment.api.url}/Keyword/${environment.key}/${genre}`).pipe(
  //     map(titles => titles.items.map(title => this._objToKeywordModel(title)))
  //   );
  // }
}
