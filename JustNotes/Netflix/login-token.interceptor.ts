import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor
  } from '@angular/common/http';
  import { Injectable } from '@angular/core';
  import { Observable } from 'rxjs';
  
  // Services
  import { UserService } from '../services/user.service';
  
  @Injectable()
  export class LoginTokenInterceptor implements HttpInterceptor {
  
    constructor(private _userService: UserService) {}
  
    intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
      const req = request.clone();
      /**
       * next: gestisce la richiesta http rigirandola al prossimo interceptor.
       * next.handle()= prenditi questa richiesta e passala all'interceptor successivo;
       * se è l'ultimo, fai partire la chiamata.
       * 
       * Gli int vengono considerati TUTTI. 
       * Quelli con la gestione dell'httpReq gestiscono le richieste prima che partano, 
       * con httpResponse prima che arrivino sul tuo servizio
       * Eg: aggiungi content type perché il BE needs it (application JSON - utf8)
       * Saranno tutte cose che servono al BE per far partire questa chiamata. 
       * Potresti cambiare TUTTO con un int. Se poi al BE non serve x che gli aggiungi, 
       * stica. 
       */
      
      if (this._userService.hasToken()) {
        req.headers.append('Authorization', `Bearer ${this._userService.getToken()}`);
      }
      
      return next.handle(req);
    }
  }
  