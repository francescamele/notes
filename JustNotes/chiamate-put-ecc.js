//Il PUT vuole TUTTA L'ENTITA': se voglio modificare il commento, 
//glielo devo dare TUTTO. Se non passi interamente il commento ti
//dà errore classe 400: risposta non strutturata correttamente.
//PUT: solo per visualizzare i dati.
//POST: creare i dati.
//PATCH: non vuole che trasferisci l'intera entità, quando fai la 
//      modifica. 
//      EG: hai tabella con ordini di un e-commerce, con mille
//      dati, tra cui stato dell'ordine: quando vuoi passare quest
//      info, gli passi solo questa info, mentre invece con il PUT
//      gli avresti passato tutto
/**
 * HEAD: serve a prelevare gli header di una richiesta, equivalente 
 * in GET. S
 */


//Dobbiamo descrivere l'oggetto: metodo PUT, che come POST
//lavora inserendo le info nel body, & needs headers.