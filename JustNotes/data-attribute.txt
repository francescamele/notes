    // commentsSection.innerHTML += `
    //     <div class="row">
    //         <div class="col.lg-12">
    //             <h3>${c.name} <span class="delete-comment" data-id="${c.id}">X</span></h3>
    //             <p>${c.body}</p>
    //             <p>${c.email}</p>
    //         </div>
    //     </div>
    // `;
    //^span class="delete comment": per poter eliminare l'elemento
    //^data-id: è un data attribute, vedi slide. 

    //DATA ATTRIBUTE
    //L'id dev'essere univoco, non posso avere due id uguali: data-id crea un attributo 
    //customed di tipo id, ma non è un id di tipo html, bensì un attributo con nomi 
    //arbitrari che non andranno mai in conflitto con le norme html.
    //Quindi quando farò clic sullo span, mi cercherò nei suoi data attribute un id, 
    //lo troverò e dirò al backend di eliminare il commento con quell'id

      //Tutti i data attribute vengono agglomerati in una chiave "dataset" presente
        //sull'elemento html
        
        //Quando devo eliminare un commento con id x, posso ricavarmi x in due modi:
        //1. Sfruttando il data attribute di id: e.target.dataset.id
        //2. Tenendo in consideraz che tutte le funz in JS sono closure: anche nella
        //   funz di qst ascoltatore posso accedere al param 'c' che contiene l'ogg Comment