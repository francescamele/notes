28.01
REC I

I servizi possono avere come dipendenze altri servizi

Business logic: tutto ciò che NON riguarda la gestione degli 
utenti. 

Servizi UTENTI: 
- prelievi info 
e quindi:
- metodi che ci risolvono il paradigma CRUD:
   create - read - update - delete

Il servizio utente lo usiamo come scusa per fare le
CHIAMATE

Fino a oggi, le chiamate le abbiamo fatte thr xhr (lavora a eventi) 
e fetch (lavora con promise).
Ma ang. ha un suo SERVIZIO per la produzione di CHIAMATE, quindi non 
usa nessuno dei due ma:

HTTP CLIENT
Questo è un servizio:
Non è un serv. disponibile all'interno di ang. da subito: è all'interno 
di un modulo, chiamato HttpClientModule. 
Il decorator NgModule ha una chiave "imports": serve 
ad attingere funzionalità da altri moduli, qualsiasi sia la provenienza, 
dal core di angular o altro. Quindi, se vogliamo sfruttare 
l'HttpClientModule dobbiamo importarlo nel MODULO PRINCIPALE e metterlo 
quindi nell'arr imports del decorator.
HttpClientModule è dentro @angular/common/http 

Le chiamate, quando ricevono una risposta, devi gestirle con una trasformaz 
del dato ricevuto: di solito prendevi ogg JSON e lo trasformavi in un 
modulo, con response.JSON, json.parsify ecc. Se arr, 10'50 ...
L'HttpClientModule è un servizio, quindi verrà iniettato da qualche parte: 
dove? Magari poi manderò a qualcun altro la risposta e me la trasforma. NON 
VA BENE, perché sennò ogni comp che needs it deve trasformarla. 
... 12'
We can work on dependencies called GERARCHICHE: 
Lo UserService DIPENDE FUNZIONALMENTE dall'HttpClient. 
DIPENDENZA FUNZIONALE: x needs y to work. 
DIPENDENZA OPZIONALE: cosa che, se riesci a fare una cosa, viene 
passata, sennò fai qualcos'altro 15'. Ma Andrea thinks it's bonkers. 
UserService deve fare chiamate: è chiaro che HttpClient sia necessario. 
Sennò devo dire che se ce l'ho lo uso, sennò uso fetch, sennò xhr. 
Quindi, anche i servizi possono avere iniettate delle dipendenze. 

Un servizio che dipende da un altro servizio riceve la sua dipendenza 
esattamente come un comp: lavori sul suo costruttore. L'INIEZIONE la 
facciamo mettendo, in user.service.ts: 

   export class UserService {

      constructor(private _http: HttpClient) {

      }
   }

in user.service.ts 
In console troverò:
- NULL INJECTOR ERROR
- ...26'
- NULL INJECTOR ERROR: no provider for HttpClient!
Questo terzo errore: il provider mi crea l'associazione token-dipendenza: 
qst mi sta dicendo che non c'è un provider per creare quell'associazione. 
Qst significa che non è stata creata questa dipendenza: ... 
Vado nell'app.module e aggiungo HttpClientModule agli imports in @NgModule:

   import { HttpClientModule } from '@angular/common/http';

   @NgModule({
      declarations: [
         ArenaComponent,
         ...
      ],
      imports: [
         BrowserModule,
         AppRoutingModule,
         HttpClientModule
      ],
      providers: [

      ],
      bootstrap: [AppComponent]
   })
   export class AppModule { }

Ricordati che in "imports" puoi mettere SOLO MODULI: serve a declinare 
i moduli da cui attingere funzionalità.

Quando si lavora con una DI, gli errori dell'injector vengono scatenati 
a runtime. 
Invertion of control: la risoluzione delle dipendenze non avviene ...33'
Prima viene creato HttpClient, che è l'el più annidato che viene risolto 
prima. 


STRUTTURA HTTPCLIENT 
In user.service.ts: 

   export class UserService {

      constructor(private _http: HttpClient) {

      }

      public list() {
         this._http.get()
      }
   }

Metti che in UserService voglio creare un metodo che va a prendere delle 
info, idealmente un elenco di utenti. 
Dovendo lavorare con HttpClient, non fetch o xhr, mi appoggerò alla prop 
_http. 
Che ci troverò in quest'istanza di _http, scrivendo il punto dopo di essa. 
Si chiamano come i verbi HTTP: visto che devo prendere info, uso .get(). 
Il primo parametro che DOBBIAMO mettergli è un url: 

      public list() {
         this._http.get('https://jsonplaceholder.typicode.com/users')
      }

43' ... ambiente produzione e link intero (che NON va bene, perché non 
dovresti riscrivere lo stesso link fino a .com/ uguale). 
Prende un array di el: come me li restituisce?
Finora abbiamo usato async/await o ci siamo agganciati con .then, anche 
perché dobbiamo schedulare un processo...
Il tooltip di .get() ci fa vedere che alla fine c'è un ": Observable<...>"

Promise: strumento col quale emettiamo UNA informazione: su una sorta di 
linea temporale, troveremo UNA sola emissione di dati. 
47' vedi video: al massimo potrei individuare un errore: o una cosa o l'altra, 
UNA info.
Col then, posso aggangiarmici sotto con un altro then, sfruttando il promise 
chaining: se ascoltassi ancora con un altro .then avrei ...

Gli Observable sono diversi dalle Promise perché NON sono LIMITATI all'emissione 
di UNA INFORMAZIONE: ne possono emettere più di una, e possono anche scatenare 
degli errori (di solito uno). È tipo un socket: ci permette di lavorare con 
uno stream di dati, dei flussi. 
Quando ha terminato di emettere i suoi dati, l'Observable è CONSUMATO: 
- quando termina tutte le emissioni o 
- quando genera un errore. 
In questi casi chiude il canale trasmissivo e lì finisce. 

L'Observable nasce come parte di una libreria chiamata ReactiveX: questa 
serve per semplificare l'Observable pattern (detto anche Stream pattern). 
Ang. è interamente sviluppato su questa libreria. 
È meglio degli strumenti nativi che ci danno i ling progr. 
Di ReactiveX esistono tanti 52? 
Automaticamente imparerai a utilizzarla anche in altri linguaggi: le API di 
ReactiveX sono le stesse in tutti i porti. 

Per usarla, facciamo questo import: 

   import { Observable } from 'rxjs';

   const o = new Observable(observer => {
      observer.next(1);
      observer.next(2);
      observer.next(3);

      setTimeout(() => {
         observer.next(4);
         observer.complete();
      }, 4000);
   });

Il costruttore dell'observable prende 1 funzione, il cui unico param è 
observer: riferimento a chi ascolta. 
55' ^
Se lavorassimo con un socket e volessimo propagarne le info con l'observable 
ogni volta propaghi roba nuova. 
observer.complete() chiude lo stream dovunque esso sia: se mettessi un altro 
.next() dopo di esso ti darebbe errore.
Per poter ascoltare le emissioni: 

   const o = new Observable(observer => {
      observer.next(1);
      observer.next(2);
      observer.next(3);

      setTimeout(() => {
         observer.next(4);
         observer.complete();
      }, 4000);
   });

   o.subscribe(data => console.log(data));

^L'el emesso lo metto in 'data', e con un console.log lo mostro nella console. 
o è l'observable: chi lo ascolta? -> il subscribe
Se nessuno si registra ad ascoltarlo, l'observable non emette un piffero. 
Senza il subscribe(), il canale trasmissivo non esisterebbe: crei l'oggetto, 
l'istanza, ma non ci fai niente. Nel momento in cui fai subscribe() ti 
metti sulla sponda del fiume col retino e raccogli la pallina che il fiume 
trasporta. 

   const o = new Observable(observer => {
      observer.next(1);
      observer.next(2);
      observer.next(3);

      setTimeout(() => {
         observer.next(4);
         observer.complete();
      }, 4000);
   });

   o.subscribe(data => console.log(data));
   o.subscribe(data => console.log(data));

LIMITI 
Obs. è uno strumento UNI-CAST:
Ci può essere AL PIU' UN ASCOLTATORE. Se io scrivo due subscribe, il secondo 
riceve dati diversi rispetto al primo. Te ne rendi conto mettendo il 
timestamp: vedi che prima vengono eseguite le istruzioni alla riga del primo 
subscribe. ?
v. quad. !!
I timestamp delle due chiamate sono diversi. 
Se invece fosse MULTICAST, i timestamp sarebbero uguali. 
Emettitore multicast => tutte le sorgenti in ascolto ricevono lo stesso dato. 
Se fosse multicast, l'observable emetterebbe UN SOLO ogg. a tutti gli 
ascoltatori. 
ES. 1h12-13' ASCOLTALO
Quando fai un'eliminaz emetti un dato con l'observable, e con l'unicast ha 
senso solo se l'ascoltatore è uno. 
Il multicast usa dei buffer: mantiene in memoria delle info. Ci sono diversi 
tipi di subject; eg with replay subjects you can stabilire quante emissioni 
può ricordare? 1h14'. 


Se abbiamo un observable, sappiamo that we need a subscribe to listen to it. 
BUT gli elem che ci restituisce sono un JSON, mentre noi vorremmo che al 
comp arrivassero i dati già elaborati: è nel servizio che dobbiamo trasformare 
le info in ogg coerenti col comp. 
Quindi mettiamo da parte il soggetto subscribe per ora, per capire come 
trattare le info dello stream nell'observable.

Una chiamata http ci dà 
1h25'50 ricomincia
Se la risposta alla domanda del backend è una, nello stream abbiamo una sola 
emissione: quello è il dato che ci risponde il backend. Fosse stato un 
socket, magari 10 ogg con 10 utenti separati. E invece no. 
Gli observable hanno anche un altro metodo: .pipe()

.PIPE()
Qst metodo ci permette di ELENCARE UNO O PIU' OPERATORI. 
Gli operatori fanno un subscribe allo stream precedente, in automatico. 
Ciascuno crea uno stream parallelo: qst ci dà una forte garanzia rispetto 
alla tutela del dato che è stato emesso in precedenza: se applico un operatore 
dopo, in cascata, l'arr originale non verrà intaccato, perché andrò a 
lavorare su una sorta di copia. 
Vedremo due operatori:
- map
- filter
Questa nomenclatura è super diffusa nei metodi delle librerie, come quelli 
degli array. 
Map: in arr, applica una callback a ciascun el dell'arr che gli passo; 
     sull'observable applica una callback che gli passo a ciascuna 
     emissione dell'observable. ...1h29
Quindi posso scegliere un operatore per trasformare l'ogg nello stream in 
un'istanza di model ?

v. quad. [3]
map(data) ...1h32'
L'HttpClient fa il parsify da solo, in automatico, sulla risposta. ?
1h39 riprende
users è un arr, già trasformato.

quindi: 

   export class UserService {

      constructor(private _http: HttpClient) {

      }

      public list() {
         this._http.get('https://jsonplaceholder.typicode.com/users').pipes(
            map(users => users.map(user => new User()));
         )
      }
   }

1h47': creiamo un nuovo stream, parallelo a quello di prima. 
Stiamo trasformando l'arr in ogg. Col map, nello stream parallelo, avrò una 
risposta di ogg e non array ?
riascolta e scrivi quello che dice Alberto.
L'originale non lo gestisci tu, ma lo gestisce il metodo. 
Ma c'è un errore nel .map di users. 
1h52': non sa che tipo di risposta ti darà questa chiamata prima del runtime, ma 
a me serve a compile time: glielo devo dire io prima. Lui mi dice che map non esiste 
su Object perché Object è l'el principale da cui derivano tutti i datatype di js, 
quindi lui dà per scontato che sia quello. Glielo devi dire tu che è un array. 
Quindi: 

   public list() {
      this._http.get<[]>('https://jsonplaceholder.typicode.com/users').pipes(
         map(users => users.map(user => new User()));
      )
   }

Ma errore su User: al costruttore devi dare delle info:

   public list() {
      this._http.get<[]>('https://jsonplaceholder.typicode.com/users').pipes(
         map(users => users.map(user => new User(user.id, user.email, user.name)));
      )
   }

Ma dà errore su id, email e name:

   public list() {
      this._http.get<User[]>('https://jsonplaceholder.typicode.com/users').pipes(
         map(users => users.map(user => new User(user.id, user.email, user.name)));
      )
   }

^Questo funziona, e viene usato tantissimo come soluzione, ma fa cagare.
Lo user dentro users.map NON è ancora un model: è l'el arr da trasportare. Quindi, 
se metto in User un getter "readable()", in user non c'è. Ma se lo metto in user.ts: 

   export class User {
      constructor(
         public readonly id: number,
         public name: string,
         public email: string
      ) { 
         //
      }

      get readable(): string {
         return `User called ${this.name} with id ${this.id}`;
      }
   }

e poi in user.service.ts:

   public list() {
      this._http.get<User[]>('https://jsonplaceholder.typicode.com/users').pipes(
         map(users => users.map(user => new User(user.id, user.email, user.readable)));
      )
   }

NON mi dà errore, perché gli sto dicendo che la risposta è strutturata in ...
Non mi dà errore anche perché la p...
Neanche così:

   public list() {
      this._http.get<User[]>('https://jsonplaceholder.typicode.com/users').pipes(
         map(users => users.map(user => user.readable));
      )
   }

mi dà errore, perché gliel'hai detto tu che esiste.
VEDI VIDEO 

TS vede tutte le proprietà declinate in una classe e in automatico 
si crea un'INTERFACCIA:
schema che riassume le proprietà di un oggetto e i loro datatype: 
se dentro User c'è id numerico, name stringa, email stringa, per lui
la risposta conterrà quei quattro elementi. Ma così manca una marea 
di roba della risposta prototizzabile con tantissimi elementi che così 
ignoriamo. 
Ts ti crea automaticamente un'interfaccia a partire dalla definizione 
di una classe. Ma dobbiamo farlo raramente: infatti in questi scenari 
si crea l'interfaccia a mano. 
Quando usiamo la classe invece dell'interfaccia rischiamo di evidenziare 
proprietà nella risposta dal backend che in realtà non esistono nel 
backend.

2h40' ca ricomincia
REALIZZARE INTERFACCIA
Crea cartella Interfaces in app, con dentro file "user.interface.ts":

   export interface IUser {
      id: number;
      email: string;
      name: string;
      phone: string;
      username: string;
      website: string;
   }

"interface" è la parola chiave con cui creiamo interfacce in ang. 
La chiami IUser per avere I come contraz di interface, parola troppo 
lunga.
All'interno di un'interfaccia creiamo le chiavi. Guardando il JSON, vediamo che 
c'è una chiave address con un altro oggetto: invece di scriverlo qui, 
crei un'altra interfaccia, IAddress.
Poi, sul servizio: dico che la risposta sarà strutturata in oggetto arr, e 
aggiungo l'import di IUser:

   public list() {
      this._http.get<IUser[]>('https://jsonplaceholder.typicode.com/users').pipes(
         map(users => users.map(user => new User(user.id, user.name, user.phone)));
      )
   }

A questo punto, in new User() decidiamo noi cosa metterci. Ma devono comunque
essere tre param, perché nel costruttore di User ne abbiamo messi solo tre. 

Gli operatori messi in cascata restituiscono uno stream parallelo, 1h54'50, 
ma quando invoco il metodo list nessuno riceve niente: non 
abbiamo messo return. Il metodo list così lancia solo un processo, 
restituisce un observable a cui non si attacca nessun subscribe, 
quindi non emette nessuna info. 
Col return, quale observable viene restituito? Perché stiamo restituendo 
il risultato del metodo get.
L'observable finito che viene restituito dal metodo get() è quello che 
viene dato dall'operatore map (il primo map()): è lo stream restituito 
da questo map che l'ascoltatore vedrà, l'ultimo operatore messo nel pipe.
Chi invoca list() riceverà un observable, cioè lo stream parallelo 
creato da map().
2h57'18

Quindi in users-table.component.ts:

VEDI VIDEO per cambi su table.component.ts 3hca.

   public users: User[] = [];

^"User con la U maiuscola..." 3h04


REC II 
p.3 slides

Comp e direttive di ang., dal momento in cui ... terminano l'esecuz del costr 
passano per una serie di stati che determinano la valorizzazione di 
alcune proprietà al verificarsi di alcune condizioni. 
Questo tipo di segnali che manda a. sono i 
LIFECYCLE CALLS:
richiami/agganci ad alcuni momenti del ciclo di vita del comp. 
Adesso vediamo dei MOMENTI del ciclo di vita della direttiva in cui qst 
istanza passa. 
Sulle slide, a p. 2, vedi i vari ?03
Quelli verdi sono gli hook del lifecycle condivisi sia da comp che dir. 
Quelli in azzurro sono gli hook del lifecycle riservati ai comp. Qst 
perché si riferiscono a situaz particolari che avvengono nel template.

Ci agganciamo agli hook del lifecycle thr 05'50 ?
Ciascun hook ha un'interfaccia che possiamo implementare sul comp. 
Eg OnInit 06'40
...
ngOnChanges: primo hook che eseguiamo. 
Particolare perché si può scatenare più volte durante il ciclo di vita 
del comp: viene eseguita ogni volta che una proprietà decorata con Input 
riceve prop dall'esterno ...

slide 4: ngOnChanges verrà eseguito? ogni volta che prop verrà valorizzata 
dal comp padre.
Al suo interno troverai una sola chiave il cui nome coincide con la prop 
decorata con Input.
Si scatenerà, cioè verrà eseguito il metodo ngOnChanges, solo quando prop 
verrà valorizzata dal padre. 
Se prop = 7 e poi il padre la valorizza con 4:
previousValue: 7
currentValue: 4 
firstChange: true
Se dopo questo il padre la valorizza con 5:
previousValue: 7
currentValue: 4 
firstChange: false

...13'?
se quest'interfaccia OnChanges è implementata qui, significa che 
devo dichiarare i metodi esattamente come sono descritti nella sua 
interfaccia. 
VEDI VIDEO
In console dovrei trovare prima undefined, e poi dopo il costr passa 
il primo utente, che è il primo valore e anche il primo cambiamento.

22': esecuzione ngOnInit
Certamente eseguito dopo tutti gli ngOnChanges: tutte le propr decorate 
sono state valorizzate dall'esterno, e ...?
!!!

ngOnInit è anche il posto migliore dove far partire chiamate a 
servizi esterni: non punta solo a endpoint, it may also need info 
coming from comp father, e ha senso farla solo quando quelle info 
ce le abbiamo ?.
Tutte le chiamate a servizi esterni thr servizi vengono definiti 
qui dentro. 
Quando facciamo subscribe su un observable è un po' come se qst ci 
dà un biglietto, e lui si tiene il taglioncino col numero. Quando non 
vogliamo più ascoltare i dati, sulla subscription invochiamo il 
metodo unsubscribe(), e lì chiudiamo la porta. 
26' su unsubscribe
Senza mettere il subscribe in una proprietà, non posso fare 
unsubscribe.

GARBAGE COLLECTOR: sistema che, thr 3 algoritmi condivisi ?, serve 
a gestire la memoria della ram quando perdiamo scope su un el ?.
Ma non è detto che gli el vengano eliminati subito. 
Se un observable propaga i dati di un socket, riemettendoli; magari 
distruggo il comp, perdo lo scope dell'observable e la sua subscription;
quello in memoria continua a gestire l'ascolto, e quindi occupa 
risorse sulla ram, e potenzialmente non le vedo a parte un picco di 
saturazione. 
Per evitare questo, ?
Su ogni observable invoco un unsubscribe: se non 
c'è un el che ha fatto il subscribe, l'observable non emette. 


Ang. crea una sequenza alfanumerica a partire dallo stato che hanno 
i comp: tutte le proprietà in input vengono lette quando si verificano 
dei cambiamenti di stato. 
La sequenza rappresenta il valore delle propr in input al momento 
del cambiamento (quindi cambiate): se arriva un'info dall'esterno 
a. può paragonare i due hash e vedere se ci sono i cambiamenti. 

...39
!
Però, ogni volta che si verificano certe situaz--eventi sul DOM, 
observable che emettono...--controlla se si sono verificati 
cambiamenti, e se sì aggiorna il template.
Uno di questi stimolatori sono el che arrivano dall'esterno: una 
valorizzazione di una prop input. 
Ma ...?
-> ngDoCheck: viene scatenato al termine di ogni lifecycle sul 
comp, quindi vedo se ci sono stati dei cambiamenti. 
IterableDiffers: map avanzato... 40'


NGONDESTROY
JS implementa il costr ma non il distruttore: quando distruggo 
una classe, non posso fare nulla ?
ngOnDestroy 
?
... fino a 46'
In angular usiamo ngOnDestroy per fare l'unsubscribe.


Il $ si usa per memorizzare una subscription o uno stream.
48'... :( ._.

Gli hook dei lifecycle sono metodi pubblici. Mai specificato 
perché in js tutto è public, però scrivilo. 

Quale zona della ram viene attenzionata dall'ngOnChanges per 
vedere se ci sono state modifiche? 
57'
Vedo il cambiamento ma non leggo niente in console: ngOnChanges 
rileva i cambiamenti nello STACK, non nello heap.


ENVIRONMENTS
Hai due file, per environment diversi. 
Sono file con una costante che si chiama environment.
All'interno c'è un ogg con delle chiavi.
Premessa: tra diversi ambienti, le chiavi dentro questi ogg 
devono essere le stesse. Perché se nell'ambiente di test alla 
chiave API ci metti url, stringa, con indirizzo x, in produzione 
saraà una e sviluppo un'altra stringa, ma la chiave dev'essere 
la stessa. 
Quindi se aggiungo chiave api, quella dev'essere uguale per 
entrambi i file. 
Come distinguere tra un ambiente e un altro? 
Basta importare environment dove ti serve: nei servizi. La prendiamo 
dal file di SVILUPPO: da questo, a. cerca env. di sviluppo e ci 
mette 'prod': la costante che si chiama allo stesso modo farà 
solo uno scambio del nome del file. 

Da qlc parte c'è un file che dice: ogni volta che 1h07
