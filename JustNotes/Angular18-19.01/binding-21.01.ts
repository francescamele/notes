/**
 * 21.01
 * REC II
 * Da 15' (prima spiegazione per gruppo Ari)
 * 
 * EVENTEMITTER
 * EventEmitter è un OGGETTO ISTANZA: devi crearlo tu con 
 *      new EventEmitter()
 * - Quando crei un ev custom in un comp, devi memorizzare l'istanza 
 *   all'interno di una proprietà del componente.
 * - Quando lo vai a tipizzare, devi usare questa notazione: 
 *      p: EventEmitter<x>
 *      Dove x è il datatype del payload dell'evento
 * Visto che nell'ev click hai un tot di info, legate all'ev, in 
 * questo caso decidiamo noi quali sono le info che diamo a chi 
 * ascolta. 
 * Eg, possiamo mettere un numero: num che ha raggiunto il counter 
 * e che può essere una soglia massima per il counter. Quindi 
 * metteremmo number, ma può anche essere string, o boolean. Se 
 * vuoi che sia un oggetto, ti fai una classe, la chiami user, e 
 * poi scrivi EventEmitter<user>.
 * 
 * Perché funzioni, questa proprietà dev'essere preceduta da:
 *      @Output 
 * 
 * DEVI IMPORTARE EventEmitter da @angular/core. 
 */
@Output() public limitReached: EventEmitter<number> = new EventEmitter;

/**
 * Il metodo emit() serve a propagare l'evento.
 * Prende, in input, un parametro: il ...27'
 * 
 * ...32
 * 36' vedi notebook
 * 
 * 37'?
 * 
 * Le proprietà decorate con @Input ricevono ... DOPO la lettura 
 * del costruttore 43'
 * ... ascolta
 * Se le proprietà in input non sono inizializzate nel costruttore, 
 * vuol dire che la funzione/metodo inizializzato in @Input è un 
 * processo schedulato nella Task Queue. 
 * 
 * 47'?
 * $event mostra il payload dell'ev.
 * Quando creiamo noi degli ev custom, tipo LimitReached, non 
 * cambia niente. 
 * Con limitReached.emit(this.i) ho propagato this.i: per prendermi 
 * quello che passi al metodo emit, cioè il payload dell'ev, cioè in 
 * questo caso this.i, scrivi: 
 * 
  <app-counter (limitReached)="handleLimitReached($event)" [i]="2">
 * 
 * 56' payload
 * 
 * 57'28
 * Quando succede qlc a. aggiorna il template. 
 * i è una propr che contiene un valore: se muta, non è che a. sta 
 * a farsi le pippe di attenzione per cambiare il template. Qlcs di 
 * così forzato risulterebbe in una sorta di 
 * POLLING:
 * il sistema guardava compulsivamente tastiera e mouse finché non 
 * gli arrivava un input. 
 * Poi si è spostato tutto sugli eventi. 
 * a. ragiona su questo tipo di approccio: quello che scatena questi 
 * controlli sono:
 *  - gli eventi: quando si scatena un evento, a. controlla il 
 *    template per vedere se deve aggiornare qlc. 
 *    Ad ogni ev, a. controlla il suo stato guardando l'hash del 
 *    template. Se qst rimane uguale, non aggiorna il template; se 
 *    è cambiato, rielabora il template.
 *  - l'interpolazione
 * Questo è un approccio CONSERVATIVO: non rielabora il template ogni 
 * volta. Rielabora l'intero template del comp solo se trova un hash 
 * cambiato.
 * Se l'hash di un component cambia, viene controllata tutta 
 * l'alberatura, a partire dalla root. 
 */
