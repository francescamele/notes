03.02 REC I 
RISPOSTE DOMANDE 

Tutto era un two-way databinding in Ang JS: se avevo comp uno 
dentro l'altro, sulla funzione (erano fn, non classi: tt JS), 
potevi iniettare cose (come coi costruttori delle classi): 
c'erano mille cose col $ tipo $event, tra cui $scope: nei 
controller (equivalenti di attuali comp) dovevo dire $scope.p 
(dove p è la proprietà) per farci cose?2'
Dentro c'era una proprietà "parent": dal controller 1 potevo 
modificare cose nello scope del controller 4. Non va bene 
perché qualsiasi controller poteva modificare lo stato di 
qualsiasi altro controller. Fare il debug diventava 
complicatissimo. 

Quindi su ang. il binding unidirezionale è nato per evitare 
questo tipo di salti: dovrei mettere property ED event binding 
tra uno e l'altro e l'altro...
...04-5
Adesso, se voglio far comunicare comp1 e comp4, uso i servizi. 
Il subject può far comunicare diversi comp tra loro, ma è una 
comunicazione isolata su un el, cioè quello che il subject 
emette. 
Inoltre, coi servizi hai traccia di quello che fai, puoi andare 
a risalire a quello che hai fatto: hai una logica centralizzata, 
so che la logica di emissione è all'interno del servizio, 
perché quello che fanno i comp è semplicemente di dire al 
servizio di propagare. 

ABSTRACT CONTROL 
C'è perché sono vincolato a farlo: la firma di un ValidatorFn 
vuole un AbstractControl.
La ValidatorFn può essere applicata anche ai form group nella 
reactive-driven form, e quindi vuole la classe astratta. 


FORM REACTIVE DRIVEN 
Creo un gruppo principale con dentro altri form control: 

    const g = new FormGroup({
        'email': new FormControl('', [
            Validators.required,
            Validators.email
        ]),
        'passwords': new FormGroup({
            'password': new FormControl('', [
                Validators.required,
                Validators.minlenght(8)
            ]),
            'passwordConfirm': new FormControl('', [
                Validators.required
            ])
        })
    }, [
        equalValidator('password', 'passwordConfirm')
    ])

    <form [formGroup]="g">
    </form>

Anche il FormControl può accedere agli el del suo gruppo, con un 
metodo parent, ed eventualmente confrontarsi. 
V. pics on phone 