REC I

SINGLETON PATTERN
Definire questa classe "static": i suoi metodi e propr 
saranno anche static, che finiranno in una zona della ram 
chiamata anch'essa static. Come può essere accessibile in 
tutto l'applicativo? 
Non ci riferiamo a un'istanza, non faremo "new Static": 
02 ... UserService::get
...????
Per es per JSE: legge codice, individua parole chiave e le 
elabora, POI legge tutto il resto. Qui, individuerebbe gli 
el dichiarati ...?
Approccio singleton: esiste UNA SINGOLA OCCORRENZA (NON istanza, 
che si crea con new e ne posso avere tante) della classe Static
...
Il problema di questo approccio è che questi singleton occupano 
memoria a prescindere, anche quando non vengono usati, che 
devono essere disponibili nel global scope. 

Riascolta tutto

var us = new UserService
^Questo non eliminava il problema ma lo spostava soltanto in un 
altro punto della memoria: continuava a occupare quello spazio. 

Bisognava trovare un modo per creare le istanze solo quando 
servono. 
Immaginala come una scatola: 
Apro Amazon: che mi serve?
- Servizio utenti 
- Servizio prodotti
- Servizio categorie
Passo a un'altra pagina (facciamo finta che sia una single page 
app e che questa roba NON venga distrutta cambiando finestra): 
apro un dettaglio di un product: I need a comp che mostra le 
info del product. Qst comp, appena caricato, legge le info del 
product and knows it needs the product service. Comp becca l'id 
prodotto e lo passa al comp? 13'50...
A runtime verrà creato il servizio del carrello. 
Quando passiamo dall'homepage al dettaglio, il servizio del 
prodotto era già nella scatola: ha usato quello. 

(continua a riascoltare)

13':
LOGICA INTERAZIONE UTENTE: va in component. 
BUSINESS LOGIC: va in servizi.



Nuova spiegaz: 25'
Abbiamo una classe:

class DI {
    _#box

    get(token) {
        if (token in this._#box) {
            return this._#box[token]
        }
    }

    set(cl, fn, t) {
        this._#box[t] = fn(cl);
    }
}

Per uno user service, you need a GET method. 
token: è una stringa, ma è il param formale. 
#box: proprietà privata
Se la chiave che ho chiesto esiste all'interno di quest'oggetto, 
...27'
cl: definizione della mia classe: gli passo un intero UserService. 
funzione: dice "per creare uno UserService devi fare questo". 
?????
Il modo in cui questo new UserService viene fatto lo facciamo 
stabilire da una funzione (fn(cl)), che poi sarà il costruttore 
di questa classe. 
t: token, la chiave che metterò all'interno della scatola (31'). 
    Il token è un identificatore univoco.

Di base, una DI è una classe che contiene una proprietà che 
accumula i servizi, un metodo get e un metodo set.
Quando dai un servizio alla DI e questo servizo non esiste, la 
DI lo crea per noi. 
(La DI di Angular non sarai in grado di realizzarla, troppo complessa)

Per poter funzionare, le DI need sort of a big book: 
REGISTRO DIPENDENZE
Qst associa a ciascun token una dipendenza: quando io dico al 
token 'user-service', deve associarla alla classe UserService. 
'user-service' puoi chiamarla come vuoi, ma si preferisce usare 
la parola che si usa dopo "class", quindi il nome della classe 
stesso. 
Come si compone questo RD? 38'
Attraverso i 
PROVIDERS:
fornitore. Sono il primo elemento che serve a far funzionare una 
DI. In js può essere un oggetto, in PHP un array ...
P. mi serve a creare la coppia token-dipendenza.
Con i providers creo questo "libro", più o meno grande, che può 
essere utile all'elemento
INJECTOR:
el che si occupa di fare l'iniezione 
41 !!!!
...
L'Inj. si prende il token, consulta il registro dipendenze, e 
vede la classe con cui coincide. Esiste nella scatola? Sì: ti 
do quell'istanza; no: te la creo io. 
Inj prende token -> va nel DR -> trova "UserService" e trova 
il nome della classe -> va nella scatola e vede che l'istanza 
c'è -> te la passa. 
NON trova il servizio nella scatola:
1. Non trova il token.
    Gli hai passato un token che non conosce: Injector error. Non 
    trova il token nei providers, perché non hai mai descritto 
    quella dipendenza. Qst significa che hai scritto una cazzata 
    nei providers. Anche nei token, ma è raro che accada (ci farà 
    vedere perché).
2. Non trova l'istanza.
    Nella scatola non c'è UserService.
    Crea l'istanza UserService, la mette nella scatola, e te la 
    passa. 
    Per capire come funziona qst, dobbiamo legare il discorso alla 
    RAM: v. quad. Quando creiamo UserService, abbiamo un puntatore 
    nello stack che punta all'istanza che vive nello heap. Quindi 
    lo UserService nella scatola è un puntatore. QUINDI, la 
    scatola con tutti i puntatori è nel global scope, così è 
    raggiungibile dappertutto. 

* * SVUOTAMENTO SCATOLA * *    
51' Garbage collection: questi algoritmi intervengono quando 
perdo lo scope su un el.
La scatola è un oggetto, dove token è chiave e istanza è valore 
53'...
* *         ***         * *

Finora abbiamo visto la DI FLAT.
54'
Tre elementi fondamentali:
- Providers: elenco associazioni token-dipendenza (la forma 
             dipende dal linguaggio di programmazioni)
- Injector: el che legge il registro dipendenze e, in base al 
            token che gli chiedo ...55'
- Dipendenza. 
