/**
 * 18.01 REC I
 * 
 * Tre framework più conosciuti per le client side di SPAs sono:
 * Angular - sviluppato da google
 * React - facebook
 * Vue.js - sviluppatore giapponese fuggito da google, tipo A.
 * Si usano per le:
 *  
 * SPA
 * Single-page application
 * 
 * Sono state sviluppate perché o abbiamo un server a cui facciamo una 
 * richiesta e questo risponde in html, css e js, poi noi lo lavoriamo;
 * o sviluppiamo noi diverse pag html che gestiamo col js, ma poi ogni 
 * volta che passiamo da una all'altra il dom si svuota e si perdono i 
 * dati, vediamo schermo bianco.
 * Da un po', la direzione è verso lo sviluppo di app che caricano una 
 * volta, con UNA richiesta al server per scaricare roba, e poi una 
 * porzione dello schermo ha il compito di cambiare il contenuto in base 
 * ai link di navigazione.
 * 1. Meglio scaricare tanti dati con 1 richiesta che scaricarne pochi
 *    un po' alla volta.
 *    Vantaggio pratico: a questo modo il dom non viene mai distrutto. 
 *    Una var nel global scope mi campa sempre a meno che non refresho. 
 * 2. UI: nn c'è + il tempo morto con lo schermo bianco: posso fare 
 *    richieste a un backend, prendere solo la struttura dati, non tutto 
 *    il CSS e html, e gli dico cosa mostrare, magari uno spinner o un 
 *    "Try again later". Posso gestire le situazioni con un setTimeout 
 *    che manda un messaggio del genere.
 * 
 * Angular x applicazioni lato client: useremo typescript: js ci 
 * servirà per React, o meglio React Native, che è per sviluppare app 
 * per cellulare (quello che permette Angular però lo permette anche 
 * React).
 */