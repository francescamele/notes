REC III 
Da 2' ca

Creazione componente login

FORM 
Due tipi di form:
1. TEMPLATE-DRIVEN FORM: asincrono: prevalentemente, lavora per 
   direttive. 
2. REACTIVE-DRIVEN FORM: sincrono: lavora per istanze (del form) 
   create dallo sviluppatore. 
Da una parte, 1 si occupa da solo di sviluppare l'istanza del form, 
mentre in 2 lo dobbiamo fare noi. 
1.: circa 90% del codice sarà nel template, 10% nel comp.
2.: circa 50-50%
Non è detto però che il codice da scrivere sia della stessa quantità: 
con 1 scriviamo meno. Inoltre, gli sviluppatori di solito conoscono 
meglio 2, perché è simile al ? 7'

Perché due approcci? Dipende da quello che ci devi fare. 
1. è semplice e veloce, al prezzo di non poter add or remove, 
quindi di non poter rendere il form dinamico a runtime. 
Eg, form costruito a partire da info sent from BE: i campi, il relativo 
? del campo in input, detto dal BE: for this, we need 2.


TEMPLATE-DRIVEN FORM 
Molto simile all'approccio usato in html5
Primo: creo link di navigazione. 
...fino a 12, da 9?
Questo import andrà a ? tutte le occorrenze di ? e le cambierà, 
aggiungendo qualcosa. Altera tag form, campi in input, perché 
metterà info non tipiche di html ma che servono ad angular per 
fare cose. 

Poi vai in login.component.html
21 ricomincia 
Ogni campo in input DEVE avere attributo name: la stringa che 
dai al name sarà la chiave, e il campo in input sarà il valore 
(o meglio, il ? che avvolge il campo in input).
Stessa cosa per password. 
- Email 
- Password 
- Button 
Due domande: 
1. Come ascoltare l'evento submit? -> con l'eventListener. 
L'evento da ascoltare NON è submit, ma ngSubmit: l'evento submit 
NON va bene per ascoltare l'evento del form, perché prova a 
ricaricare la pagina. 25'30
ngSubmit, invece, in automatico si prende il $event di submit e 
invoca il preventDefault() da solo. 

Il nome handleSubmit() è arbitrario, come sempre. 
Quando lo scriviamo nel comp, come tutti gli handler restituisce 
niente: void. 
Nell'approccio template-driven mi faccio UNA PROPRIETA' PER 
CAMPO INPUT. Queste accoglieranno, man mano, ciò che l'utente 
scrive all'interno dei campi in input. 

TWO-WAY DATA BINDING: v. appunti su login.component.html 
...ascolta fino a 56' (e oltre)
1h01...
required email/required minlenght="8" -> proprietà html5. 
ngModel è una direttiva.
Esistono due variabili create da a.: 
- ngForm: riferimento all'istanza del form: quando ang si legge 
  il form crea un'istanza particolare e la mette all'interno di 
  una costante: ngForm. 
- ngModel
In <form> scrivo: #f="ngForm": mi sto prendendo l'istanza creata 
da a. Invece se scrivessi #f prenderei il form a livello di html.
Il cancelletto ha lo stesso valore di let: serve per creare 
variabili sul template.  
Adesso hai la variabile f che punta all'istanza di ngForm. 
handleSubmit() è un metodo: gli passo f

    handleSubmit(f)

^Così invio il form al submit 
Domanda: quando devo inviare il form? Quando 1h12' 
in 
    handleSubmit(form: NgForm): void {
        if (form.invalid) {
            return;
        }

        console.log(this.email, this.password);
    }

form.invalid = ALMENO una delle regole del form NON rispettata. 