/**
 * 25.01 REC I
 * 
 * Quando fai moduli, UN modulo per file, NON più moduli nello stesso 
 * file.
 * 
 * user.ts: classe User.
 * Cosa cambia rispetto al js? Cambia che abbiamo il tipo sui parametri 
 * formali, quindi se abbiamo:
 */
constructor (id: number) {
    this.id = id;
}
// In ts abbiamo gli acces modifier: this.id si riferirà a una 
// proprietà presente magari prima del costruttore, che sarà:
public id: number;
/**
 * Per evitare questa ripetizione, ts introduce le:
 * SUGAR SYNTAX:
 * tipi particolari di sintassi che permettono di contrarre le 
 * espressioni.
 * Nel costruttore dichiaro una propr di tipo numerico, ma dico anche 
 * che è public. Quando metti un access modifier nel costruttore, 
 * automaticamente si deve creare/dichiara una proprietà con quel 
 * datatype, se la dichiara, e quando istanzieremo l'oggetto il 
 * costruttore passerà quella proprietà all'ogg. Sposta quello che 
 * in js era nel corpo della funzione nei parametri formali. 
 */
// Quindi questo:
constructor (public readonly id: number) {}
/*
 * Nel costr c'è una propr "public readonly id: number"
 * In questo caso, "readonly" significa che non può essere sovrascritto. 
 * Gli id di solito lo sono perché non possono cambiare, per es perché 
 * sono indicizzati, anzi, quando sono chiave primaria (di solito) sono 
 * clusterizzati, cioè indicizzati anche meglio delle altre chiavi. 
 * 
 * In questo esempio^, basta una riga di codice invece di 3 (quella della 
 * propr, quella del parametro, e quella di this.id).
 * Il valore si assegna in automatico: scrivendo "public id: number" 
 * nella firma del costruttore, si crea questa riga, e quando istanzi 
 * l'oggetto il primo valore sarà l'id. 
 */
constructor (
    public readonly id: number,
    public status: string,
    public amazonOrderNumber: string,
    public amazonReviewUrl: string,
    public createdAt: Date,
    public updatedAt: Date,
    public product?: Product | number,
    public user?: User | number
) {
    //
}
/**
 * Quel "?" significa che il parametro è opzionale: dentro user o 
 * troverai un ogg user o un numero (in quel caso sarebbe l'id dello 
 * user).
 * Se poi non gli passi nulla, automaticamente troverai undefined, che 
 * per lui va bene, perché matcha col valore che gli dà il jsEngine. 
 * Ma non lo darai tu mai a mano, piuttosto potresti 
 * lavorare con data.product: non esiste la chiave, mi dà undefined, 
 * e va bene perché c'è il "?". 
 * 
 * 
 * ESERCIZIO
 * 1 arena component
 * 1 hero component con 2 istanze di hero component 
 * 1 model Hero
 * Questi due model conterranno: 
 *  - nome pg
 *  - img se riesci
 *  - p.ti vita rimanenti
 *  - danno
 *  - bottone per attaccare: al click, qst pg deve attaccare l'altro. 
 * Trova un modo per far comunicare i componenti hero. 
 * I due pg, il model, creali dentro l'arena. 
 * 
 * Crei Hero comp, crei Arena comp, dentro l'arena metti i parametri 
 * per instanziare new model. 
 * Il costr del componente NON LO DEVI TOCCARE. 
 * @Input()
 */

/**
 * 2h21
 * SOLUZIONE 
 * 
 * Va bene mettere hero.comp inside arena.comp, ma SOLO se hero lo 
 * userai ESCLUSIVAMENTE da dentro arena.
 * La prima cosa che fai è inserise <app-arena> dentro <app-component> 
 * e verificare che funzioni.
 * Passo successivo: introdurre i due componenti hero.
 * Dentro arena, cancello <p>arena works!</p> e scrivo <app-hero> 
 * due volte. Se non ricordo il nome, apro il component e leggo il 
 * selector. 
 * Controllo di leggere due volte "hero works": funzionano. Da adesso 
 * parto a lavorare. 
 * 
 * Il comp hero deve mostrare info: posso accogliere un intero eroe 
 * in @Input() dentro hero: arena passerà a ciascun comp hero una 
 * istanza di hero.
 * Ts impone l'inizializzazione delle proprietà anche quando vengono 
 * decorati con @Input, perché quando vengono valorizzati così NON 
 * succede appena si inizializza new Hero, ma un po' dopo: ci vuole 
 * il "!" prima dei ":" per poter far aspettare ts. 
 * 
 * Dentro Arena, devo avere due proprietà: hero1 e hero2. 
 * Sul selettore <app-hero> posso fare un property binding: 
 * 
    <app-hero [hero]="hero1"></app-hero>
    <app-hero [hero]="hero2"></app-hero>
 * 
 * Nel comp di Arena scrivo:
    public hero1: Hero = new Hero(Ciccio, picLink, 25, 27)
    public hero2: Hero = new Hero(Puffo, picLink, 35, 17)
 * 
 * Queste due istanze verranno passate da Arena alle singole 
 * istanze di hero.comp.
 * Se non ho errori, vado nell'html di hero.comp: voglio mostrare 
 * il nome del pg: nomeProprietàDecorata.nomeProprOgg, quindi 
 * hero.name (=>Ciccio per hero1, Puffo per hero2)
 * 
 * In hero.html ci vuole un (click)="handleClick()" in un bottone. 
 * In hero.ts ci metto handleClick(): void
 * 2h36' su comunicazione figlio-padre per la funz:
 * Ciascuna istanza di HeroComponent ha accesso al SUO personaggio, 
 * che è quello che attacca, ma non ha modo di accedere al pg che 
 * subisce i danni: quindi devo trovare un modo per comunicare 
 * all'arena quale pg sta attaccando, perché questa è L'unica info 
 * che mi è stata passata e quindi l'unica che posso comunicare.
 * 
 * IMPORT EVENTEMITTER/INPORT/OUTPUT PORCA TROIA
 * 
 * EVENTEMITTER
 * Nelle "<>" dell'EventEmitter inseriamo il payload dell'evento: 
 * l'elemento che andiamo a propagare emettendo l'evento che stiamo 
 * creando. L'unica info che possiamo emettere è l'ogg hero, quindi 
 * gli mettiamo come payload "Hero".
 * L'Eventemitter è un oggetto istanza: lo dobbiamo inizializzare con 
 * new EventEmitter.
 * L'ultima e unica cosa che può fare questo componente è propagare 
 * l'evento "attack", e quindi propagare il payload.
 * Il metodo emit() accetta un'info: il payload dao propagare:  
 * dentro emit() rimando l'el che mi è stato passato: this.hero 
 * Propaghiamo l'ogg hero -> dobbiamo dire ad Arena to listen 
 * for an event called "attack", whose payload is an object 
 * "hero" on eventEmitter.
 * 
 * Binding: a sx l'el che sta nel figlio, a dx quello che sta nel 
 * padre.
 * Property binding: a sx l'el decorato con @Input(), hero. 
 * Event binding: a sx l'el decorato con @Output(), attack. A dx, 
 *  "handleAttack()" è un metodo che è dentro arena.ts.
 * $event: chiave con cui angular cattura le info dell'evento che 
 * sta ascoltando. Nel nostro caso, visto che abbiamo propagato 
 * this.hero, dentro $event ci trovo l'intero eroe. 
 * 
 * Quando faccio .emit(this.hero) sto passando il puntatore allo 
 * stack, che contiene un puntatore allo heap, dove c'è l'intero 
 * ogg. hero1 detiene un puntatore, hero2 un altro puntatore. 
 * Come capire chi attacca e chi subisce danno? 
 * Se hero = hero1, hero1 attacca e hero2 subisce. E viceversa.
 * 
 * Ricordati che "a -= b" è una contrazione di "a = a - b".
 */