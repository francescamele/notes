/**
 * 18.01 REC I
 * (Da 47')
 * 
 * CREARE APP CON ANGULAR
 * We need an A. CLI.
 * Lo invochi nella powershell/terminale, come fai con webpack cli (che
 * serve a dare comandi per webpack dal terminale).
 * 
 * 
 * DIFF 1.ANGULAR JS / 2.ANGULAR
 * 
 * (1.) era la prima versione: sviluppata interam in js, e si sviluppavano 
 * le app interam in js. Si usa ancora, ma non dovrebbero: Google non lo 
 * aggiorna più, quindi se ci sono problemi di sicurezza legati al 
 * framework, te li tieni.
 * (2.) sta alla versione 13 (forse è uscita/sta per uscire la 14): il 
 * fr., dalla versione 2 in poi, è stato aggiornato. 
 * Potresti lavorare con versioni vecchie di A.: non ha senso, perché
 * Le dipendenze vanno aggiornate, sprtt per le correzioni legate 
 * alla sicurezza, since these fr. are often colabrodi on that. 
 * Infatti, la maggior parte delle new release sono bug fix.
 * I problemi sono nella gestione, eg quella delle info digitate 
 * dall'utente nei campi in input e passate a un componente.
 * 
 * I form non li gestirai più allo stesso modo: potremo creare un legame 
 * tra campo input e proprietà di una classe. 
 * Però il campo in input aggiornato sulla proprietà del componente 
 * (della classe) crea casini tipo MySpace: il pischello aveva iniettato 
 * codice thr a form, il code non era sottoposto a escape (eg, "<" non veniva
 * convertito in "&;" : questa è una delle prima contromisure al cross-side 
 * scripting), e quindi quando ha iniettato del codice, questo è stato 
 * eseguito da tutti. Tutti quelli che vedranno il commento faranno eseguire 
 * quel codice. 
 *  
 * XSS: cross-side scripting, tipo di attacco informatico con il quale 
 * introduciamo dei tag html per eseguire js, eg iniettando del typescript 
 * dentro il form: se il framework? non fa l'escape delle <>, so cazzi. 
 * Un altro tipo di XSS è quello sui query param: se ne ho uno che viene 
 * sottoposto alla funz eval di js, che esegue il codice scritto nella 
 * stringa, piazzo delle funz js sul query param, le faccio eseguire io 
 * contestualmente quello che voglio.
 * Di solito si usa XSS assieme ad altri tipi di attacchi. 
 * 
 * Un esempio di problema di fr. tipo A. è che, nel contesto di un form 
 * con un input numerico (con le freccettine), se utilizzi il two-way 
 * data binding sul terzo campo in input, quel campo non viene sottoposto 
 * ad escape. ALlora quel bug viene corretto in un nuovo release. 
 * Di solito A. aggiorna anche la versione di Typescript. Il fr. è già 
 * strutturato, quindi you don't need big adjustments.
 * 
 * Quindi è importante aggiornare A., anche perché hai new tools and 
 * possibilities. 
 * La stragrande maggioranza di applicativi A. sono retrocompatibili: 
 * basta aggiornare. 
 * 
 * 
 * Quando installiamo un pacchetto che ha una "@" nel nome: @nomePacchetto, 
 * sappiamo che a dx ci sta qualcos'altro: @angular identifica una cartella 
 * che contiene sottomoduli. Bootstrap non l'aveva perché è monolitico, NON 
 * modulare. 
 * 
 * npm install -g @angular/cli
 * -g   flag installazioni globali: non stai installando il pacchetto nella 
 *      cartella node_modules dove sta il puntatore, ma in una cartella 
 *      ugualmente accessibile da qualsiasi punto del filesystem. 
 * 
 * 
 * ...1h22 ???
 * 
 * 1h26'
 * sudo: installi la CLI nella cartella dell'amministratore ma non 
 * dell'utente, quindi comunque non puoi accederci. (?)
 * New folder in user folder: .npm-global
 * Configuri questa cartella come cartella global thr npm: npm sa 
 * dove installare la roba.
 * Crei un file .bashprofile, nella cartella user: estendi la 
 * path variable di unix indicandole la cartella npm -global: dentro 
 * c'è la cartella bin, nella path variable indichi il percorso e ...
 * 
 * 
 * 1h55'ca (riprende)
 * CREARE APPLICAZIONE ANGULAR
 * Usare cli di A.: il comando è "ng"
 *  ng version
 * 
 * Per usare Fedora, si può fare dal terminale: è più difficile ma 
 * devi impararlo.
 * 
 * A volte il main problem dei linux avviene anche su windows
 * 
 * Su Fedora: 
 *      sudo dnf install nodejs
 *      yes
 *      node -v
 * Ho la node 16 e va bene così: l'ultimo è node 17, ma andrea ha un 
 * warning che dice 
 * 
 * 
 * REC II
 * 
 * La ~ ti dice che sta nella cartella dell'utente
 *  mkdir npm-global
 *  ls -l    mostra cartelle, ma non le nascoste
 *  ls -a   mostra anche cartelle nascoste
 *  npm config set prefix ~/.npm-global
 *  npm install @angular-cli -g
 * 
 *  nano .bash_profile
 * Trovi .bashrc da eseguire nell'if
 * 
 *  . [...] ==> esegui [...]
 * 
 * Controlli cos'è .bashrc:
 *  nano .bashrc
 * Fuori dall'if, sotto export path, scrivi:
 * PATH="$PATH:$HOME/.npm-global/bin"
 * Dentro questa cartella bin ci troverai il comando ng: gli 
 * eseguibili di tutti i pacchetti che hai installato con npm con 
 * flag -g.
 * Alla variabile path assegna una stringa ottenuta dalla concatenaz 
 * del percorso della variabile path... 17'
 * Probabilmente tutto va tra doppio apice
 * 
 * Poi CTRL+X per uscire, salvi, e lo salvi col suo nome.
 * 
 *  source .bashrc  -> aggiornare la variabile d'ambiente su questa shell (19')
 * Per verificare che la variabile d'ambiente sia effettivamente aggiornata: 
 *  ng version
 * ...20'... l'ultimo percorso è quello di npm-global/bin, va lì e ci trova 
 * qualcosa... 
 * 
 * 23' ricomincia
 * La cli a. è interattiva: mi fa domande a cui rispondere.
 * Ci chiede se voglio aggiungere le ? di routing.
 * Nelle cli ... c'è una porzione di schermo che cambia: non possiamo 
 * fare il get, perché con quello si svuota il dom ?.
 * Quindi dobbiamo 
 * 
 * ...
 * 28'
 * ... questo componente verrà mostrato in una particolare zona dello 
 * schermo dedicata al rooting: i componenti verranno ricaricati lì 
 * dentro invece di ricaricare l'intero dom ogni volta. 
 * 
 * Crei una nuova struttura:
 *  ng new etl-ng-fra
 * Scegli la lingua: SCSS
 * Thr npm, installa le dipendenze: elementi che compongono il framework 
 * Angular: fr impostato a moduli, con un core module e tanti altri 
 * sviluppati attorno a esso.
 * 31'
 * Dev'esserci sprtt chiaro il concetto di Promise: ReactiveX lavora con 
 * un sistema molto simile ai socket. Dopodiché, capire cose come gli 
 * observable è facile. 
 * 
 * Fedora ha il comando ls -l: lista.
 * Invece sulla power...
 * ...
 * Se sei il proprietario puoi cambiare i permessi, sia con le lettere 
 * (r/w ecc), però anche con la notazione ottale: 
 * ...
 * 
 * Angular language service: permette di leggere all'interno dei template 
 * le proprietà ...
 * 
 * 
 * 8-9'
 * Variabili di ambiente servono a eseguire? persorsi ??
 */