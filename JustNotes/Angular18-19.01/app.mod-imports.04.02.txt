04.02
REC I 
... sul campo form: vuol dire che il modulo reactive-forms module 
non è stato caricato. Se ti dà questo errore anche quando nell'arr 
imports il modulo ci sta, puoi provare due cose: 
1. Il transpiler si è incartato: ha transpilato senza includere il 
modulo ReactiveFormsModule, anche se l'hai messo: glielo rimetto. 
Ma se non funziona, allora: 
2. Se quel COMP è l'unico che mi dà problemi su quel modulo (quello 
del form), probabilmente quel comp non fa parte del modulo che si 
è importato il ReactiveFormsModule: 
Se nell'appModule usi uno dei moduli di un form (Reactive o Forms)
ma il comp che usi non è nell'arr declarations di quel modulo 
(cioè dell'AppModule), allora in quel comp non avrai la roba che 
si importa AppModule. 
In AppModule l'array declarations si aggiorna da solom ma gli 
import no. 