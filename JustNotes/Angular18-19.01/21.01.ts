/**
 * 21.01
 * REC I 
 * 
 * ...30'
 * 
 * 
 */

element.addEventListener('click', e = {
    // 'e' è il payload
    // e.target...
})

/**
 * In a., se vogliamo passare a e un valore, usiamo 
 * '$event'
 * 
 * 'Event' è un datatype.
 */

 public changeNum(isSum: any, e: MouseEvent): void {
    if (isSum) {
        this.i++;
        return;
    }

    this.i--;
}

/**
 * 46' ricomincia
 * ...
 * Quando invochiamo un comp thr its selector, stiamo necessariam 
 * creando una gerarchia padre-figlio tra componenti. 
 * 
 * Eg., so che counter è un comp col selett <app-counter> 
 * <app-counter> è nel template di AppComponent: se il sel <app-counter> 
 * è presente nel template di AppComponent, AppComponent è il 
 * padre, e <app-counter> è il figlio. 
 * 
 * AppComponent è il padre di tutti i comp che sono invocati nel 
 * suo template. 
 * Se nel template di Counter invochi <app-buttons>, Counter è il 
 * padre di Buttons.
 * 
 * La relazione tra comp NON È data da dove sono le cartelle coi file, 
 * che saranno TUTTE nella cartella components, allo stesso livello. 
 * È data invece da dove viene invocato il selettore: un widget del 
 * meteo può essere invocato più volte in un template e/o in diversi 
 * template, quindi può essere figlio di tanti component diversi. 
 * Per ogni invocazione del selettore in uno o più template, il 
 * comp verrà istanziato singolarmente: esisterà un'istanza del 
 * comp per ogni selettore invocato. 
 * 
 * 
 * 1h09 ricomincia
 * PROPERTY BINDING
 * Bind. tra comp legati da gerarchia padre-figlio
 * In counter, il comp padre deve passare un valore iniziale a i, 
 * che sia diverso da 0. 
 * Inizialmente, i sarà 0, poi appComponent gli darà un altro valore 
 * e passerà a quello.
 * 
 * Tutte le proprietà che vogliamo siano valorizzabili dall'esterno 
 * devono essere DECORATE con @Input: ci dev'essere un Decorator. 
 */

export class CounterComponent implements OnInit {
    @Input() public i: number = 0;
    public subButtonText = '-';
    // Roba
}

/**
 * @Input() sta decorando SOLO i: se vuoi decorare subButtonText devi 
 * scriverlo anche prima di quello.
 * ... (import Input)
 * 
 * Quando creiamo una 1h18'
 * Sintassi property binding: []=""
 * .. riascolta tutto te prego
 * 
 * vedi <app-counter> in app.component.html: 
 * a sx metti il nome della proprietà da cambiare, a destra gli dai il 
 * valore che vuoi dargli. In questo caso, il contatore non partirà più 
 * da 0 ma da 5.
 */
// In counter.component.ts:
 export class CounterComponent implements OnInit {
    @Input() public i!: number;
    public subButtonText = '-';
    // Roba
}
// In app.component.html:
// <app-counter [i]="5"></app-counter>
/**
 * All'interno di appComponent metti una proprietà che contiene un 
 * numero. (startingNumber)
 * Il valore di quella proprietà lo dovrai passare al contatore come 
 * valore iniziale. (3)
 * Poi, verifica una cosa: se la proprietà numerica presente dentro 
 * AppComponent cambia valore (eg con un setTimeOut dp 10s), nel 
 * CounterComponent cosa succede? Il valore viene aggiornato? No? 
 * Il cambiamento viene riflesso nel CounterComponent?
 */