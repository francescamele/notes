/**
 * 21.01
 * REC I 
 * 
 * ...05'
 * Stringhe: UTF16 -> ogni carattere occupa 16 bit all'interno della RAM. 
 * Quindi se il parametro è 'add' o 'sott', sono 16bit*3 o 16bit*4.
 * Numero: 64bit, perché NON distinguiamo intero/float/double ecc, quindi.
 * 
 * Per il counter, +/-, il migliore è booleano: fisicamente in memoria si 
 * prende 8bit, ma il dato booleano se ne prende 1 solo.
 * 10-13' su booleano/memoria.
 * Concetto di word e d-word.
 * 
 * Con un param booleano si nomencla sempre con "is ..." o "has ..."
 * 
 * 
 */