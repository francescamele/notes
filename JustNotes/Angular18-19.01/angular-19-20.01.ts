/**
 * 19.01 
 * REC I
 * 
 * Cos'è un componente? 
 * A volte dobbiamo copiare-incollare lo stesso codice, sprtt in html 
 * (in js, se sei bravo no). È un approccio dispendioso, e per evitarlo in 
 * a. ci sono i:
 * 
 * COMPONENT
 * La fusione tra un elemento html, usually called "template", e 
 * una parte che detiene la logica del componente (usually js, il 
 * componente vero e proprio), che uniti vengono chiamati "view", quando 
 * vengono istanziati insieme. 
 * Quindi VIEW = istanza di un componente.
 * 
 * In a., le info tra component e template vengono scambiate thr meccanismi 
 * chiamati binding (event binding, o property binding).
 * 
 * I COMPONENTI sono classi (js, typescript): di solito è una classe plain 
 * js, cioè senza alcun tipo di estensione particolare, vuota. Ciò che la 
 * rende un componente sono i: 
 * 
 * METADATA: 
 * Un oggetto con alcune chiavi a cui associamo alcuni valori. 
 */
// La classe è così, completamente vuota:
class MyComponent {

}
/**
 * In typescript andiamo a mettere anche cose di js, after all.
 * Quello che la rende un componente è un DECORATOR a cui passiamo dei 
 * metadata: in js, una funzione che avvolgeva un'altra funz, donandole le 
 * proprietà caratteristiche.
 * In typescript, il concetto si estende: applicabile a proprietà, variabili, 
 * funzioni, metodi, classi. 
 * Cn ts possiamo dire: questa è la funzione, deve diventare un componente, 
 * quando gli passo certi param attuali. 
 * 
 * DIRETTIVA
 * Sorta di regola che puoi mettere sui tag html che serve a cambiare il 
 * comportam di quel tag.
 * Eg, una d. che posta su un el html ascolti un evento e cambi il colore 
 * di sfondo dell'el. 
 * A differenza del compon., visto che si applica su el già esistenti, non 
 * ha il template. 
 * 
 * SISTEMA INJECTOR
 * Servizi--delle classi--le cui instanze vogliamo iniettare all'interno di 
 * classi, quindi componenti, direttive, ecc. 
 * 
 * MODULI
 * A. è gestito in maniera modulare: ci consente di sviluppare modularmente.
 * 
 * MODULO:
 * Insieme di componenti/direttive/servizi che servono, messi 
 * insieme, a risolvere una parte del mio problema. 
 * 
 * Eg con amazon home page:
 *  - prodotti 
 *  - utenti 
 *  - categorie 
 * CRUD = Create, Read, Update, Delete
 * Dovrò fare un modulo per le categorie, che conterrà un servizio per gestire 
 * questo paradigma: leggere dati, aggiornarli, eliminarli ecc.
 * Dovrò creare un modello che rappresenti la categoria, che avrà un id e un 
 * nome. 
 * Potremmo avere un componente che mostra le categorie, magari posizionato in 
 * una barra di navigazione. 
 * Se sei navigato, riesci ad associare il modulo all'entità del backend. 
 * Se clicco sul menu a sx, questo menu in verticale non è parte del comp 
 * orizzontale: dialogherà con esso, probabilmente con un servizio, ma sono 
 * entità diverse. 
 * Un comp. può essere dentro un altro comp., e comunicano tra loro. 
 * 
 * C'è un modulo principale su cui è basata tutta la programmaz in a.: 
 * nella doc si chiama RootModule, nel code si chiama AppModule.
 * 
 * Il componente non ha moduli, è il modulo che ha componenti.
 * Puoi pensare al comp come ciò che l'utente vede, anche se è riduttivo. 
 * 
 * Modules allow you to parallelizzare un lavoro. But you need a good backend, 
 * e devi sapere come restituire i dati: agree on endpoints to interrogare per 
 * each entity, e le strutture dati con cui lavorare, su JSON server. Poi, 
 * una volta che il backend è finito, ci si può spostare su quello. 
 */

@Directive({
    selector: '[appMyDirective]'
})
class MyDirective {
    // ...
}
// MyDirective è un component solo perché ha il decorator @Directive. 
// Questo decorator prende un'info che si chiama selettore, non sappiamo
// cosa fa, potrebbe fare mille altre cose. 
// Il primo è un decorator che agisce sulla classe. 

/**
 * I comp. stanno ciascuno dentro un file, anzi hanno una cartella per comp.: 
 * comp. + html + scss + file di testo: sono 4 file. 
 *  
 * Negli ogg a. abbiamo una copia della cli, tanto che quando useremo il 
 * comando ng nella cli ci rifaremo a questa cli, non a quella installata 
 * globalmente.
 * 
 * Se guardi il package.json: 
 * Se scrivi ng significa che usi la cli del progetto: i comandi li 
 * esegue rispetto alla sua cartella node_modules, non quella globale.
 * "ng serve": comando che useremo per far partire un webserver di sviluppo già 
 * impostato. Sotto ci saranno webpack e altra roba + un server che potremo 
 * interrogare con localhost:4200. Questo perché dovremo avere un 
 * riferimento a cui puntare. Se non scrivessimo la root 4200,  ...45
 * 
 * Guarda index.html
 * Non c'è niente, neanche un tag script ...50'
 * 
 * main.ts:
 * equivalente di index.js. Questo file non lo toccherai mai. 
 * Riga 7-9: controllo se mi trovo in un ambiente di produzione. Se sì, 
 * funzione che disabilita alcuni log di controllo: se sono in produz, non 
 * c'è bisogno di testare l'app, così da nascondere certe info all'utente. 
 * - platformBrowserDynamic: 53'
 *   Questo perché ... per a. esiste la possibilità di sviluppare mobile 
 *   apps ibride. ...
 * App ibrida: non fatta per essere solo per android o IOS, ma che va bene 
 * per entrambe. Eg apache ?: un'istanza di chrome ...
 * Quello, a livello di performance, è povero.
 * Invece strumenti come Creative, Native Script, ecc, loro al posto 
 * del widget generico che voglio costruire mettono il widget originale: 
 * non c'è quindi una reale differenza tra un'app ibrida e una nativa. 
 * L'unico problema è con la grafica 3D.
 * 
 * 1h03' Circa discorso di Andrea
 * 
 * 
 * 1h16 RICOMINCIA
 * Quello che cambia è il platformBrowserDynamic
 * bootstrapModule() dice qual è il modulo principale dell'applicativo: 
 * root module, quindi, della struttura alberata di moduli. 
 */ 
    platformBrowserDynamic().bootstrapModule(AppModule)
       .catch(err => console.error(err));
 /* 
 * .catch serve a gestire gli errori che ci dà ...1h22
 * 
 * Da qui, quello che ci interessa analizzare è l'AppModule.
 * 
 * APPMODULE
 * È una classe che, levato il decorator, è vuota.
 * Non estende nulla, quindi l'unica cosa che sta dicendo a questa 
 * classe di essere un module di a. è il decorator: gli vengono passate 
 * delle info tramite il decorator.
 * * *
 * app.module.ts:
 */ 
    @NgModule({
        declarations: [
            AppComponent
        ],
        imports: [
            BrowserModule,
            AppRoutingModule
        ],
        proviserd: [],
        bootstrap: [AppComponent]
    })
    export class AppModule { }
 /*
 *
 * - declarations []: declinati i componenti, le direttive e le pipes. 
 * 
 * - imports []: declinati tutti i moduli da cui vogliamo attingere 
 *   funzionalità.
 *   Eg: se in a. abbiamo un modulo per gestire i form, se il mio 
 *   AppModule deve usare i form, in questo import ci sarà il modulo
 *   per quello.
 *   BrowserModule: aggiungi all'applicazione la possibilità di essere 
 *   gestita nel DOM: funzionalità per alterare e manipolare il DOM. A 
 *   sua volta, contiene/importa anche il CommonModule
 *   AppRoutingModule: tutta roba per i routing, non pensarci ora.
 *   
 * - providers []: declinati tutti gli el, usually classi che per 
 *   convenzione vengono chiamate 'servizi': grazie alla DI (che 
 *   vedremo) possono essere iniettati nell'app. 
 *   Anche altri valori, a cui dai un token. 
 * - bootstrap []: chiave presente SOLO nel modulo principale. 
 *   Nella fase di bootstrap, ci permette di indicare quali sono i 
 *   componenti da caricare all'avvio dell'applicazione. 
 *   La fase di bootstrap è l'avvio dell'applicazione (perché ci piace 
 *   riusare i nomi...).
 *   Quindi gli diciamo che deve partire l'AppComponent, che quindi 
 *   si occupa di quello che viene mostrato al momento iniziale. 
 * 
 * 
 * app.component.ts
 * * */
    @Component({
        selector: 'app-root',
        templateUrl: './app.component.html',
        styleUrls: ['./app.component.scss']
    })
    export class AppComponent {
        public title = 'etl-ng-fra';
    }
 /* * *
 * 
 * public title = 'etl-ng-fra';
 * Quando dichiaro proprietà, variabile, funzione ecc, posso 
 * specificare il tipo:
 * 
 * public title: string = 'etl-ng-fra';
 * 
 * Non c'è bisogno di scrivere ": string", o "public" (che è di default),
 * però è più chiaro così.
 * Se invece NON assegni un valore, tipo:
 *   public title;
 * DEVI mettere il datatype (anche fosse any).
 * 
 * DICHIARARE PROPRIETA' IN UNA CLASSE
 * access modifier - nome proprietà : datatype - (eventualmente) assegnazione
 * 
 * 1h46' tipo 'any'
 * 
 * Il component fa tre cose:
 * - styleUrls: array di fogli di stile; posso applicargliene più di 
 *   uno, ma di solito è uno.
 * - templateUrl: indirizzo al template per unire componente al template. 
 * - selector: quello qui dentro diventerà un tag html. 'app-root' è 
 *   diventato il tag <app-root>: nell'html, è il tag che invoca la 
 *   classe AppComponent.
 */

/**
 * 20.01 REC 1 26'
 * 
 * ...
 * If I need to start up something as soon as the app starts, I'll 
 * edit the 'bootstrap' key. (Andrea has never ever done it).
 * Ogni comp declinato in bootstrap viene gestito quasi come fosse 
 * un'applicazione a parte.
 * Se aggiungi altri componenti, è come se fossero diversi punti 
 * iniziali di diverse app separate, ma che girano nella stessa pag 
 * html.
 * 
 * Come fa a. a riconoscere i selettori dei comp che decliniamo?
 * Thanks to 'declarations': questi componenti hanno un selettore, 
 * che viene letto e poi viene creato un tag o qualcos'altro, perché 
 * tutto è incluso nella declarations. Se un comp non è in quell'arr, 
 * puoi aver scritto il selettore in termini di tag in un html ma 
 * non verrà mai interpretata da a.: il browser leggerà un tag che 
 * non esiste e lo interpreterà come div.
 * 
 * ...31
 * Quando creo il comp thr cli, a. crea i file del comp. e aggiorna 
 * l'arr declarations di @NgModule.
 * In modo che: crei un comp con la cli, vai sul codice, lo invochi, e 
 * già vedi se funziona o no (funziona).
 * 
 * ...39'
 * Importeremo la > parte delle cose da @angular/core
 * 
 * (Cosa sono kernel e distro linux?) 
 * 52'
 * Comando per fare che?
    ng serve
 * ^Transpiler incrementale:  
 * Appena riavvii, questi leggono tutto il codice che devono transpilare 
 * e lo transpilano nella sua interezza, da ts a js.
 * Li rende incrementali il funzionamento che si innesca dopo la prima 
 * transpilazione: quando salvi, il transpiler legge solo il pezzo 
 * modificato e lo transpila. 
 * DIFETTO: spesso si incartano. Se scrivo una cazzata e genero un 
 * errore, se ne generano altri due in cascata, il tr. magari ne vede 
 * solo uno; o magari ne aggiusti due e ne lasci uno, ma il tr. ne vede 
 * comunque due. Succede sprtt con grossi errori.
 * Devi fermarlo e farlo ripartire. 
 * 
 * Angular usa webpack, anche se non lo vediamo. 
 * 59'...
 * 'localhost:' ecquivale a '127.0.0.1:'
 * Quindi se scrivi '127.0.0.1:4200' invece di 'localhost:4200', ti 
 * trova comunque a. 
 * 
 * 1h03' !!
 * ng serve: se ci dà una porta, collegandoci a localhost:4200, 
 * vuol dire che c'è qualcuno che sta ascoltando le nostre 
 * richieste, a cui dà delle risposte.
 * Questo è un web server: analizza la richiesta che gli mandi 
 * con il verbo GET, cerca di capire che file vuoi, e te lo 
 * manda. È un web server perché ti risponde con un file 
 * e non con una stringa. 
 * La mia macchina è un web server? 
 * ...
 * Il backend deve stare dentro un web server: "se mi arrivano richieste 
 * per x, cosa devo fare? - Per la tipologia x, vai ..."
 * Con la roba per il web, tutto gira su una porta:
 * PORTA 80
 * Questo per quanto riguarda http. https lavora diversamente, e la 
 * porta è la 993, o 45qualcosa (or so Andrea remembers), dipende dal 
 * codice (?) di cifratura.
 * 
 * 1h13 ricomincia
 * Quindi abbiamo localhost:4200 perché c'è un web server che gira sotto. 
 * ...?? !
 * 1h19' di nuovo
 * http vs https?? 
 * CERTIFICATI
 * 2 file:
 *  1. Per cifrare la comunicazione
 *  2. Per decifrare la comunicazione
 * Quando usi un qualsiasi browser lui ha un set di certificati ...
 * che associa a una serie di eventi. 
 * Qeusti cert vanno upgraded.
 * Sono firmati da enti che li validano con la loro firma. 
 * https è scritto in verde o ha una spunta verde perché quel cert 
 * è firmato digitalmente da un ...
 * Quando è in rosso, 
 * Se il cert non è firmato, non è associato ad alcun dominio: lo possono 
 * utilizzare tutti. Se a e b cifrano la comunicazione, c può mettersi 
 * in mezzo e inculare tutti. Se su uno di questi siti scrivi email e 
 * password, che sicuramente avrai molto simile in altri siti, sei 
 * inculato. 
 * Interrotto 11:21, 1h32'35
 * 
 * 1h35' ricomincia
 *      ng generate
 * ?
 * Dopo component, nome del componente:
 *      ng generate component compName
 * 
 * John Papa: uno dei biggest divulgatori nelle conferenze di a. 
 * Ha scritto le convenzioni per strutturare i progetti a. a livello 
 * di cartelle e di file, convenzioni che ora sono nella doc ufficiale di 
 * a. e quindi tutti le seguono. 
 * 
 * Queste convenzioni dicono che tutti i comps devono essere agglomerati 
 * in una cartella che si chiama components:
 *      ng generate component components/compName
 * Il nome del comp dev'essere SEMPRE DASHCASE (chiamato anche kebab case)
 * 
 * Il comp così creato viene messo in un'altra cartella.
 * Sulla shell ci darà quattro create: quattro file.
 * E anche un update di app.module.ts
 * Affinché a. sappia che esiste il tag messo nel selector 
 * in @Component() in app.component.ts, e che lo tratti e ci 
 * metta un comp, dobbiamo mettere questo comp nell'arr 
 * declarations di un modulo, sennò a. non lo vedrà mai. 
 * I selettori non li scrivi tu, li compila automaticamente 
 * a. con la cli.
 * Quando a. beccherà quell'app-counter, automaticam. creerà
 * un'istanza del counter component.
 * 1h45'...??????
 * 
 * In app.component.html troverai:
 *      <span>{{ title }} ...</span>
 * In a., quando cerchiamo di passare info dal comp al template, o 
 * viceversa, o tra componenti, sfruttiamo dei meccanismi chiamati: 
 * DATA BINDING:
 * legami tra dati.
 * Questo nello span è il primo data binding di a. che analizziamo, e 
 * si chiama:
 * INTERPOLAZIONE:
 * Scrivendo il nome di una proprietà del componente, questa viene 
 * stampata sullo schermo.
 * 
 * Perché si crea un legame tra il template e la proprietà title? 
 * Perché l'interpolazione non si limita a prendere il valore trovato
 * nella proprietà e a stamparlo. Il legame consiste nel fatto che 
 * se quella proprietà title dovesse cambiare il valore che contiene,
 * a. beccherebbe il cambiamento e lo metterebbe nel template. 
 * 
 * Per farlo, in app.component.ts :
 */
    export class AppComponent {
        public title: string = 'etl-ng-fra';

        constructor() {
            setTimeout(() => this.title = 'cambiato', 5000);
        }
    }
 /* 
 * In questo modo, dopo 5s la pagina passerà da stampare 'etl-ng-fra' a 
 * stampare 'cambiato'.
 * 
 * 2h24' ricomincia
 * r.355, scrivi Cliccami invece di Resources. Per metterci un ascoltatore: 
 * metti id, richiami, aggiungi addEventListener, ecc.
 * In a., per mettere gli ascoltatori a un evento, usi la tecnica di 
 * 
 * EVENT BINDING:
 * Legame tra gli eventi.
 * La struttura/sintassi è:
 *  (evento)="metodo nel componente"
 * Il tutto posizionato nell'html:
 */
    // <h2 (click)="handleClick()">Cliccami</h2>
 /*
 * Tra "" c'è il nome del metodo che vogliamo sia eseguito all'evento. 
 * Il metodo dev'essere descritto nel componente a cui è stato associato 
 * questo template: se app.component.html è il template di app.component.ts, 
 * il metodo devo descriverlo in app.component.ts.
 * 
 * Poi, in app.component.ts scrivi:
 */
 export class AppComponent {
    public title: string = 'etl-ng-fra';

    public handleClick() {
        alert('Hai fatto click');
    }
}
 /* 
 * 2h35ca esercizio
 * 
 * REC II
 * 
 * Per domani:
 * Analizza il tipo di dato che conviene passare al parametro. Motivato 
 * tecnicamente.
 * Motivazione agnostica rispetto al tipo di programmazione, ma gli 
 * aspetti tecnici che ci interessano vanno considerati in js. 
 * 
 * Realizza un componente che mostra i bottoni del triage così come sono 
 * nel progetto: al click su un bottone, memorizza in una 
 * proprietà del componente il codice del paziente (rosso/giallo/blu...)
 */