04.02 REC I 

REACTIVE-DRIVEN FORM 
Scriviamo il doppio del codice perché abbiamo dovuto creare 
l'istanza del form. 
Vantaggi: sincrono; ci permette di gestire le istanze del form a 
runtime, quindi aggiungere condizionalmente dei campi o rimuoverli, 
o cambiarne le regole di validazione. Molto utile quando hai un form 
molto dinamico, oppure anche quando il BE manda info per aggiungere 
o togliere input al form (caso limite e Andrea non è sicuro che 
funzioni bene). 
C'è alta variabilità della struttura del form. 

Il Validators è una classe statica che contiene riferimenti alle 
funzioni che verranno usati anche dalle direttive di ang. Mentre 
forbiddenValidator è una funzione che restituisce la fn interna, 
che è quella realmente usata per la validazione, e lo stesso si 
fa nella direttiva: analogia tra forbiddenValidator e minlength.
Anche 'required' e 'email' contengono funzioni, ma visto che sono 
funzioni che non hanno bisogno di param dall'esterno, sono direttamente 
le funzioni interne: la fn esterna la usiamo come tramite per 
raccogliere info per la validazione, ma in realtà potremmo anche 
passare direttamente una fn interna, inline, dentro new FormGroup. 

1h36
GETTER: metodo che crea proprietà fittizia poi recuperabile come 
fosse una proprietà. 
La firma di get() ci dice: AbstractControl | null 
Perché col get() (metodo del form) posso prendere o un FormControl o 
un formGroup. Ma lui non sa cosa ti prenderà: dà la classe astratta 
e te la fa sbrigare a te. 
Noi, quando prendiamo 'email', sappiamo che vogliamo prenderci un 
FormControl: quindi gli do questo come restituzione. 
Ma lui mi dà errore: non posso resistuire AbstractControl | null 
quando tu mi dici FormControl. E quindi tu gli dici: 
    return ... as FormControl 
^TYPE ASSERTION: gli assicuri tu qual è il tipo di file che gli 
stai passando. 