/**
 * 21.01
 * REC I
 * 
 * Da 26'ca in poi (prima era su choosingDatatypes.ts)
 * Risentilo per lo stack delle funzioni:
 * 
 */

function fn() {
    // cose
    // altre cose
    fn2();
}

function fn2() {

}

// Cose
// Altre cose
fn();

// In questo caso, quindi, NON metti l'else per evitare di 
// aggiungere un altro livello sullo stack:
public changeNum(isSum: any): void {
    if (isSum) {
        this.i++;
        return;
    }

    this.i--;
}
