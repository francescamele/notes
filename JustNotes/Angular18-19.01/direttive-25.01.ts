/**
 * 25.01
 * REC II
 * 
 * DIRETTIVE
 * 
 * 2 categorie:
 * - D. attributo
 * - D. strutturali 
 * 
 * * DIRETTIVE ATTRIBUTO * * 
 * Lavorano modificando gli attributi di un qualsiasi tipo di 
 * selettore, sia el html che selettori di tipo angular.
 * Eg: direttiva evidenziatore: metto su un tag testuale una dir. 
 * evidenz. che ascolta l'ev mouseover, e quando questo viene scatenato 
 * il colore di sfondo dell'el cambia. 
 * È una dir. attr. perché attraverso l'attributo style dell'el possiamo 
 * cambiare le proprietà CSS e di conseguenza cambiare il colore di 
 * sfondo, e quindi avere un el evideniato.
 * 
 * 
 * * DIRETTIVE STRUTTURALI * *
 * Non lavorano su degli el del selettore, ma nascono per 
 * ALTERARE LA STRUTTURA DEL DOM.
 * Una dir. str. può quindi introdurre o rimuovere degli el dal DOM.
 * Sono complesse da realizzare. 
 * Di solito lavorano con l'*: prendila per obbligatoria, ma fondamentalmente 
 * l'* abilita un sistema particolare di interpretazione di angular, la 
 * microsintassi. 
 * Questi vengono usati direttamente sull'el html o sui selettori 
 * di un comp. angular. 
 * L'html dei componenti non è html puro: l'html non è un linguaggio 
 * di programmaz, ma qui con *ngIf, *ngSwtich, else, fai anche altro. 
 * 
 * COSTRUTTO DI SELEZIONE
    <selector *ngIf="____">
 * ^Quando la condizione espressa nell'if è vera, angular introduce il 
 * selettore all'interno del DOM.
 * Nelle "", esattamente come l'if di js, ci può andare qualsiasi cosa, 
 * e verrà valutato in termini booleani allo stesso modo.
 * Trovandomi nel template di un comp, posso dargli la proprietà del 
 * comp., e a quel punto posso metterlo/toglierlo dal DOM.
 * OCCHIO: quando l'if diventa false l'ELEMENTO viene RIMOSSO. E se levi 
 * l'el dal template, anche la SUA CLASSE viene DISTRUTTA.
 * Quando distruggiamo un comp così, si eliminano tutti gli ascoltatori 
 * agli eventi. 
 * 
 * COSTRUTTO DI ITERAZIONE (15')
    <selector *ngFor="
      let e of elements
    ">
 * 
 * Ricordati come funziona l'iterazione in js (ha ha ha):
    for (const e of elements) {
      Ciò che dev'essere ripetuto per ciascun el dell'arr, che 
      individuo in "e".
    }
 * Const può anche essere "let", ma per motivi di immutabilità si 
 * tende a usare sempre const. Inoltre, se usi i lintel, degli 
 * analizzatori di codice, let dà errore: meglio 
 * 
 * L'*ngFor viene applicata sul selettore di un el che dev'essere 
 * ripetuto tante volte quanti sono gli el di quell'array. 
 * Se conosci il for of di js, l'*ngFor di angular è una cazzata. (Sigh)
 * Oggi non si può più cambiare quel let in const, perché è tutto 
 * retrocompatibile. 
 * 
 * Preso l'arr, mantiene un contesto di quello che ti sei creato: se 
 * fai clic su Elimina e rimuovo un el dall'arr, avrò una riga in meno. 
 * Devi dire all'el che contiene la tabella che il comp ha un bottone 
 * con un payload che è la riga che devo eliminare. 
 */