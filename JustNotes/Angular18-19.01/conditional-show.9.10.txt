REC I 

MOSTRARE COMP CONDIZIONALMENTE
Di solito, se voglio vedere cose che non ho il permesso di vedere, 
il BE ti restituisce un 401. 
Ma a noi non importa: se il BE sono salami, fatti loro. 
Quello che NOI possiamo fare è usare qualche escamotage per 
indicare all'utente che non può fare qualcosa, o impedirglielo. 

Per farlo, c'è un tipo particolare di servizi, le:

GUARD
- Sincrone: siamo in grado di stabilire da soli se l'utente 
    può accedere a comp di routing oppure no. 
- Asincrone: (le vedremo simulate) per capire se un utente 
    possa accedere a una risorsa o no dobbiamo chiedere al BE. 
    Al BE dico: token x, id y: può accedere alla risorsa A? 
    Il BE dice sì o no. Se l'ACL lo implementa il BE possiamo 
    contare su di loro, sennò possiamo usare delle guard system. 
    Interrogare il BE può essere dispendioso: magari non può 
    dirci la risposta ...4'
    Se me lo dice una sola entità, se le cose devono cambiare 
    cambiano nell'async. 
    Nasce per usare il BE, poi tu puoi fare altro. Eg, puoi 
    adottare delle strategie di preloading (Andrea pensa che siano 
    inutili in una SPA).

Quello che cambia tra le due^ è che l'asincrona wrappa il 
risultato in una promise o in un observable, la sincrona in 
un booleano (true: accedi, false: non accedi). 

PRELOADING: eg ho una dashboard, con widget, grafici, elenchi, 
percentuali: dati. Andrea mostra dashboard, mette spinner per 
far capire che stai ricevendo info, e basta. 
Il preloading invece aspetta a mostrarti la dashboard finché 
non siano arrivati i dati, ti mostra solo il loading. Scemo 
per una SPA, sembra tornare al pre-SPA. 
Mille modi per risolvere questa situazione: eg la barra di 
loading in alto nel sito (yt ce l'ha), che ti fa capire cosa 
sta succedendo. 

URL3: oggetti particolari 11'? ma non li vediamo.
Posso restituirli in sincronia o asincronia (wrappati in 
Promise o obs). 
...


GUARD SERVICES
Le guard sono SERVIZI: hanno il decorator injectable. 
    ng generate guard modules/users/guards/is-logged

1    >(*) CanActivate
2    ( ) CanActivateChild
3    ( ) CanDeactivate
4    ( ) CanLoad

1. 12'50: 
    Non ti faccio arrivare, 16' e ti mando al login. 
    Stabilisce se l'utente è abilitato a fare qualcosa. 
2. Percorso punta all'utente, ma ha anche percorsi figli: ci 
    dice se siamo autorizzati a navigare su questi elementi. 
3. Magari stai modificando il tuo profilo; click Save, and part 
    of my interface is blocked: wait for the server's response. 
    Magari lui non dice ok, fallisce nel salvataggio. Oppure il 
    token sta scadendo, e la chiamata fallisce. Io dovrei 
    aspettare un messaggio di successo/errore, ma non sempre lo 
    faccio: magari navigo altrove. 
    CanDeactivate impedisce all'utente di uscire dal percorso in 
    cui è. Implementato molto raramente. 
4. Strategie preloading (Andrea thinks). 


Vedi is-logged.guard.ts 
Barra di caricamento: 26'
Facile da mostrare ...

Come si applica una Guard? 
All'interno dei singoli oggetti in users.routing.module 
possiamo specificare
28...

... ha pagato? È loggato/in grado di accedere alla risorsa x? 
Con la guard, controllo l'abbonamento. Posso metterli in serie 
o in cascata. 
Se avessimo fatto la guard per "children", sarebbe stato meglio 
mettere il CanActivateChild a users piuttosto che mettere un 
CanActivate per ogni child--SE POSSIBILE. 
31-32 su modulo principale e router. 

Se metto return: false in is-logged.guard.ts, non posso più 
andare sulla sezione Users. Ma non dico niente all'utente. 
Tenendo conto che la Guard è una classe, è un servizio, è 
decorata dal decorator injector, come si risolve il problema? 
Per fare un redirect al login, devo giocare con la route: 
inietto _router nel costruttore della Guard e poi ... (check
file). 

44'.
Che succede se false è asincrono?