
33-34'ca

Proprietà form.submitted: ci permette di vedere se l'utente 
ha provato a fare il submit del form: al click del submit 
diventa true.

Quando gli observable mi permettono di mettere gli operatori 
in cascata, e quindi metti un operator dopo l'altro dopo 
l'altro, parlo del metodo pipe: così come l'operatore || si 
chiama pipe perché ti fa passare da un param all'altro all'altro. 



string.validators.ts
Noi raccogliamo le info nella fn esterna, poi invochiamo la 
fnInterna e gli passo i valori delle mail/psw non ammesse. 
Come se li ricorda, questi valori? 
Ogni volta che ho delle {} JS mi crea un lexical environment. 
Quando ci sono di mezzo le funzioni, per gestire casi così 
con + invocazioni della stessa funzione con param attuali diversi, 
i lexical environment vengono creati per invocazione: a ogni 
invocazione con param attuali si crea un lexical environment 
diverso. 
Non ne viene creato uno per le {}, come per l'if, ma uno per 
ciascuna invocazione. 
In questo caso vengono creati sia per l'interna che per l'esterna. 
Riascolta questi minuti, 1h ca. Also, check javascript.info: 
studiati il lexical environment. 
Non è scontato che se invoco tre volte forbiddenValidator con 
valori diversi la funzione interna riesca a orientarsi. 


REC III 
Reactive driven form 
Struttura

form Group è la main instance: agglomerato di form Control. 
Sono oggetti istanza, quindi vanno istanziati con new 
(new FormGroup, new FormControl)


FORM GROUP  
1. Primo param è un ogg, le cui chiavi sono i nomi dei campi e 
i valori sono un'istanza FormGroup o FormControl
2. Secondo argomento: 
    Array di validatori sincroni. 
    Eg. forbiddenValidator: faccio il return di un ogg, restituisco 
    un ? istanza in modo sincrono, quindi è un validator sincrono 2'
3. Terzo argomento:
    Array di validatori asincroni.
    Validatori che restituiscono il risultato all'interno di una 
    promise o un observable. 

(2)
Quando il controllo su un contenuto di un campo va fatto in 
maniera formale, uso un validatore sincrono: posso stabilire se 
l'info è corretta senza dover interpellare risorse esterne. 
(3)
Li usiamo quando dobbiamo interrogare un BE o risorsa esterna, perché 
non possiamo stabilire da soli se il dato è accettabile o no: i 
validatori sincroni possono dirmi che rispetti le regole, ma magari 
non è comunque accettabile. 

Prima si risolvono i (2), solo dopo che tutti questi danno esito 
positivo si interpellano anche i (3). 

Crei comp rdf-login
Aggiungi il comp all'app.routing.module: 
    component: Rdf-Login, 
    path: 'login-rdf'
^Il path è arbitrario: Andrea inverte i termini così da avere il prefisso 
uguale al login. 

Nell'approccio template-driven si parte dal template, qui è il 
contrario: in rdf.login.comp.ts 


FORM CONTROL 
1.  Prima info che prende: 
    Stato iniziale. 
    29'40
    Quand'è utile passargli uno stato iniziale diverso da vuoto? 
    Placeholder: quando tocchi col mouse si leva. Value: devi 
    eliminarlo carattere per carattere. 
    Dobbiamo distinguere i form in due tipi: 
    - creazione 
    - modifica: parte già popolato
    Quindi stato iniziale: o risultato di una chiamata o stringa 
    vuota. 33'50 
    Se invece i dati ce li ho già in pancia ?, allo stato iniziale 
    do gli input già filled in. 
2.  Array di validators sincroni.
3.  Array di validators asincroni. 

36'?
V. file, public form. 

File html:
Prima differenza è 43' siamo noi a dovergli specificare qual è 
l'istanza del form che abbiamo creato: [formGroup]="form"
Ma dobbiamo aggiungere questa formGroup: app.module, negli imports 
aggiungi ReactiveFormsModule. 
Stabilito il formGroup, stabiliamo i campi 

ricomincia 49
le Proprietà di tipo input seguono le stesse regole di html5 
Non abbiamo l'obbligo di usare l'attributo name, ma siamo 
obbligati a usare formControlName=""
Se scrivo: 
    formControlName="email"
sto legando questo campo in input con questo formControl
Abbiamo creato dei legami: 1h 
Anche in approccio reactive, il submit si ascolta con ngSubmit:
    (ngSubmit)=""
1h07 come capisco se ? nel form sono valide?
... qui l'istanza ce l'abbiamo direttamente nel comp: la ?
ngForm non esiste. Quindi quello che abbiamo fatto in ngForm 
lo facciamo qui diversamente: 
handleSubmit() in comp 
    public handleSubmit(): void {
        if () {

        }

        console.log(this.form.get('email')?.value);
    }
... 1h10
- ?.value -> se e solo se esiste mi dai value, altrimenti NON 
  guardare la proprietà value e dammi null. 