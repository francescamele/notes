/**
 * 26.01
 * REC I
 * 
 * CORREZIONE ES. *NGFOR
 * ...00'
 * 
 * Perché a prescindere un prefisso sui selector dei componenti? 
 * Perché se lo chiami "table", come lo distingui dall'elemento 
 * <table>?
 * Quindi lo chiamerai users-table
 * 
 * Errore nel nome: cancelli le cartelle, e in app.module.ts CANCELLI  
 * SIA GLI IMPORT che LE DECLARATIONS.
 * Btw, queste sarebbe meglio sistemarle in ordine alfabetico. 
 * 
 * Metti il READONLY all'ID in User (il modello).
 * 
 * ...
 * riascolta tutto
 * 
 * ...
 * Se users-table-row deve mostrare le info di un utente su una riga, le 
 * deve prendere dal padre: dovremo passare l'elemento su cui il padre 
 * sta iterando al figlio. 
 * Le proprietà decorate con @Input() non vengono inizializzate al 17' 
 * ma dopo.
 * Ts vuole che tu inizializzi una propr: se non la inizializzi e gli 
 * dai anche un tipo, troverai undefined, e ts quindi ti avvisa. Allora 
 * gli devi dire tu che il valore lo darà qualcun altro, sicuramente 
 * DOPO IL COSTRUTTORE, col "!". 
 * 
 * 29'ca spiegazione payload!!
 * 
 * 
 * TORNA ALL'ESERCIZIO
 * tcx.user is undefined -> "ctx" è "contest": indica lo scope. Sta 
 * dicendo che dove hai indicato user, user non ha un valore. 
 * Questo è un errore a runtime e me lo mostra in console: non è un 
 * errore di compilazione. 
 * Infatti dove ho fatto *ngFor, non ho fatto il property binding: qui,  
 * a dx el figlio decorato con @Input(), a sx l'el del padre. 
 * 
 * Come non stampare le tr una accanto all'altra invece che in colonna? 
 * Fare:
    <tr *ngFor="___">
        //
    </tr>
 * è una parte della soluzione. Perché qui dentro non posso scriverci i 
 * td, perché se li scrivo qui non userei l'altro componente. E poi, 
 * facendo così non riconosce la property binding to "user" perché non 
 * è una proprietà di "tr". 
 * Il selettore del comp non lo possiamo usare: siamo quindi costretti 
 * a usare tr. Però tr ...58' 
 * Dobbiamo dire ad ang che questo tr deve stamparlo nel DOM, ma si deve 
 * comportare come il comp. 
 * Ang riconosce il comp quando scriviamo il tag: quando in un comp 
 * abbiamo il selector scritto in modo x, lo chiamiamo a quel modo x. 
 * Ma se noi nel @Component({}) il selettore lo scriviamo tra [], quello 
 * non sarà più un selettore ma un SELETTORE ATTRIBUTO. 
 * 
 * 1h11' COSTRUTTORE
 * 
 * 1h12' in che altro può essere questo selettore attributo? 
 * 
 * BACK TO ES.
 * Per l'event binding, flusso opposto: dal figlio al padre. 
 * Voglio intercettare quando l'utente fa click. 
 * Metti lo spazio tra le propr Input, quelle in Output, e quelle ...1h16'
 * SPLICE
 * Serve a rimuovere uno o più el per riferimento: gli do la posizione da 
 * cui iniziare a eliminare e il num di el da eliminare. 
 * Per trovare la posiz dello user da eliminare, uso .indexOf(): gli passo 
 * user e mi dà la posizione in cui si trova quell'ogg utente. 
 * 
 * ...1h32 ricomincia
 * ...
 * Lui si è mappato ciascun el dell'array su ciascuna riga ?
 * 
 */

/**
 * CREARE DIRETTIVE
 * Sulla cli:
    ng generate directive directives/directiveName
 * Nel nostro caso:
    ng generate directive directives/highlight
 * 
 * Ci sono solo due file perché le direttive sono componenti che non ci 
 * hanno creduto abbastanza: sono comp senza template. 
 * Creata la direttiva, di cui abbiamo un file della dir e uno per i test, 
 * viene aggiornato anche AppModule. 
 * ...1h42
 * In directive.ts vediamo che il selettore è già un attributo: la useremo 
 * così. Inoltre, è scritta in camelCase, NON dash-case. 
 * Per applicare questa direttiva, innanzitutto mettiamo un console.log 
 * nel suo costruttore: al reload della pagina, vedremo il messaggio nella 
 * console. 
 * 
 * Vogliamo farlo funzionare come evidenziatore:
 * mi creo un metodo dal nome arbitrario (non è il nome del metodo che 
 * scatenerà l'evento), con all'interno un altro ...
 * ...
 * ...!!
 * Gli eventi ascoltano le dir all'interno delle dir stesse. Come? 
 * Grazie a un decorator:
    @HostListener()
 * ^ascoltare un el secondo la sua direttiva? 1h57
 * Nelle () ci va il nome dell'evento.
 * @HostListener() dice al DOM di ascoltare l'ev: prende riferimento 
 * all'el su cui ha posto la direttiva, e poi farà addEventListener su 
 * quell'el. ... lo prende e lo mette come handler: è js. 
 * Che cosa fa @HostListener() nella direttiva? 2h01
 * 
 * Dobbiamo evidenziare il testo: cambiare il colore di sfondo. Prima 
 * di tutto, come accedo all'el su cui ho posto la direttiva? 
 * Sul costruttore, nei parametri, mettiamo:
 * 
    constructor(private _el: ElementRef) {
        console.log("highlight directive")
    }

 * Non puoi toccare il costr mettendo cose tue, ma puoi per chiedergli 
 * cose di ang. Per ora pensalo come un param formale, che può accogliere 
 * info da angular, thr which we can access the el with the directive. 
 * NON FARE COSI':
 * 
    private el: ElementRef;

    constructor(el: ElementRef) {
        this.el = el;
        console.log("highlight directive")
    }
 * 
 * 2h08
 * Ora abbiamo accesso a un el che punta al sel da dentro la direttiva: 
 * 
    @HostListener('mouseover')
    public handleMouseOver(): void {
        console.log("mouse over!");
        this._el.nativeElement.
    }

 * ^Dentro nativeElement troverai l'el nativo per l'ambiente in cui sta 
 * girando ang.: 2h11
 * Dopo il punto dopo nativeElement. non trova nulla: ...
 * Mi vuole dare tutti gli el che sono in comune tra..., che sono 0. 
 * Quindi, quando dichiaro ElementRef uso il generic:
 * 
    constructor(private _el: ElementRef<HTMLElement>) {
        console.log("highlight directive")
    }

 * ^Scrivendo ElementRef<HTMLElement> gli dici cosa trova nell'ElementRef 
 * quando accede alla direttiva: tutti gli el html?
 * 
    @HostListener('mouseover')
    public handleMouseOver(): void {
        console.log("mouse over!");
        this._el.nativeElement.style.backgroundColor = 'yellow';
    }
 * 
 * ^Scrivo 'yellow', che è una stringa riconosciuta in CSS, anche dalla 
 * prima versione. 
 */

/**
 * Compito:
 * 1. Ripristina il colore precedente alla modifica quando esci dal 
 * mouseover: né bianco né trasparente, il COLORE PRECEDENTE.
 * 2. Modo per impostare dall'esterno il colore di sfondo: non è che la 
 * direttiva decide 'yellow': metti qualcosa che dice 'io ho il colore'. 
 * L'info deve venire dall'esterno.
 * Hint: direttive non hanno template, ma hanno in comune molte cose coi 
 * comp: il comp estende la dir.
 * Una volta fatto (1), rivedi quello che hai fatto finora, eg. il 
 * counter. 
 */
@Directive({
    selector: '[appHighlight]'
})
export class HighlightDirective {
    private _starting!: string;
    
    @Input() public highlightedColour!: string;

    constructor(private _el: ElementRef<HTMLElement>) {
        console.log("highlight directive");
    }

    @HostListener('mouseover')
    public handleMouseOver(): void {
        console.log('mouse over!');
        this._starting = this._el.nativeElement.style.backgroundColor;
        this._el.nativeElement.style.backgroundColor = this.highlightedColour;
    }

    @HostListener('mouseleave')
    public handleMouseLeave(): void {
        console.log('mouse has left.');
        this._el.nativeElement.style.backgroundColor = this._starting;
    }
}
// HTML:
// <h1 appHighlight [highlightedColour]="'yellow'" style="background-color: aqua;">
/**
 * REC II
 * Prima del cambio con 'yellow', quella proprietà avrà il valore corrispondente 
 * a quello attuale. 
 * Me lo devo memorizzare, ma non all'interno di questo metodo, perché 
 * limiterebbe lo scope là: mi creo una cartella all'interno della classe, che 
 * dichiaro privata perché il suo scope dev'essere limitato all'istanza corrente: 
 * l'esterno non deve accedervi. 
 * Prima di farlo però, scriverò il valore attuale del backgroundColor.
 * 
 * Inizializzare _starting fuori da handleMouseOver() significa farlo nel 
 * momento in cui costruisci la direttiva. Ma conviene farlo all'interno di 
 * handleMouseOver(), perché così viene inizializzata ogni volta che ci passa 
 * il mouse, e non prima per tutto: se qualche altra direttiva/libreria cambiano 
 * il backgroundColor a runtime, inizializzandola all'interno del metodo ho 
 * più probabilità di intercettare il colore più recente. Con l'altro modo, il 
 * colore originale lo leggi UNA volta nel costruttore, e poi potrebbe essere 
 * cambiato e il metodo non lo saprebbe. 
 * 
 * Quando hai un VALORE LETTERALE da passare a una proprietà in input, puoi 
 * togliere il singolo apice e le quadre. L'html quindi diventa: 

    <h1 appHighlight highlightedColour="yellow" style="background-color: aqua;">

 * VALORE LETTERALE/DATATYPE PRIMITIVO
 * Se dici 
    let a = 'ciao' -> letterale di tipo stringa: non puoi dire 'ciao' = 'boh' 
    let b = 4 -> letterale di tipo numerico: non puoi dire 4 = 7
 * I valori letterali sono LETTERALMENTE quello che gli assegni. 
 * Tutti i letterali sono primitivi. NON È VERO???
 * I datatype sono primitivi, i valori sono letterali. (?)
 * Ai datatype primitivi assegni praticamente sempre un valore letterale--se li 
 * assegni nel codice. 
 * Un valore letterale può essere anche un oggetto o un array:
    [1, 2, 3] =/= [4, 5, 6]
 * Un qualsiasi valore letterale ha un valore ben preciso e immutabile. 
 * Nel caso di: 

    let inputA = document.getElementById('input').value;

 * questo "value" non è un valore letterale: è un puntatore a qualcosa che 
 * conterrà un valore letterale, ma non lo è esso di per sé. 
 * 
 * ANCHE LE DIRETTIVE HANNO LE PROPRIETA' IN INPUT
 * Quante proprietà concorrono a determinare lo stato della direttiva 
 * evidenziatore? 
 * Due: una pubblica e una privata. 
 * Quindi c'è solo una proprietà rilevante valorizzabile DALL'ESTERNO che 
 * determina lo stato di questa direttiva. 
 * Quindi, invece di scrivere "appHighlight highlightedColour="yellow"" 
 * nell'html, è meglio scrivere "appHighlight="yellow"". 
 * Ma nessuno ha detto, all'interno della direttiva, che appHighlight è una 
 * proprietà in input. 
 * Se volessi chiamare la proprietà "hightlightedColor" "col", all'interno 
 * delle () di @Input metto una stringa 'col':
 * ...
    @Input('col') public highlightedColour!: string;
   <h1 appHighlight col="yellow" style="background-color: aqua;">

 * Quando ho UNA SOLA PROPRIETA' PUBBLICA che determina lo stato della mia 
 * direttiva posso copiare il selettore: questo è un alias di proprietà in 
 * @Input().

    @Input('appHighlight') public highlightedColour!: string;
   <h1 appHighlight="yellow" style="background-color: aqua;">
 * 
 * Così, appHighlight indica ad a. di caricare la direttiva, ma allo stesso 
 * tempo passiamo una propr all'el decorato con @Input(): la stringa 'yellow' 
 * finirà all'interno della propr decorata con Input che ha il nome del  
 * selettore della direttiva. 
 * Nell'Input() non puoi mettere il nome di un'altra Directive, perché le 
 * direttive tra loro devono essere indipendenti. 
 */
