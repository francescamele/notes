02.02 REC I 

Ascolta 16-22 per un ripasso. 


INTERFACCIA 
L'interfaccia la uso per prototipizzare un ogg generico: 
dici a VSC che quest'ogg è strutturato in questo modo. 
Lavori separatamente anche perché così l'interfaccia la 
tieni identica al BE ma poi col tuo model fai quello che 
vuoi, anche migliorando i nomi where needed. 

Il model si istanzia con new: occupa spazio nello heap, è 
il tavolino che costruisco. 
L'interfaccia prototipizza: dice che la risposta è fatta 
così, è il foglio delle istruzioni. 