13.01
REC I CELL 

https://getbootstrap.com/docs/5.1/layout/gutters/
... primi minuti sui gutters/margini

Con !important, prevale quello.
...
...i gutters, se privati di x o y, lavorano sia sull'asse x 
che sull'asse y.

Ci sono anche altre classi sulle colonne: eg. come far partire 
una colonna da un determinato punto della griglia.

Decido di disegnare un blocco di lg-10, però con un offset di 
2 col da sx: se non lo specifico, le due col di sx non le salta, 
parte dal bordo sx del container.
Per saltarle, utilizzo la CLASSE START:
    g-start-2

B. è scritto con scss: scaricandoci i sorgenti e facendo degli 
override (NON sovrascrivendoli, perché sennò quando esce la nuova 
versione della libreria perdi le modifiche fatte).

I browser, sprtt vecchie versioni cm internet explorer, aggiungono 
bordi di loro e annullando le tue regole, resettando tutto. Quindi chi 
sviluppa b., per scrivere regole su una base pulita, rimuove questa cosa 
con REBOOT.
Inoltre, Reboot imposta font di default.
Tutte queste regole in cascata si basano sul principio che se un font 
non esiste andrà a prendere quello che viene a destra, e poi quello 
in basso.
Di solito i font sono tutti derivati della stessa famiglia.


https://getbootstrap.com/docs/5.1/content/typography/

Gli h vanno da 1 a 6: l'h1 dev'essere uno, perché ha una rilevanza 
pazzesca sugli spider dei motori di ricerca, quindi solo il titolo 
della pagina. Minore è il numero dell'h, maggiore è la loro importanza 
per gli spider di indicizzazione. 
Se metti tanti h1, i motori di ricerca (sprtt Google) ti penalizzano; 
inoltre beccano se hai scritto il testo per loro o per una persona; 
leggono la keyword density: se è troppo alta, capisce che sei spam; 
memorizzano il momento in cui indicizzano il contenuto: se il sito è 
modificato spesso, lo controllerà più spesso; se il testo è identico 
a qualcosa di già pubblicato, mi distrugge, se invece è molto simile 
mi penalizzano.

È facile perdere punti coi motori di ricerca, ma è difficile riacquistarli. 
Tutto collegato alla brand reputation, che i mdr prendono in consideraz.
Abbassare il SEO-rank(/score) == essere buttato in fondo alle pagine.

Classi display, da 1-6, cambiano l'aspetto del testo a prescindere 
dall'h scelto, quindi slego l'importanza del contenuto del testo per 
l'utente da quella per gli spider. 


IMMAGINI 
    img-fluid
Con questa classe, l'imm. scala automaticam rispetto al contenitore: 
diventa responsive mantenendo l'aspect ratio (rapporto altezza/larghezza).

    img-thumbnail
Dà un bordino di 1px per creare l'anteprima dell'immagine.

    class="rounded float-start"
Img con margini ammorbiditi e float sx.

    class="rounded mx-auto d-block"
mx-auto: margine sull'asse x automatico.
d-block: elemento block, non inline.

    <div class="text-center" ...
        <img class="rounded" ...
Tutti gli el inline possono essere allineati da text-center.


TABLES
https://getbootstrap.com/docs/5.1/content/tables/


FORMS
    form-control-lg
rendere l'input del form più alto
    form-control-sm
rendere l'input del form più basso
    readonly
Sola lettura: non posso alterarne il contenuto ma posso selezionarlo 
(eg. per copiarlo).
    type="file" 

1h30-36' check?
CHECKBOX
Andrea sconsiglia la :indeterminate pseudoclass (che si può mettere 
solo con js).

Radio button: devono avere lo stesso name per non permettere all'utente 
di selezionarne più di uno.

[
    ID 
    Usarli in due casi:
    - Un elemento che non si ripete (eg navigation bar)
    - ?? (non l'ha detto e non voglio rompere)
    Magari se hai una checkbox usi il name, non un id diverso per ogni 
    option. È meglio usare un queySelector su un input con attributo name. 
]

RANGE
Non tutti i browser li supportano.
Makes sense for music stuff, like volume.
O per altezza/larghezza regolabili, o luminosità.