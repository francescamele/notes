export class Forecast {
    /**
     * @param {City} city
     * @param {string} description
     * @param {string} icon
     * @param {Temperature} temperature
     * @param {Wind} wind
     * @param {number} pressure
     * @param {number} humidity
     */
    constructor(city, description, icon, temperature, wind, pressure, humidity) {
      this.city = city;
      this.description = description;
      this.humidity = humidity;
      this.icon = icon;
      this.pressure = pressure;
      this.temperature = temperature;
      this.wind = wind;
    }
  
    // Un getter (un metodo che usa come parola chiave "get") crea una proprietà fittizia all'interno dell'istanza
    // il cui valore è ciò che viene restituito dal metodo
    // i.e. this.iconUrl <- NO parentesi tonde (si comporta come una proprietà)
    get iconUrl() {
      return `http://openweathermap.org/img/wn/${this.icon}@2x.png`;
    }
  
    // Questo è un metodo qualsiasi, e per leggerne il valore restituito devo invocarlo
    // i.e. this.getIconUrl() <- SI parentesi tonde (è un metodo, deve essere invocato)
    getIconURL() {
      return `http://openweathermap.org/img/wn/${this.icon}@2x.png`;
    }
  }

      //Dichiaro un metodo che a sx ha la keyword get:
    //serve a definire dei getter: un metodo che deve restituire 
    //qualcosa
    //Un getter (un metodo che usa come keyword get) crea una 
    //propr fittizia all'interno dell'istanza, il cui valore è 
    //ciò che viene restituito dal metodo
    //i.e. this.iconUrl <- NO parentesi tonde (si comporta come 
    //                     una proprietà)
    get iconUrl() {
        return `http://openweathermap.org/img/wn/${this.icon}@2x.png`;
    }

    //La differenza tra il getter e il metodo è come accedo 
    //al valore restituito

    //Qui invece è tutto attaccato, è il nome del metodo
    //Questo è un metodo qualsiasi, per leggere il valore 
    //restituito devo invocarlo
    //i.e. this.getIconUrl() <- SI' parentesi tonde (è un metodo, 
    //                          deve essere invocato)
    getIconURL() {
        return `http://openweathermap.org/img/wn/${this.icon}@2x.png`;
    }
    //Di là però di devi richiamare questo metodo
  