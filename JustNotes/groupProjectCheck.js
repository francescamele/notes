/**17.01 REC I
 * 
 * In un progetto, conviene avere una funzione che si occupa delle 
 * riposte per quanto riguarda i messaggi di successo e di errore.
 * 
 * Se devi fare un if else, usa l'operatore terziario.
 * 
 * 09'
 * 
 * Ti fai la funzione da aggiungere all'addEventListener in un file 
 * js che ti importi nel file index, e poi NELL'INDEX ci metti 
 * l'addEventListener dove chiami la funzione.
 * 
 *
 * *** YOUR PROJECT ***
 * 
 * Devi guidare l'utente: se il CF è sbagliato, glielo devi dire.
 * Si tende a lasciare il layout così com'è invece di cambiarlo (i 
 * colori non cambierebbero).
 * 
 * NO camel case nel CSS
 * Dividere file CSS per selettori, eg selettori e loro peso.
 * Ordine alfabetico e ordine di peso.
 * Non scrivere il CSS seguendo l'ordine dell'html, perché se dovessi 
 * modificare l'ordine nell'html poi dovresti farlo anche nel CSS.
 * Stili globali da una parte, dettaglio in un altro foglio di stile.
 * Se dai bene le classi, basta fare la divisione tipo bottoni da una 
 * parte, navbar dall'altra ecc. Se poi hai qualcosa di particolare, 
 * eg un bottone in una pagina precisa, trova un nome identificativo 
 * per quel btn particolare e comunque mettilo nella pagina dei btn, 
 * non in quello della pagina.
 * 
 * 47' ??
 * 
 * Stacco import in index.js tra JS core/polyfills e functions
 * Nomenclatura camelCase su file js NON errata, ma Andre preferisce 
 * dashcase. 
 * COSTANTI:  ...58'?
 *  - camelCase: valore calcolato a runtime
 *  - UPPERCASE: valore letterale, statico
 * Nome espr. reg: "re" non va bene quanto "fcRegex", che è più 
 * chiara 1h02
 * Attenta a ..?
 * 
 * Evita di piramidizzare (?) il codice, evita gli if se possibile. 
 * 
 * Hai fatto raccolta di info per l'idea, ma non la stessa analisi 
 * per scrivere il codice: tipo cosa scrivere. come codice, e dove. 
 * A una certa, se non organizzi il lavoro dal pov del codice, 
 * potresti non capire perché qualcosa funziona o no.
 * 
 * Per le date, per averle sempre precise, puoi farle formattare 
 * nell'ogg date con una stringa
 */
const d = new Date();
event.toLocalDateString();
/**
 * Potresti anche usare una libreria tipo moment: bypassi ogg Date 
 * e dai new Moment, e poi usi il metodo format
 */

const radio = document.querySelector('input[name=""]')

/**
 * keyup: inserisci CF e automaticamente ti compila gli altri 
 * campi senza che premi il bottone, appena finisci di scrivere 
 * l'ultima lettera, cioè appena rispetti la regEx. Un invio con 
 * qualsiasi altro pulsante.
 * addEventListener("keyup", () => {...})
 * 
 * 2h08
 * Carico utenti
 * ev keyup -> cerco tra utenti
 * ev keyup/change -> carico utenti
 * In risposta faccio partire la chiamata: con JSONplaceHolder ho il 
 * modo di fargli filtrare delle info. 
 * Il server fatto da Andre è basato su JSON server: se leggi le sue 
 * doc, trovi cose interessanti: posso filtrare le info con Filter. 
 * localhost3000/people?tax_code=MLEFNC93S70A662F
 * Quando hai il problema di fare tante chiamate e rischi di avere 
 * tutti gli utenti, così ne prendi uno solo. 
 * 
 * Lato client, la stampa la fa il server: le info le butti lì e loro 
 * pensano alla stampa. Magari ti crei una pagina da stampare
 * 2h14'
 * Basta nascondere el non da stampare, scrivere info e contatti, e 
 * mettere le info con 
 * foglio CSS che metti nel file, con rel="printer" (probably).
 * ...
 * Mi salvo questa pagina; il foglio CSS che mi tiro giù salvando, lo 
 * modifico finché
 * ...
 * Riascolta!
 * 
 * Poi Ari fa una domanda e non ho capito. Thro to 2h21 e oltre
 * 
 */

/**
 * Se usi google font, il font è salvato nella cache: quando la 
 * ricarichi, ti svuota la cache e quindi ci mette un secondo o più 
 * per ricaricare il font.
 */

/**
 * REC II 
 * 36'
 * CTRL + SPACE BAR = fa apparire in basso la tendina dei suggerimenti
 * CTRL + CLICK = apre un file con info su quell'el di js (tipo var o remove ecc)
 * Se commenti una funzione o el con:    /** */    /*sopra di essa, quando 
 * poi la chiami da un'altra parte e ci passi sopra il cursore, vedrai quello 
 * che hai scritto nel commento.
 */


 /**
  * 20.01 REC I
  * 
  * ... metti un data ttribute ...
  * Capisci qual è l'elemento che hai cliccato e fai le azioni necessarie. 
  * Dare il valore esadecimale dentro un data attribute sul bottone che 
  * clicchi, lo leggi nell'eventListener che è L'UNICO che scrivo, e poi leggo 
  * il colore nel data attribute
  * Sull'el html scrio: <button data-color="F4F4F4"> ...
  * O sennò data-class con le cose che ti interessano e poi lo prendi 
  * e gli fai fare quello che vuoi.
  * 
  * Riprende a 15' ca per spiegare a Gerardo.
  */

 /**
  * 24.01
  * REC I 
  * 10' undefined
  * Meglio null perché undefined è quello che viene dato dal JSEngine quando 
  * ..., quindi noi come valore non lo dobbiamo dare mai.
  * Neanche null è bello, che nasce come refuso?, significa: non ho un 
  * puntatore alla zona di memoria. Quindi al più usiamo quello per indicare 
  * che l'ogg non esiste e non c'è quindi un puntatore alla memoria. 
  * 
  * 58' OUR PROJECT
  * Per quanto riguarda il print, vai di dimensioni fisse invece che responsive. 
  * 1h23 Le chiamate vanno fatte in maniera diversa.
  * Negli environment di solito ci sono tanti file quanti sono gli ambienti: 
  * produzione, stage, sviluppo. 
  * ...?
  */