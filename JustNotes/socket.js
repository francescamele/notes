//REC VID 18.12
//8'04'' differenza tra mine (-8) e Andrea's (+8)
/**
 * Esempio su direttagoal.it
 * Ci sarà magari un socket che ascolta la partita Roma-Regina.
 * - Faccio una chiamata con fetch, inizializzo prendendomi 
 *   tutti i dati.
 * - Mi faccio un socket e dico: mandami tutti i dati che carichi,
 *   ogni volta che ce n'è uno di cui ho bisogno.
 * 
 * Il socket deve iniziare con:
 * - ws: comunicazioni scambiate in chiaro (non si usano più)
 * - wss: comunicazioni cifrate
 */
// const socket = new WebSocket("url",
//     JSON.stringify({ auth: {token: "qwerty" } })
// );

//Prendo l'indirizzo della chat e lo passo al new WebSocket: 
//questo è l'indirizzo del mio socket
const socket = new WebSocket("wss://demo.piesocket.com/v3/channel_1?api_key=oCdCMcMPQpbvNjUIzqtvF1d2X2okWpDQj4AwARJuAgtjhzKxVEjQU6IdCjwm&notify_self");
//^Questo socket sta propagando le stesse info a tutti i socket 
//aperti sui vari browser in ascolto di queste info

// Evento "message": 
// Si scatena ogni volta che il socket siceve dei dati
// I dati sono contenuti all'interno del payload dell'evento 
// all'interno della chiave "data"
socket.addEventListener('message', ev => {
    // il nostro ascoltatore è una funzione che ha la stessa firma
    // che passiamo all'addEventListener...29'
    console.log(ev.data); 
})

//Per fare la stessa cosa, con fetch dovremmo usare un setInterval e 
//rifare la stessa cosa più volte.
//A volte però è meglio non usare il socket: magari vuoi solo mostrare 
//gli utenti di x, quindi non ha senso mantenere aperta la chiamata.
//Quando puoi risolvere una richiesta con una sola risposta, meglio
//fetch;
//Se invece devi avere aggiornamenti in tempo reale, usi un socket.