/**
 * Read a post for the given id 
 * @param {number} id The post id
 * @returns {Promise<Post>} Returns Post wrapped in a promise
 */
 export const getPost = async id => {
    //In Models, creiamo un nuovo file, post.js, in cui creereno una nuova classe:
    //Post, che esporterò. Per i suoi parametri, vedo l'oggetto sul sito: 
    //metto le chiavi dell'ogg, nell'ordine dell'ogg.

    // return fetch(`http://jsonplaceholder.typicode.com/posts/${id}`)
    //     .then(response => response.json())
    //     .then(data => new Post(data.id, data.title, data.body, data.userId));
    //A questo punto, nel file presente dovrebbe farmi l'import di Post in automatico
    //     //Ricordati che con l'arrow function c'è un return implicito
    //     //Poi iniziamo a passare le info al costruttore: usiamo i nomi dei parametri, e 
    //     //vengono fuori nello stesso ordine/con gli stessi nomi dell'ogg sul sito, perché
    //     //quelli sono fatti bene

    //Commento tutto per rendere la funz asincrona:
    const response = await fetch(`http://jsonplaceholder.typicode.com/posts/${id}`);
    //(Il response.json è l'equivalente di JSON.parse: trasforma la stringa in oggetto 
    //leggibile. Questo perché le promise hanno il METODO JSON che compie la stessa azione)
    const data = await response.json();

    //Se voglio passare al Post l'id, scrivo data.id... ecc. Abbiamo fatto in modo che le nome 
    //delle proprietà del model coincidano con la risposta: non è fatto apposta, è che le chiavi 
    //in quest'oggetto si chiamano in modo ottimo (a parte 'body', che poteva essere 'content')
    return new Post(data.id, data.title, data.body, data.userId);
    //^Così posso TRASFORMARE UNA STRUTTURA DATI GENERICA IN UN MODEL.
    
    //Facendo return fetch, restituisco tutto quello che mi dà l'ultimo then, cioè un'altra 
    //Promise.
    //Per arrivare a leggere il risultato di tutta questa elaborazione, e quindi per poter 
    //leggere la promise che restituisce il post, devo restituire tutto questo^: restituendo 
    //tutto quello, sto prendendo l'ultima promise dell'ultimo then. È come se stessi evocando 
    //delle funzioni in cascata, come i metodi, ottenendo il risultato dell'ultima invocazione. 
}
