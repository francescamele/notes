/**
 * È importante la versione degli strumenti e dei pacchetti che usiamo
 * Il progetto parte da zero con npm, che va inizializzato
 * Node Package Manager
 * Il gestore di pacchetti di node: quindi bisogna installare NodeJs
 * 
 * Per poter usare npm:
 * - npm init
 * ^Crea il package.json iniziale, con le dipendenze:
 * - Dipendenze di produzione: strumenti di cui abbiamo bisogno per 
 *    ESEGUIRE l'app in un ambiente reale, che userà l'utente
 * - Dip di sviluppo: strumenti che usiamo noi per svilippare
 * 
 * entry point: inizio del codice, adesso hai notes.js perché ci hai messo questo
 * file, ma dovrebbe essere index.js di suo
 * keywords: parole chiave che servono al motore di ricerca di npm per individuare 
 * delle librerie che ci serviranno da usare su npmJS
 * 
 * Una volta finito npm init, si è creato package.json con tutte queste
 * info che ci ha chiesto
 * 
 * Una delle cose fondamentali è webpack: è un bundle system che, a fronte
 * di tanti file, tra src, functions, models etc., prende tutta questa roba 
 * e la agglomera nel minor numero di file possibile.
 * (Cartella models: solitamente il nome del model coincide col nome del file,
 * e in quei file ci trovi SOLO quella classe)
 * La cosa figa dell'esportare un elemento, come un model, è che la sua 
 * definizione la scrivi una volta sola ma poi la puoi esportare TUTTE LE
 * VOLTE CHE VUOI
 * Eg. nella cartella functions avevi tutte le funzione per 
 * leggere/creare/modificare commenti o post: in tutte le parti 
 * dell'applicativo in cui devo leggere un commento o un elenco di commenti, 
 * richiamo la funzione in functions. Il vantaggio è che l'ho dichiarata 
 * solo 1 volta: se c'è qulacosa da correggere, devo farlo solo una volta.
 * 
 * Quindi aggiungiamo WEBPACK
 * Documentazione ufficiale per installarlo:
 *  - npm install webpack webpack-cli --save-dev
 *  - npm install webpack webpack-cli -D
 * --save-dev: declini la dipendenza in dip di SVILUPPO. Stessa cosa di -D
 * 
 * DIPENDENZE
 * - Di produzione: necessaria al funzionamento dell'applicazione: senza 
 *   quella dipendenza, l'applicazione non funziona
 *   Eg: Bootstrap (gestione layout di pagina)
 *       popper, dip di Bootstrap
 * - Di sviluppo: servono allo sviluppatore per agevolare il processo di
 *   di sviluppo dell'applicazione
 *   Eg: Babel: transpiler
 *       Prettier: indentazione
 * 
 * Quindi diciamo una serie di cose a webpack, oltre a unire i file:
 * - Indenta il codice con prettier
 * - Transpile the code with babel
 * 
 * Adesso dobbiamo creare il file di configuraz di webpack:
 *  - webpack.config.js
 * Chiamalo così perché webpack lo cerca automaticamente così.
 * Mettilo nella ROOT DEL PROGETTO
 * 
 * Ci creiamo una cartella src: conterrà i sorgenti del nostro applicativo.
 * Qui metteremo main.js
 * 
 * Ora dobbiamo spiegare a webpack che quando analizza i file js
 * deve fare una serie di cose
 * Lo vedi in Docs: module, con all'interno rules
 * - test:  /\.js$/
 *   ^Il $ nelle espressioni reg indica che devi cercare solo alla fine
 * 
 * Perché webpack funzioni, ha bisogno di un 'connettore': babel, prettier,
 * e e altri loader. Questi sono in "use", dentro "rules", dentro "module" 
 * (in webpack.config.js)
 * 
 * PRETTIER
 * Devo installare prettier: vai a cercare la doc
 * npm install --save-dev --save-exact prettier
 * Ma togli --save-exact prettier perché quello ti fa scaricare una versione 
 * precisa: quando poi andrai a fare npm install per aggiornare, ti scaricherà
 * di nuovo questa versione, non le successive.
 * Quindi:
 *  - npm install -D prettier
 *  - npm install -D prettier-loader
 * Crei anche un file:
 *  .prettierrc.json
 * E ci metti un oggetto vuoto
 * 
 * 
 * AGGIUNGI SCRIPT WEBPACK
 * Poi run it: aggiungi mode: 'development'
 * Poi testi prettier: scrivi 
 * const a = 2+2; , poi salvi e prettier dovrebbe indentartelo
 * 
 * 
 * Abbiamo configurato webpack e prettier: ci resta babel
 * 
 * BABEL
 * 
 * babel ci serve perché così possiamo scrivere in Es6+ in tutta tranquillità,
 * perché babel ce li transpila in ES5, così qualsiasi browser online, 
 * inclusi quelli vecchi che leggono solo Es5 o 6 o 7, può leggere
 * Per lavorare con babel, vai sulla doc:
 * https://babeljs.io/setup
 * Build System - Webpack
 * Trovi:
 *  - npm install --save-dev babel-loader @babel/core 
 * ma tu scrivi:
 *  - npm install --save-dev babel-loader @babel/core @babel/preset-env
 * 
 * babel legge il codice, applica trasformazioni, e poi ci dà il code
 * trasformato. Per farlo, deve avere al suo interno delle POLYFILL: 
 * questa si occupa di trasformare il code in qualcos'altro. Sappiamo 
 * che nel nostro transpiler ci saranno una o più polyfill. Eg, una ci 
 * dirà: quando trovi una arrow function, levala e mettici quella scritta 
 * con 'function'.
 * Ogni polyfill si occupa di qualcosa.
 * @babel/preset-env è una raccolta di polyfill, quelle più importanti 
 * per trasformare da ES10 in giù.
 * Ci sono alcune situazioni che per cui si dice a babel di trasformare 
 * il codice in ES6, o ES7, ma noi preferiamo trasformare fino a ES5
 * 
 * Di norma, i transpiler non usano polyfill a meno che non glielo dici tu:
 * quindi lo devi dire tu su un file
 * - file: babel.config.json
 * - content: {"presets": ["@babel/preset-env"]}
 * 
 * L'ultima cosa che ci resta da fare è di dire a webpack di utilizzare
 * anche il babel-loader.
 * RICORDATI: wbp applica i loader dall'ultimo al primo. Quindi prettier
 * dev'essere l'ultimo nell'array: wbp lo userà per primo per indentare 
 * il codice, POI userà babel per transpilarlo
 * 
 * Puoi anche usare questa notazione:
 *  rules: [{
      exclude: /node_modules/,
      use: [{
        loader: 'babel-loader'
      }, {
        loader: 'prettier-loader'
      }],
 * perché così la config è pronta per aggiungerci altre cose. Ma per ora,
 * che usi solo questi due loader, non c'è bisogno
 */