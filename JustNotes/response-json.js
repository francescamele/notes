//Commento tutto per rendere la funz asincrona:
const response = await fetch(`http://jsonplaceholder.typicode.com/posts/${id}`);
//(Il response.json è l'equivalente di JSON.parse: trasforma la stringa in oggetto 
//leggibile. Questo perché le promise hanno il METODO JSON che compie la stessa azione)
const data = await response.json();