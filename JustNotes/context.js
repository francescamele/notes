function fn(n) {
    return n * 2;
}

// Invocazione tradizionale:
// Il "contesto" viene desunto automaticamente
// dal Javascript Engine (l'interprete): "this" vale
// "window" ovvero un riferimento al browser
fn(2);

// Invocazione con il metodo "call" dell'oggetto Function
// Il contesto viene esplicitato dallo sviluppatore
// Es.1: il contesto è "this" che, in questa parte
// dello script, vale "window" (non mi trovo in un oggetto istanza)
fn.call(this, 2);

function altraFn(n) {
    console.log(this.p);
    return n * 2;
}

// Il contesto di esecuzione "this" vale quanto "window"
// Ovviamente "window" non ha una proprietà chiamata p:
// il console.log mi stamperà "undefined"
altraFn.call(this, 2);

// Cambiamo il contesto di esecuzione:
// L'oggetto passato ha una proprietà "p"
// quindi console.log mostrerà "ciaone"
altraFn.call({p: 'ciaone'}, 2);
