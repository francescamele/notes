- Manda questi comandi dalla CLI:
  - npm init: Creazione package.json su cli

  - npm install webpack webpack-cli --save-dev
  OPPURE
  - npm install webpack webpack-cli -D
  (stessa cosa)

- Crea file in root: 
webpack.config.js

- Crea cartella /src/ in root:
Qui metteremo main.js (o index.js: il sorgente del progetto)

- Configura webpack.config.js:
module.exports = {
  entry: {
      main: './src/main.js',
  },
  mode: 'development',
  module: {
      rules: [{
          exclude: /node_modules/,
          test: /\.js$/,
          use: [
-->           'babel-loader',
-->           'prettier-loader'
          ]
      }]
  },
  output: {
      filename: '[name]bundle.js',
      path: __dirname + '/dist'
  }
}
^I loader di babel e prettier andranno aggiunti
dopo che sono stati scaricati.


PRETTIER
- Installa prettier:
  - npm install -D prettier
  - npm install -D prettier-loader

- Crea file prettier:
.prettierrc.json
e ci metti un oggetto vuoto: {}

- Aggiungi il loader di prettier nel webpack.config.js,
  nell'array di use:
...
  use: [
      'prettier-loader'
  ]
...

- Aggiungi script al package.json:
"scripts": {
    "test": "echo \"Error: no test specified\" && exit 1",
--> "webpack": "webpack --watch"
  },

TEST
- Fai partire webpack nella cli:
  - npm run webpack
e controlla che prettier funzioni: magari scrivi in un file js:
const a = 2+2;
premendo invio dovrebbe separarti i numeri


BABEL
Documentazione: https://babeljs.io/setup
- Nella cli, manda:
  - npm install --save-dev babel-loader @babel/core @babel/preset-env

- Crea file:
babel.config.json
Al suo interno:
{
    "presets": ["@babel/preset-env"]
}

- Aggiungi il loader di babel nel webpack.config.js,
  nell'array di use:
...
  use: [
      'babel-loader',
      'prettier-loader'
  ]
...
^Ricordati che webpack applica i loader dall'ultimo al
primo: prettier deve venire prima di babel

- L'array di use si puo scrivere anche così:
use: [{
        loader: 'babel-loader'
      }, {
        loader: 'prettier-loader'
      }],
perché così l'array è pronto per aggiungerci altri loader,
ma a noi non serve.