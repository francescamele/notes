//REC 4 9'

class MySet {
    //fuori dai metodi non hai bisogno del this: lo capisce
    //da solo
    #_values = [];
    //Col # possiamo segnare qualcosa di privato: ne limito
    //la visibilità, e quindi ho più controllo sullo stato delle
    //informaz e sullo stato interno degli ogg
    //Il _ è una convenz storica per indicare una proprietà
    //PRIVATA: lo mantieni perché così quando metti tutto in
    //ordine alfabetico, le propr private che iniziano tutte
    //per _ staranno tutte in cima.

    //Ho già memorizzato l'arr vuoto nella classe, quindi non
    //c'è bisogno di farlo nel costruttore
    //È meglio metterlo anche se è vuoto, per chiarezza, perché
    //nel costruttore si inizializzano le proprietà di stato
    //di una classe
    constructor() {
        //se lo scrivi qui, ci vuole il this perché sei 
        //dentro un metodo.
        //this._values = [];
    }

    //Questi metodi li mette in ORDINE ALFABETICO
    add(value) {
        //l'el contenuto in "value" esiste già: 
        //non devo aggiungerlo nuovamente.
        if (this.#_values.includes(value)) {
            return;
        }

        //l'el non è stato individuato nella collezione
        //quindi lo aggiungo
        this.#_values.push(value);
    }

    clear() {
        //In clear non c'è 'value' perché non gli devi 56'
        //perché prende e brasa (svuota) l'arr

        //elimino tutti gli el dall'arr semplicemente creandone
        //uno nuovo
        this.#_values = [];
    }

    delete(value) {
        //Individuo l'el "value" all'interno dell'array
        //indexOf restituisce un numero >= 0 se viene trovato 
        //un el nell'array, altrimenti -1
        const pos = this.#_values.indexOf(value);

        //Nel caso in cui l'el non sia stato individuato, non
        //procedo
        if (pos === -1) {
            return;
        }

        //Elemento trovato: gli dico la posizione e lo elimino
        this.#_values.splice(pos, 1);
        //splice dice: dammi la posizione, dimmi quanti elementi
        //ti devo rimuovere, e poi tolgo tutto
    }

    has(value) {
        //Restituisco il valore datemi da 'includes': se
        //value esiste, restituirò true, altrimenti false
        //Se l'el nell'arr ci sta, allora mi darà un true
        return this.#_values.includes(value);
    }
}

//Come in ES5, con la differenza che in ES5 è una funz:
function MySet() {

}

//È come scrivere così: nell'ogg conta la chiave, NON l'ordine
const o = {
    add: value => {

    },

    clear: () => {

    }
}