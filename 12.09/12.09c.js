function cacheDecorator(fn) {
    const cache = new Map();

    //Il rest operator mi permette di prendere con il
    //cacheDecorator una funz con qualsiasi numero di param:
    //La funz restituita dal decorator sfrutta il Rest
    //Operator per poter essere invocata con un numero arbitrario
    //di argomenti
    //Stiamo dicendo: 1h05'
    return (...params) => {
        //Devo rivedere però anche il meccanismo di cache: come
        //possiamo creare una key che identifichi TUTTI I NOSTRI
        //PARAMETRI?
        //Could think of a concatenaz di stringhe:
        //params[0] + params[1] + params[2] => stringa
        //Ci pensiamo dopo, come generarci una chiave.
        
        //Pensiamo ora a come invocare la funz senza sapere quanti
        //param ha:
        if (cache.has(v)) {
            return cache.get(v);
        }

        //fn non può essere invocata così: fn(), o saremmo
        //obbligati a mettere tutti i valori separati da virgola

        //Si può scrivere, in ES5:
        //const res = fn.apply(this, params);
        //perché è vero che devi dare il contesto/scope con this,
        //però da params capisce che deve creare un arr con tutti i
        //parametri

        //Ma noi scriviamo, con ES6:
        const res = fn(...params);
        //this è lo scope; ...params sono tutti i parametri con
        //cui dev'essere invocata, e lui ce li dà separati da virgola

        cache.set(v, res);

        return res;
    }
}

const funzioneDecorata = cacheDecorator(() => {})

const a = [1, 2, 3, 4, 5];
fn(...a);
//METODO APPLY
cacheDecorator.apply


//ES6:
const cacheDecorator = fn => {
    const cache = new Map();

    return (...params) => {
        //Il for of non mi dà l'indice, quindi usi il forEach,
        //che si prende una funz con l'elem e volendo anche 
        //l'indice come secondo parametro:
        //  let k = '';
        //  params.forEach((e, i) => k.concat(`${i}: ${e}`))
        //ricontrolla REC, 3' ?
        //Però è più complicato, perché devi dire
        //data type: valore - data type: valore

        //Crea una stringa contenente tutti gli elem di un arr, separati
        //attraverso il separatore specificato
        // const k = params.join(',');

        //Oppure:
        const k = params.map(p => `${typeof(p)}:${p}`).join(',');
        //Il map prende tt gli el di un arr, ci applica una cb, e mi
        //tira fuori un nuovo array
        //typeof(p) mi restituisce il datatype di p
        //2nd REC 20' su $ e `

        if (cache.has(k)) {
            return cache.get(k);
        }

        const res = fn(...params);
        cache.set(k, res);

        return res;
    }
}

const funzioneLenta = (v1, v2) => {
    //Ci metto un sacco di tempo... avrei proprio bisogno di
    //una cache
    return v1 * v2;
};

// const funzioneDecorata = cacheDecorator(() => {})
const funzioneDecorata = cacheDecorator(funzioneLenta)

funzioneDecorata();

// '1', '01' => '101'
// '10', '1' => '101'
//^Non funziona: due info completamente diverse mi danno la 
//stessa cosa.

//Potrei generare una chiave composta, dicendo che alla posiz
//1 hai il primo param, alla posiz 2 hai 01, e così via:
// '1: 1, 2: 01'
//Quindi come fare? 
//Dobbiamo iterare su tutti i parametri

//Funz join degli arr: prende tutti gli elem degli arr e li
//unisce in una stringa, e tutti gli elem me li separa da una
//virgola
// const k = params.join(',');

//params = ['1', '10']
//map = ['string:1', 'string:10']
//join 'string:1,string:10'
funzioneDecorata('1', '10'); 
//con datatype: string:1,string:10 
//con solo join: 1,10
funzioneDecorata(1, '10');
//con datatype: number:1,string:10 
//con solo join: 1,10