//Funzioni possono vedere i propri ? estesi, come gli ogg:
class A {

}

class B extends A {

}
//^Si prende tutti i metodi e anche tutte le chiavi 37'

/**
 * C'è un sistema thr which possiamo creare funz che servono
 * a implementare una specifica funzionalità di un'altra funz
 * ?
 */

 function fnMoltoComplessa(v) {
    return v * 2;
}

//I decorator si applicano invocando il decorator, passandogli
//la funz da "migliorare" e recuperando la nuova funz che il
//decorator ci restituisce
function cacheDecorator(fn) {
    const cache = new Map();

    //Qst funz, potendo accedere a tutti gli elementi dichiarati
    //al suo esterno, può accedere anche a cache
    return v => {
        if (cache.has(v)) {
            return cache.get(v);
        }

        const res = fn(v);
        cache.set(v, res);

        return res;
    }
}

// Applico il decorator della cache passando ad esso la funz
// da "avvolgere"
const nuovaFnMoltoComplessa = cacheDecorator(fnMoltoComplessa);

//Per invocare la fnMoltoComplessa con il meccanismo di cache,
//uso la funzione che mi ha restituito il decorator:
nuovaFnMoltoComplessa(4);

//Problema: così funziona SOLO per un parametro
//vedi next file