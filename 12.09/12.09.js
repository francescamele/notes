// Map, a differenza degli ogg JS (istanziati con le {}),
// permette di creare delle coppie di dati (chiave-valore)
// dove la chiave puà essere qualsiasi tipo di dato.

/**
 * Ad es:
 *  Se in un ogg la chiave true viene convertita a stringa...
 *      { true: 'valore } => true diventa 'true' (la stringa true)
 *  Con Map, il valore true (booleano) rimarrà inalterato
 *      Map() -> true => 'valore'
 *  per accedere a 'valore' dovremo passare il valore booleano
 *  true e non la stringa 'true'
 */
const o = {
    true: 'valore' //Non sarebbe true bool ma 'true' stringa
}

//Map ci permette di passare dal valore della stringa NELL'OGG
//(quindi nelle {}), e aggiungere un altro valore per usare
//il valore primitivo (aggiungere valore A e valore C in qst es)
1 => 'valore A'
'1' => 'valore B'
true => 'valore C'
'true' => 'valore D'

//È l'esecuz dello script che a runtime mi genererà il valore.


//Map è un OGGETTO ISTANZA: come tale, deve essere istanziato:
const m = new Map();
//Con new Map() abbiamo creato un vero e proprio oggetto nuovo,
//richiamabile solo con GET e riempibile con set:

//Come aggiungere elementi a Map? Ha una serie di metodi.
//Quello che mi interessa è il set: prende una chiave e le
//dà un valore:
m.set(1, 'Valore associato alla chiave 1 numerica');
m.set('1', 'Valore associato alla chiave 1 stringa');

//Come recupero queste informazioni? Thr metodo GET:
//get permette di recuperare il valore associato a una chiave
const val1number = m.get(1);
//Conterrà: 'Valore associato alla chiave 1 numerica'
const val1string = m.get('1');
//Conterrà: 'Valore associato alla chiave 1 stringa'

//Come chiave puoi mettere qualsiasi cosa, tra cui anche una
//funz; l'unica cosa che NON puoi usare sono le PAROLE RISERVATE

// METODO CLEAR
//Elimina tutte le coppie key-value
m.clear();

// METDOO DELETE
// Rimuove una coppia key-value a partire dalla key fornita
m.delete('1');

//forEach: permette di iterare su tutti gli elementi

// KEYS
// Dà un arr con tutte le chiavi
m.keys()
//Mi darebbe qui: [1, '1'];

//HAS
// permette di stabilire se una key esiste all'interno di Map
m.has(1);
//restituisce: true/false


//ABBIAMO UNA FUNZIONE:
const cache = new Map();

function fnMoltoComplessa(v) {
    //se dato un valore v dobbiamo calcolarci 

    //cache: magazzino dove si mettono delle info calcolate
    //una volta, così se qualcuno le richiede di nuovo non
    //devi ricalcolarle. Tipo mega blocco di appunti dove ti
    //segni le cose che hai messo un po' per calcolare.

    //26'

    //v è un param formale: lo stabilirò quando invocherò la
    //funz: potà essere la qualunque
    if (cache.has(v)) {
        return cache.get(v);
        //se ce l'ho, restituisco il valore con get
    }

    //se non ce l'ho, mi calcolo il valore che deve restituire
    //qst funz e me lo salvo in cache con set
    const res = v * 2;
    cache.set(v, res);

    return res;
}
//quando poi:
fnMoltoComplessa(4);
fnMoltoComplessa(4);
//la seconda volta, non entrerò più nella parte del calcolo,
//ma nell'if, perché il valore (8) sarà già salvato in cache.


// Voglio creare una funzione che si prende un'altra funz, 
// la avvolge, e le regala il meccanismo di cache.
// NON così:
// const altraCache = new Map();
// function altraFnMoltoComplicata(v) {
// }

// MA: DECORATOR FUNCTION
// Una df serve ad inglobare un'altra funzione e ad estenderne
// le proprietà. 
// Eg, cacheDecorator prenderà una funz e le avvolgerà intorno
// un meccanismo ci cache

// I decorator si applicano invocano il decorator, passandogli 
// la funx da 'migliorare' e recuperando la nnuova funzione che
// il decorator ci restituisce.
function cacheDecorator(fn) {
    const cache = new Map();

    //Qst funz, potendo accedere a tutti gli elementi dichiarati
    //al suo esterno, può accedere anche a cache
    return () => {

    }
}
//VEDI PROSSIMO FILE