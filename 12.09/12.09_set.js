//Set è un oggetto istanza che serve a contenere collezioni di
//informazioni.
//A differenza degli array, in cui gli elem possono ripetersi,
//set NON CONSENTE LA RIPETIZIONE DI ELEMENTI all'interno della
//collezione:
//  [1, 2, 1, 4] => ok in un array
//  NON consentito in un Set

//Come tt gli ogg istanza, va istanziato
const s = new Set();

//Non ha il metodo get, ma il METODO ADD:
s.add(1);
s.add('1');
//s.add(1); NO: NON posso ripetere l'elem numerico 1

//metodo has: verifica la presenza di un elem nella collez
//restituisce true/false
s.has(1);

//Metodo delete: rimuove un elemento dalla collezione
s.delete(1);

//Metodo clear: rimuove tutti gli elem dalla collezione
s.clear();

//METODO INCLUDES PER ARRAY
//Dai l'el da cercare e lui risponde con true or false
//[1, 2, 3].includes(2) => dà true

/**
 * Come poter creare una collezione di elem che non ammetta 
 * ripetizioni
 * Crea una classe che simuli il comportamento del Set: devi
 * evitare la possibilità di ripetizioni all'interno della 
 * collezione
 */