//ARROW FUNCTIONS. REC 2
//V. anche users.js in .\CodeSlider\src\functions

//In ES5:
//function declaration:
function fnName() {

}
//function expression:
var fn = function() {

}
//^FUNZIONE ANONIMA

//IN ES6
//Si chiamano a.f. poiché la simbologia '=>' ricorda
//una freccia
//Sono SEMPRE anonime.
//Il simbolo della freccia separa la parte dichiarativa
//(cioè la firma) dal corpo.
const fn = () => {};
//Di solito le a.f. sono inserite o all'interno di let o
//all'interno di const. Andrea preferisce const perché così
//non le cambi (perché non ci sono metodi per alterare 
//l'interno della funzione, puoi solo riassegnarle)

//Quando ho un solo parametro formale posso omettere le ()
//Ad es.:
//  a => {}
//Non è necessario quindi scrivere:
//  (a) => {}

//Quando ho più di un parametro formale, sono costretto a 
//reintrodurre le ().
//  (a, b) => {};

//In alcuni contesti però si mettono le parentesi anche per il
//singolo parametro, per via di Typescript (14')
//Però prettier è configurabile: puoi dirgli di non farlo.:
{
    "arrow parens": "avoid";
}
//Ricordati che il file di prettier va messo in linguaggio json
//quando aggiungi questo (altrimenti è txt), e che il json 
//accetta solo i doppi apici.

//In ogni caso, quando la chiami DEVI mettere le parentesi.

/**
 * Quando il corpo della funzione è costituito da una sola
 * istruzione, le {} possono essere omesse.
 * 
 *  n => n * 2
* 
 *     'n * 2' (il corpo) è soggetto a un RETURN IMPLICITO: 
 * Quando non sono presenti le {}, il risultato della singola
 * espressione viene restituito automaticamente, come se prima
 * di questa venisse scritto 'return'
 * 
 * Attenzione: in presenza delle {}, il return DEVE essere 
 * esplicitato come sempre.
 */

/**
 * Inoltre, visto che const e let NON SONO SOGGETTE A HOISTING,
 * sei obbligato a rispettare l'ordine di esecuzione.
 * 
 * Le ritrovi anche in altri linguaggi, chiamate in altri modi.
 */